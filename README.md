Quick links
============
**Home page:** www.coffeepoweredgames.com

**How to update readme.md using github markup syntax:** https://help.github.com/articles/github-flavored-markdown

**How to preview github markup syntax:** http://github-preview.herokuapp.com/

About 'GAME'
============

Proof of concept for a hobby game project.
