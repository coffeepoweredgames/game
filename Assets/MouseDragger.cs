﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class MouseDragger : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    public DraggingContext draggingContext { get; set; }
    private Agent agent;
    private LocationDescriptor preDragLocation;

    //agent is at z = -0.3, province z = -0.3, country z = -0.2
    private Vector3 MAP_BOTTOM_OFFSET = new Vector3(0f, 0f, -5f);//TODO 

    void Awake() {
        agent = GetComponent<Agent>();
    }

    public void OnBeginDrag(PointerEventData eventData) {
        DragAgentBegin();
    }

    public void OnDrag(PointerEventData eventData) {
        DraggingAgent();
    }

    public void OnEndDrag(PointerEventData eventData) {
        DragAgentEnd();
    }

    private void DragAgentBegin() {
        print("DRAG BEGIN");
        clearAndSetupDraggingContext();
        draggingContext.isDragInProgress = true;

        MapSelector.Instance.selectAgent(agent, MapSelector.ALLOW_RESELECT_TRUE);

        preDragLocation = agent.location;
    }

    private void DraggingAgent() {
        //print("DRAGGING");
        transform.position = GetMousePositionInWorld(transform);
        Debug.DrawRay(agent.transform.position, agent.transform.up * 100, Color.blue);
    }

    private void DragAgentEnd() {
        //print("DRAGGING END(begin)");
        draggingContext.isDragInProgress = false;
        Province provinceToDragOnto = MapSelector.Instance.selectProvince(new List<Ray> { new Ray(agent.transform.position + MAP_BOTTOM_OFFSET, agent.transform.up * (-1)) });

        if (provinceToDragOnto == null) {
            resetPositionCausedByDragging();

            return;
        }

        Mover.Instance.TryMoving();
        GetComponent<AudioSource>().Play();
        //print("DRAGGING END(end)");
    }

    private Vector3 GetMousePositionInWorld(Transform heldObject) {

        //// acquire x,y coordinate
        //Vector3 clickPos = Input.mousePosition; //clickPos = (x,z,0)

        //// acquire z coord
        //clickPos.z = Vector3.Distance(Camera.main.transform.position, heldObject.position);//clickPos = (x,y,distance)

        //// (x,y,z) -> world space
        //Vector3 mousePositionInWorld = Camera.main.ScreenToWorldPoint(clickPos);

        ////You may want to set the position again to clean up floating point errors
        //mousePositionInWorld.z = heldObject.position.z;

        //return mousePositionInWorld;

        float distance_to_screen = Camera.main.WorldToScreenPoint(heldObject.transform.position).z;
        Vector3 pos_move = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance_to_screen));
        return new Vector3(pos_move.x, pos_move.y, transform.position.z);
    }

    //private Vector3 GetMousePositionInWorld(Transform heldObject) {
    //    //float distance_to_screen = Camera.main.WorldToScreenPoint(heldObject.transform.position).z;
    //    //return Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance_to_screen));


    //    float distance_to_screen = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
    //    Vector3 pos_move = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance_to_screen));
    //    return new Vector3(pos_move.x, transform.position.y, pos_move.z);
    //}

    public void resetPositionCausedByDragging() {
        print("MouseDragger.resetPositionCausedByDragging");

        if (agent.location.Equals(preDragLocation)) {
            transform.localPosition = Vector3.zero;
            preDragLocation = null;
        }
    }

    private void clearAndSetupDraggingContext() {

        if (draggingContext) {
            Destroy(draggingContext.gameObject);
        }

        if (agent.draggingContext) {
            Destroy(agent.draggingContext.gameObject);
        }

        draggingContext = DraggingContextFactory.Instance.Build(agent);

        agent.draggingContext = draggingContext;

    }
}
