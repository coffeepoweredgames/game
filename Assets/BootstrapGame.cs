﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Main game script.
/// Boots up the game.
/// Adds components to gameObjects.
public class BootstrapGame : GameBase {//TODO load properties from *.yaml
    private GameObject singletons;
    private GameObject countries;
    private TileMapImpl map;

    void Awake() {
        map = GetComponentInChildren<TileMapImpl>();
    }

    void Start() {
        createManagers();
        createCountries();
        createStructuresOnMap();
        createStaticHud();
        setupCamera();
    }

    private void createManagers() {
        if (singletons != null) {
            return;
        }

        if (gameObject.transform.FindChild("Singletons") == null) {
            singletons = new GameObject();
            singletons.name = "Singletons";
            singletons.transform.parent = gameObject.transform;

            InputManager.Instance.transform.parent = singletons.transform;
            ScreenManager.Instance.transform.parent = singletons.transform;
            Positioner.Instance.transform.parent = singletons.transform;
            Mover.Instance.transform.parent = singletons.transform;
            MovementContextManager.Instance.transform.parent = singletons.transform;
            MapSelector.Instance.transform.parent = singletons.transform;
            GuiManager.Instance.transform.parent = singletons.transform;
            MissionManager.Instance.transform.parent = singletons.transform;
            MissionFactory.Instance.transform.parent = singletons.transform;
            MissionProgressFactory.Instance.transform.parent = singletons.transform;
            AgentFactory.Instance.transform.parent = singletons.transform;
            Tester.Instance.transform.parent = singletons.transform;
            PersistanceManager.Instance.transform.parent = singletons.transform;
            Notifier.Instance.transform.parent = singletons.transform;
            CountryFactory.Instance.transform.parent = singletons.transform;
            ProvinceFactory.Instance.transform.parent = singletons.transform;
            DraggingContextFactory.Instance.transform.parent = singletons.transform;
            StructureFactory.Instance.transform.parent = singletons.transform;
            TableRowFactory.Instance.transform.parent = singletons.transform;
            StatisticsManager.Instance.transform.parent = singletons.transform;
            StatFactory.Instance.transform.parent = singletons.transform;
            MissionGoalFactory.Instance.transform.parent = singletons.transform;
            Exploder.Instance.transform.parent = singletons.transform;
        }
    }


    void createStructuresOnMap() {
        GameObject spySchoolCube = Instantiate(Resources.Load(PrefabConstants.SPY_SCHOOL_PREFAB)) as GameObject;

        Country britain = DynamicObjectRegistry.COUNTRIES[Countries.BRITAIN.name];

        Transform structures = britain.transform.FindChild("structures");//TODO add structure method to province

        spySchoolCube.transform.parent = structures;
        spySchoolCube.transform.localPosition = StructureConstants.SPY_SCHOOL_LOCAL_POSITION;

        DynamicObjectRegistry.STRUCTURES.Add(StructureConstants.SPY_SCHOOL, spySchoolCube);

        Country germany = DynamicObjectRegistry.COUNTRIES[Countries.GERMANY.name];
        Country france = DynamicObjectRegistry.COUNTRIES[Countries.FRANCE.name];
        Country spain = DynamicObjectRegistry.COUNTRIES[Countries.SPAIN.name];
        Country portugal = DynamicObjectRegistry.COUNTRIES[Countries.PORTUGAL.name];

        Province province1 = france.provinces.GetProvinceList()[0];
        province1.AddStructure(StructureFactory.Instance.Build(StructureType.FACTORY, "CookieFactory"));

        Province province2 = spain.provinces.GetProvinceList()[0];
        province2.AddStructure(StructureFactory.Instance.Build(StructureType.FACTORY, "ShellFactory"));

        Province province3 = portugal.provinces.GetProvinceList()[0];
        province3.AddStructure(StructureFactory.Instance.Build(StructureType.FACTORY, "ArmsFactory"));

        Province province4 = germany.provinces.GetProvinceList()[0];
        province4.AddStructure(StructureFactory.Instance.Build(StructureType.FACTORY, "RefineryOil"));

    }

    private void setupCamera() {
        if (Camera.main.gameObject.GetComponent<GameCamera>() == null) {
            Camera.main.gameObject.AddComponent<GameCamera>();
        }

        Camera.main.transform.position = map.GetWorldSpaceCoordinatesFromPixels(map.GetMapCenter()) + map.GetMapNormal() * MapConstants.CAMERA_DISTANCE_TO_MAP;
        Camera.main.transform.rotation = Quaternion.LookRotation(map.GetWorldSpaceCoordinatesFromPixels(map.GetMapCenter()) - Camera.main.transform.position);
        Camera.main.transform.Rotate(Vector3.right, -40, Space.World);
    }

    private void createCountries() {
        Transform countriesTransform = map.transform.FindChild("Countries");

        if (countriesTransform == null) {
            countries = new GameObject();
            countries.name = "Countries";
            countries.transform.parent = map.transform;
            countries.transform.localPosition = MapConstants.MAP_ORIGIN;//children will inherit this transform
        }
        else {
            countries = countriesTransform.gameObject;
        }

        if (GameObject.Find(Countries.FRANCE.name) == null) {
            Country france = CountryFactory.Instance.Build(Countries.FRANCE);
            positionAndParentCountry(france);
        }

        if (GameObject.Find(Countries.SPAIN.name) == null) {
            Country spain = CountryFactory.Instance.Build(Countries.SPAIN);
            positionAndParentCountry(spain);
        }

        if (GameObject.Find(Countries.PORTUGAL.name) == null) {
            Country portugal = CountryFactory.Instance.Build(Countries.PORTUGAL);
            positionAndParentCountry(portugal);
        }

        if (GameObject.Find(Countries.IRELAND.name) == null) {
            Country ireland = CountryFactory.Instance.Build(Countries.IRELAND);
            positionAndParentCountry(ireland);
        }

        if (GameObject.Find(Countries.NORTHERN_IRELAND.name) == null) {
            Country northernIreland = CountryFactory.Instance.Build(Countries.NORTHERN_IRELAND);
            positionAndParentCountry(northernIreland);
        }

        if (GameObject.Find(Countries.SCOTLAND.name) == null) {
            Country scotland = CountryFactory.Instance.Build(Countries.SCOTLAND);
            positionAndParentCountry(scotland);
        }

        if (GameObject.Find(Countries.BRITAIN.name) == null) {
            Country britain = CountryFactory.Instance.Build(Countries.BRITAIN);
            positionAndParentCountry(britain);
        }

        if (GameObject.Find(Countries.BELGIUM.name) == null) {
            Country belgium = CountryFactory.Instance.Build(Countries.BELGIUM);
            positionAndParentCountry(belgium);
        }

        if (GameObject.Find(Countries.NETHERLANDS.name) == null) {
            Country netherlands = CountryFactory.Instance.Build(Countries.NETHERLANDS);
            positionAndParentCountry(netherlands);
        }

        if (GameObject.Find(Countries.DENMARK.name) == null) {
            Country denmark = CountryFactory.Instance.Build(Countries.DENMARK);
            positionAndParentCountry(denmark);
        }

        if (GameObject.Find(Countries.GERMANY.name) == null) {
            Country germany = CountryFactory.Instance.Build(Countries.GERMANY);
            positionAndParentCountry(germany);
        }

        if (GameObject.Find(Countries.SWITZERLAND.name) == null) {
            Country switzerland = CountryFactory.Instance.Build(Countries.SWITZERLAND);
            positionAndParentCountry(switzerland);
        }

    }

    private void positionAndParentCountry(Country country) {
        country.gameObject.transform.parent = countries.gameObject.transform;
        country.gameObject.transform.localPosition = country.gameObject.transform.localPosition = CountryConstants.TRANSFORMS[country.name + "_AREA_MESH_LOCAL_POSITION"];//ovo su relat koordinate u odnosu na map center // NE IDE OVDJE
        country.gameObject.transform.localRotation = Quaternion.Euler(CountryConstants.TRANSFORMS[country.gameObject.name + "_AREA_MESH_LOCAL_ROTATION"]);
        country.gameObject.transform.localScale = CountryConstants.TRANSFORMS[country.gameObject.name + "_AREA_MESH_LOCAL_SCALE"];

        //TODO quick fix, as long area meshes are used as province meshes
        //A country without defined provinces, will have one big province (country mesh duplicated for province mesh)
        foreach (Transform child in country.provinces.gameObject.transform) {
            child.position += new Vector3(0f, 0f, -0.1f);
        }
    }

    private void createStaticHud() {//TODO why not created in GUIMANAGER?
        GameObject instantiated = Instantiate(Resources.Load(PrefabConstants.SPY_SCHOOL_GUI_PREFAB)) as GameObject;
        instantiated.transform.position = new Vector3(1037.8f, 1101f, -20.6f);//TODO hardcoded temporary
        instantiated.transform.rotation = Quaternion.FromToRotation(instantiated.transform.up, (-1) * Vector3.forward);
        instantiated.SetActive(false);


        SpySchoolHud spySchoolHud = instantiated.GetComponent<SpySchoolHud>();
        spySchoolHud.gameObject.name = GuiConstants.SPY_SCHOOL_HUD_NAME;
        GuiManager.Instance.spySchoolHud = spySchoolHud;
    }




}
