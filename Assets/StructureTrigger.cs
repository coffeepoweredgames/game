﻿using UnityEngine;
using System.Diagnostics;
using System.Threading;

public class StructureTrigger : MonoBehaviour {

    public Structure structure;
    private GuiManager guiManager;
    private MissionManager missionManager;
    private Agent agent;

    //when mission is picked, dont display picker again, atleast not until reentering trigger
    public bool displayMissionPickerHud { get; set; }

    void Awake() {
        structure = GetComponent<Structure>();
        guiManager = GuiManager.Instance;
        missionManager = MissionManager.Instance;
    }

    void OnTriggerEnter(Collider collider) {
        agent = GetAgent(collider.gameObject);
        highlightProvince(true);
        displayMissionPickerHud = true;
    }

    void OnTriggerStay(Collider collider) {

        if (shouldDisplayMissionPicker(agent, structure)) {
            guiManager.showMissionPickerHud(structure, agent, true);
        }
    }

    void OnTriggerExit(Collider collider) {
        highlightProvince(false);
        GuiManager.Instance.showMissionPickerHud(null, null, false);
    }

    private Agent GetAgent(GameObject gameObject) {
        return gameObject.GetComponentInParent<Agent>();
    }

    private void highlightProvince(bool shouldHighlight) {
        if (shouldHighlight) {
            gameObject.transform.FindChild("Container").FindChild("Cube1").GetComponent<Renderer>().material.color = Color.blue;//TODO MapSelector.highlight
        }

        gameObject.transform.FindChild("Container").FindChild("Cube1").GetComponent<Renderer>().material.color = Color.white;

    }

    private bool shouldDisplayMissionPicker(Agent agent, Structure structure) {

        if (agent.draggingContext.isDragInProgress) {
            return false;
        }

        if (!agent.location.isLocationInCountry(structure.location.province.country)) {
            return false;
        }

        if (missionManager.isAgentOnMissionHere(agent, structure)) {
            return false;
        }

        if (!displayMissionPickerHud) {
            return false;
        }

        return true;
    }
}
