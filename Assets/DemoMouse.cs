﻿using UnityEngine;
using System.Collections;

public class DemoMouse : MonoBehaviour {

        public Color highlightColor = Color.red;
    public Color normalColor = Color.green;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000)) {
            GetComponent<Renderer>().material.color = highlightColor;
            Debug.Log(hit.ToString());
        }
        else {
           GetComponent<Renderer>().material.color = normalColor;
        }

        Debug.DrawRay(ray.origin, ray.direction * 2500, Color.yellow);

	
	}
}
