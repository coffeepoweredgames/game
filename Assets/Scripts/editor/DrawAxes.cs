﻿using UnityEngine;
using System.Collections;

/*
 Draw onto object to display local transform axes
 */
[ExecuteInEditMode]
public class DrawAxes : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Debug.DrawRay(transform.position, transform.right , Color.red);
		Debug.DrawRay(transform.position, transform.up , Color.green);
		Debug.DrawRay(transform.position, transform.forward, Color.blue);
		
		
		//Debug.DrawRay(transform.position, transform.right * (transform.localScale.x), Color.red);
		//Debug.DrawRay(transform.position, transform.up * (transform.localScale.y), Color.green);
		//Debug.DrawRay(transform.position, transform.forward * (transform.localScale.z), Color.blue);
		
	}
}