﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEditor;

public class GameDebugEditorWindow : EditorWindow {
	private static bool isNonDockable = false;
//	private List<GameObject> _selectedObjects = new List<GameObject>();
	private GameObject[] _selectedObjects;
//	private GameObject _selectedObject;
	private Vector2 _scrollPosition;

	[MenuItem("CPG/Game debugger %F7")]
	private static void showEditor(){
		EditorWindow.GetWindow<GameDebugEditorWindow>(isNonDockable, "Game debugger");
	}

	[MenuItem("CPG/Game debugger", true)]
	private static bool showEditorValidator(){
		return true;
	}

	void OnSelectionChange(){
		_selectedObjects = Selection.gameObjects;//.Where(go => go.GetComponentInChildren<Agent>() != null).ToList();
//		_selectedObjects = Selection.gameObjects.Where(go => go.GetComponentInChildren<Agent>() != null).ToList();
		Repaint();
	}


	void OnGUI(){
//		if (GUILayout.Button("Check singletons."))
//		{
//			//FindInSelected();
//		}

//		EditorGUILayout.PrefixLabel("test");
		
		_scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition+new Vector2(0, 10));
		{
			foreach(var go in _selectedObjects){
				EditorGUILayout.BeginHorizontal();
				{
					EditorGUILayout.PrefixLabel(go.name);
				}
				EditorGUILayout.EndHorizontal();
			}
		}
		EditorGUILayout.EndScrollView();
	}
}
