﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class CreateInventoryItemList : MonoBehaviour {
	
	[MenuItem("Assets/Create/Inventory Item List")]
	public static InventoryItemList Create(){
		InventoryItemList asset = MyScriptableObject.CreateInstance<InventoryItemList>();

		AssetDatabase.CreateAsset(asset, "Assets/InventoryItemList.asset");
		AssetDatabase.SaveAssets();
		return asset;
	}
}
