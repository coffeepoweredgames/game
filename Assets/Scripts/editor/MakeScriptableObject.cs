﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class MakeScriptableObject : MonoBehaviour {


	[MenuItem("Assets/Create/My Scriptable Object")]
	public static void CreateMyAsset(){
		MyScriptableObject asset = MyScriptableObject.CreateInstance<MyScriptableObject>();
		AssetDatabase.CreateAsset(asset, "Assets/NewScriptableObject.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();
		Selection.activeObject = asset;
	}
}
