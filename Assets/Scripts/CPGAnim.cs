﻿using UnityEngine;
using System.Collections;

public class CPGAnim : MonoBehaviour {
    protected Animator animator;
    private AudioClip coffeeSound;
    private AudioClip oneUpSound;

    void Start() {
        animator = GetComponent<Animator>();
        animator.speed = 0.085f;

        coffeeSound = Resources.Load(SoundConstants.COFFEE_POUR_SOUND) as AudioClip;

        this.GetComponent<AudioSource>().clip = coffeeSound;
        GetComponent<AudioSource>().Play();
    }

    void Update() {

        if (GetComponent<AudioSource>().isPlaying == false) {
            animator.ForceStateNormalizedTime(0.95f);
        }
    }
}