﻿using UnityEngine;

public class PromptExitPopupWindow : MonoBehaviour {
    public bool isQuit = false;
    public bool isShouldShowPopUp = false;

    void OnGUI() {
        if (isShouldShowPopUp) {
            showPrompt();
        }
    }

    private void showPrompt() {
        if (GUI.Button(new Rect(transform.parent.position.x, transform.parent.position.y, 50, 40), "No")) {
            isShouldShowPopUp = false;
        }

        if (GUI.Button(new Rect(transform.parent.position.x + 50, transform.parent.position.y, 50, 40), "Yes")) {
            Debug.Log("YES EXIT GAME");
            Application.Quit();
        }

    }

    void OnMouseEnter() {
        //mouseover color
        GetComponent<Renderer>().material.color = Color.red;
    }

    void OnMouseExit() {
        //default color
        GetComponent<Renderer>().material.color = Color.white;
    }

    void OnMouseUp() {
        isShouldShowPopUp = true;

    }

}

