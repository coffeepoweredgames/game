﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Hud))]
[RequireComponent(typeof(SpySchoolHudLogic))]
public class SpySchoolHud : MonoBehaviour {
    public Hud hud { get; set; }


    void Awake() {
        hud = GetComponent<Hud>();

    }

}
