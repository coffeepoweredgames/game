using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AgentHud : GameBase{

	public Text GetName(){
		return gameObject.transform.Find("Name").gameObject.GetComponentInChildren<Text>();
	}

	public Text GetLocation(){
		return gameObject.transform.Find("Location").gameObject.GetComponent<Button>().GetComponentInChildren<Text>();
	}
}

