﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Hud))]
[RequireComponent(typeof(StructureMisssionPickerLogic))]
public class StructureMisssionPickerHud : MonoBehaviour {
    public Hud hud { get; set; }
    public StructureMisssionPickerLogic logic { get; set; }
    public Text structureName { get; set; }

    void Awake() {
        hud = GetComponent<Hud>();
        logic = GetComponent<StructureMisssionPickerLogic>();
        structureName = transform.FindChild("Container").FindChild("Top").GetComponentInChildren<Text>();
    }

    public string GetStructureName() {
        return structureName.text;
    }

    public void SetStructureName(string name) {
        structureName.text = name;
    }

}
