﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RowAgentsSummary : MonoBehaviour {
    public Agent agent { get; set; }
    public Image portrait { get; set; }
    public Text codename { get; set; }
    public Text healthStatus { get; set; }
    public Text xp { get; set; }
    public Text health { get; set; }
    public Text identity { get; set; }
    public Text location { get; set; }
    public Text destroyedStructureCount { get; set; }

    void OnEnable() {
        portrait = transform.FindChild("Portrait").GetComponent<Image>();
        codename = transform.FindChild("Codename").GetComponentInChildren<Text>();
        healthStatus = transform.FindChild("HealthStatus").GetComponentInChildren<Text>();
        xp = transform.FindChild("XP").GetComponentInChildren<Text>();
        health = transform.FindChild("Health").GetComponentInChildren<Text>();
        identity = transform.FindChild("Identity").GetComponentInChildren<Text>();
        location = transform.FindChild("Location").GetComponentInChildren<Text>();
        destroyedStructureCount = transform.FindChild("DestroyedStructures").GetComponentInChildren<Text>();
    }

    public void SetValue(Agent agent) {
        this.portrait.overrideSprite = SpriteProvider.getSprite(agent.descriptor.portraitId).sprite;
        this.codename.text = agent.descriptor.agentName;
        this.healthStatus.text = agent.health.GetHealthStatus().ToString();
        this.healthStatus.color = agent.health.GetHealthStatusColor();

        this.xp.text = agent.xp.xp + "";
        this.health.text = agent.health.health + "";
        this.identity.text = "Hidden";
        this.location.text = agent.location.country != null ? agent.location.country.name : null;

        if (StatisticsManager.Instance.areStatsTracked(agent)) {
            this.destroyedStructureCount.text = StatisticsManager.Instance.TRACKED_AGENTS[agent.descriptor].structuresDestroyedCount.ToString();
        }
    }

    public void Refresh() {
        this.SetValue(this.agent);
    }




}
