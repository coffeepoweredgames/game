﻿using UnityEngine;
using System.Collections.Generic;

public class TableAgentsSummary : MonoBehaviour {
    private Transform parent;
    private Transform tableContent;
    private HashSet<AgentDescriptor> agentsPresentInTable;
    private GameObject scrollBar;

    void Awake() {
        parent = gameObject.transform;
        tableContent = GetComponentInChildren<ContentTableAgentsSummary>().transform;
        agentsPresentInTable = new HashSet<AgentDescriptor>();
    }

    void Start() {
        scrollBar = transform.parent.FindChild("Scrollbar").gameObject;
        scrollBar.SetActive(false);
        InvokeRepeating(CoroutineConstants.REFRESH_AGENTS_SUMMARY_TABLE, 0, 2); //TODO use coroutine
    }

    public void AddRow(RowAgentsSummary row) {

        if (agentsPresentInTable.Contains(row.agent.descriptor)) {
            print("AGENT ALREADY PRESENT IN TABLE.");
            Destroy(row.gameObject);
            return;
        }

        agentsPresentInTable.Add(row.agent.descriptor);
        row.transform.SetParent(tableContent.transform);
    }

    public void RefreshAgentsSummaryTable() {
        RowAgentsSummary[] rows = tableContent.GetComponentsInChildren<RowAgentsSummary>();

        foreach(RowAgentsSummary r in rows) {

            if (r.name.Equals("DummyRowToIgnoreHeaderScrollingWithContent")) { continue; }

            r.Refresh();
        }

        if (agentsPresentInTable.Count >= 11) {
            scrollBar.SetActive(true);
        }
    }



    
}
