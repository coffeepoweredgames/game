﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Hud))]
[RequireComponent(typeof(AgentStatusHudLogic))]
public class AgentStatusHud : GameBase {
    public Hud hud { get; set; }
    public Image agentPortrait { get; set; }
    public Text agentName { get; set; }

    void Awake() {
        hud = GetComponent<Hud>();

        //TODO find is only quick n dirty
        agentName = transform.FindChild("Container").FindChild("TopImage").GetComponentInChildren<Text>();
        agentPortrait = transform.FindChild("Container").FindChild("BottomImage").GetComponentInChildren<Image>();
    }

    public Text GetName() {
        return gameObject.transform.Find("Name").gameObject.GetComponentInChildren<Text>();
    }

    public Text GetLocation() {
        return gameObject.transform.Find("Location").gameObject.GetComponent<Button>().GetComponentInChildren<Text>();
    }

    public string getAgentName() {
        return agentName.text;
    }

    public void setAgentName(string name) {
        agentName.text = name;
    }

}
