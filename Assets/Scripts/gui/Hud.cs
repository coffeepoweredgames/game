
using UnityEngine;
using UnityEngine.UI;
using System;

public class Hud : MonoBehaviour, IPositionable {
    private Positioner positioner;

    void OnEnable() {
        positioner = Positioner.Instance;
    }

    void Start() {
        RegisterGraphicRaycaster();
    }


    public void Activate(Transform parent) { 
    
    }

    public void Clear() { 
    }

    public void aboveOf(GameObject aboveMe) {
        if (aboveMe == null) { return; }
        positioner.above(gameObject, aboveMe);
    }

    public void beneathOf(GameObject beneathMe) {
        if (beneathMe == null) { return; }
        positioner.above(gameObject, beneathMe);
    }

    public void aboveOf(Component aboveMe) {
        if (aboveMe == null) { return; }
        positioner.above(gameObject, aboveMe.gameObject);
    }

    public void beneathOf(Component c) {
        throw new NotImplementedException();
    }

    public void leftOf(GameObject o) {
        throw new NotImplementedException();
    }

    public void rightOf(GameObject o) {
        throw new NotImplementedException();
    }

    public void leftOf(Component c) {
        throw new NotImplementedException();
    }

    public void rightOf(Component c) {
        throw new NotImplementedException();
    }

    private void RegisterGraphicRaycaster() {
       GraphicRaycaster raycaster = GetComponent<GraphicRaycaster>();

       if (raycaster != null) {
            MapSelector.Instance.RegisterGraphicRaycaster(GetComponent<GraphicRaycaster>());
       
       }
    }
}


