using UnityEngine;

[RequireComponent(typeof(Hud))]
[RequireComponent(typeof(CountryBorderHudLogic))]
public class CountryBorderHud : GameBase {

	public Hud hud {get; set;}//hud
	public CountryBorderHudLogic logic {get; set;}//logic
	
	void Awake(){
		hud = GetComponent<Hud>();
		logic = GetComponent<CountryBorderHudLogic>();
	}

}
