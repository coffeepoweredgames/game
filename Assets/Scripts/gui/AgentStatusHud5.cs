﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


[RequireComponent(typeof(Hud))]
[RequireComponent(typeof(AgentStatusHudLogic))]
public class AgentStatusHud5 : GameBase {
    public Hud hud { get; set; }
    public Text agentName { get; set; }
    public ProgressBar healthBar { get; set; }
    public ProgressBar xpBar { get; set; }

    void Awake() {
        hud = GetComponent<Hud>();
        xpBar = transform.FindChild("Container").FindChild("ExpBar").GetComponent<ProgressBar>();
        healthBar = transform.FindChild("Container").FindChild("HealthBar").GetComponent<ProgressBar>();
		agentName = transform.FindChild("Container").FindChild("TopImage").GetComponentInChildren<Text>();
    }

    public Text GetName() {
        return gameObject.transform.Find("Name").gameObject.GetComponentInChildren<Text>();
    }

    public Text GetLocation() {
        return gameObject.transform.Find("Location").gameObject.GetComponent<Button>().GetComponentInChildren<Text>();
    }

    public string getAgentName() {
        return agentName.text;
    }

    public void setAgentName(string name) {
        agentName.text = name;
    }


    /// <summary>
    /// Progress bars must be reset
    /// </summary>
    /// <param name="agent"></param>
    public void clear(Agent agent) {
        agentName.text = "";

        if (healthBar) { 
            healthBar.SetValue(0f);
        }

        if (xpBar) {//TODO check, progres bars in this component should never be null, maybe only upon bootstrap
            xpBar.SetValue(0f);
        }
    }
}
