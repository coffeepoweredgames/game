﻿using UnityEngine;

[RequireComponent(typeof(Hud))]
public class HelpPanel : MonoBehaviour {

    public void ShowHelpPanel() {
        gameObject.SetActive(!gameObject.activeSelf);
    }

}
