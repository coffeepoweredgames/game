﻿using UnityEngine;
using System.Collections;

public class MainMenuScreenLogic : GameBase {

    public void resume() {
        GetScreenManager().loadLevelOne();
    }

    public void play() {
        GetScreenManager().loadLevelOne();
    }

    public void options() {
        print("options clicked");
    }

    public void credits() {
        print("load credits screen");
        GetScreenManager().loadCreditsScreen();
    }

    public void exitGame() {
        GetScreenManager().exitGame();
    }
}
