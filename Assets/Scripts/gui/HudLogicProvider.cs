using UnityEngine;


[RequireComponent(typeof(AgentStatusHudLogic))]
[RequireComponent(typeof(CountryBorderHudLogic))]
[RequireComponent(typeof(CountryHudLogic))]
[RequireComponent(typeof(SpySchoolHudLogic))]
public class HudLogicProvider : GameBase {

}
