using UnityEngine;
using System.Collections;


/// <summary>
/// Used for ingame menu pop-ups.
/// Ingame menu != main menu screen
/// </summary>
public class MainMenuScript : GameBase {
	
	public void loadMainMenu(){
		GetScreenManager().loadMenuScreen();
	}

	public void resumeGame(){
		GetGuiManager().showMainMenu();
	}

	public void exitGame(){
		GetScreenManager().exitGame();
	}
}
