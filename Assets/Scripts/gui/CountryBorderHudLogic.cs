using UnityEngine;

public class CountryBorderHudLogic : GameBase{
	public Country country {get; set;}
	public Province province {get; set;}
	public Agent agent {get; set;}
	
	public void BorderAccept(){
		GetMover().movementManager.CountryBorderAccept(agent, province);
	}

	public void BorderDeny(){
		GetMover().movementManager.CountryBorderDeny(agent, province);
	}

	public void BorderRandomAcceptal(){
		if (Random.Range(0, 2) == 0){
			GetMover().movementManager.CountryBorderAccept(agent, agent.location.province);
			return;
		}
		GetMover().movementManager.CountryBorderDeny(agent, agent.location.province);
	}	
}