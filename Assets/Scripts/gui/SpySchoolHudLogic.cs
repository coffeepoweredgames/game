using UnityEngine;
using System.Collections;

public class SpySchoolHudLogic : GameBase {

    private Country britain;
    private Province province;

    private int AGENT_TRAIN_LIMIT = 8;

    //void Start() {
    //    britain = DynamicObjectRegistry.COUNTRIES[Countries.BRITAIN.name];
    //    province = britain.provinces.GetComponentInChildren<Province>();
    //}

    public void Train() {
        if (britain == null) {
            britain = DynamicObjectRegistry.COUNTRIES[Countries.BRITAIN.name];
        }

        if (province == null) {
            province = britain.provinces.GetComponentInChildren<Province>();
        }

        int agentCount = province.agents.GetAgentCount();

        if (agentCount >= AGENT_TRAIN_LIMIT) {
            Debug.Log("Agent train limit reached!");
            return;

        }

        Agent agent = createAgent(province);

        alignAgentsIntoRows(agentCount, agent, province);//TODO temp prototype

    }

    public void Recruit() {
        Debug.Log("Agent recruited for spy school!");
    }

    private Agent createAgent(Province province) {
        Mover agentMover = GetMover();
        Agent createdAgent = AgentFactory.Instance.Build(StringGenerator.getRandomName(), StringGenerator.getRandomSurname(), StringGenerator.getRandomPortraitName());
        agentMover.AddAgentToProvince(createdAgent, province);

        Notifier.Instance.notify(EventFactory.Build("Agent created", createdAgent.descriptor.name.ToUpper()));

        return createdAgent;
    }
    /// <summary>
    /// Agent is parented to agents aka. the province he is in.
    /// His position is at province pivot.
    /// We adjust his local coordinate, because we take into account he is in Britain.
    /// No sense to adjust his world space coord.
    /// </summary>
    private void alignAgentsIntoRows(int agentCount, Agent agent, Province province) {//TODO test purpose
        agent.transform.localPosition = Vector3.zero;
        Vector3 positionRelativeToBritainProvincePivot = new Vector3(2f, 1f, 0f);//for Britain only
        agent.transform.localPosition = agent.transform.localPosition + positionRelativeToBritainProvincePivot + (0.4f) * (agentCount % 4) * Vector3.right + (0.4f) * (agentCount / 4) * Vector3.down;
    }
}
