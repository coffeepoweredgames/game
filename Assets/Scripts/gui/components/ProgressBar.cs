﻿using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {
    public Slider slider { get; set; }
    private Image colorOwner;
    private Text pointsText;

    void Awake() {
        colorOwner = transform.FindChild("Fill Area").FindChild("Fill").GetComponent<Image>();
		pointsText  = transform.GetComponentInChildren<Text>();
        slider = GetComponent<Slider>();
    }

    public void SetColor(Color color) {
        colorOwner.color = color;
    }

    public Color GetColor() {
        return colorOwner.color;
    }

    public float GetValue() {
        return slider.value;

    }
    public void SetValue(float value) {
        slider.value = value;
    }

    public void SetPointsText(float current, float max) {
        this.pointsText.text = string.Format("{0}/{1}", current, max);
    }

    public string GetPointsText(string s) {
        return pointsText.text;
    }

    public Color GetPointsTextColor() {
        return pointsText.color;
    }

    public void SetPointsTextColor(Color color){
        this.pointsText.color = color;
    }

    public void SetValueRange(int min, int max) {
        slider.minValue = min;
        slider.maxValue = max;
    }

}

