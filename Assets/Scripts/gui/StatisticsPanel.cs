﻿using UnityEngine;
using System.Collections;

public class StatisticsPanel : MonoBehaviour {
    private RectTransform panelRectTransform;
    private bool isSlidingDown = false;
    private bool isSlideUp = false;
    public TableAgentsSummary tableAgentsSummary { get; set; }

    void Start() {
        panelRectTransform = (RectTransform)transform.GetComponent<RectTransform>();
        tableAgentsSummary = GetComponentInChildren<TableAgentsSummary>();
    }

    void Update() {

        if (isSlidingDown) {
            float y_magnitude = Mathf.MoveTowards(panelRectTransform.anchoredPosition.y, -500, 20);

            panelRectTransform.anchoredPosition = new Vector2(panelRectTransform.anchoredPosition.x, y_magnitude);
        }

        if (isSlidingDown && panelRectTransform.anchoredPosition.y == -500) {
            isSlidingDown = false;
        }

        if (isSlideUp) {
            float y_magnitude = Mathf.MoveTowards(panelRectTransform.anchoredPosition.y, 0, 20);

            panelRectTransform.anchoredPosition = new Vector2(panelRectTransform.anchoredPosition.x, y_magnitude);
        }

        if (isSlideUp && panelRectTransform.anchoredPosition.y == 0) {
            isSlideUp = false;
        }

    }


    public void slideDown() {
        isSlidingDown = true;
    }

    public void slideUp() {
        isSlideUp = true;
    }

}
