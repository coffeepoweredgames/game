﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryItemList : ScriptableObject {
	public List<InventoryItem> itemList = new List<InventoryItem>();
	public string name;

	[SerializeField]
	private string privateName;

}
