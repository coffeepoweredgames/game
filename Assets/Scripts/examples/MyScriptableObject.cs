﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MyScriptableObject : ScriptableObject {
	public string objectName = "New ScriptableObject";
	public bool colorIsRandom = false;
	public Color thisColor = Color.white;
	public Vector3[] spawnPooints;
}
