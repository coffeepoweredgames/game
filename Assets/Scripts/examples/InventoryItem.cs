﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class InventoryItem : MonoBehaviour {
	public string itemName = "New Item";
	public Texture2D itemIcon = null;
	public bool isUnique = false;
	public bool isQuestItem = false;

}
