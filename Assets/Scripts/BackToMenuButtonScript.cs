﻿using UnityEngine;
using System.Collections;

public class BackToMenuButtonScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    void OnMouseEnter()
    {
        //change text color
        GetComponent<Renderer>().material.color = Color.red;
    }

    void OnMouseExit()
    {
        //change text color
        GetComponent<Renderer>().material.color = Color.white;
    }

    void OnMouseUp()
    {
        //Application.LoadLevel(gameObject.AddComponent<Levels>().getMainMenuScreen());
    }

}
