﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Generic queue with custom features.
/// Fixed size.
/// </summary>
/// <typeparam name="T"></typeparam>
public class FixedSizedQueue<T> {
    private Queue<T> queue = new Queue<T>();
    public int Size { get; private set; }
    public int Count { get { return queue.Count; } }

    public FixedSizedQueue(int size) {
        Size = size;
        this.queue = new Queue<T>(Size);
    }

    /// <summary>
    /// Enqueues new elements, and returns list of evicted elements
    /// because of limited capacity.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public List<T> Enqueue(T obj) {
        List<T> dequedElements = null;
        queue.Enqueue(obj);

        while (queue.Count > Size) {
            dequedElements = new List<T>();
            dequedElements.Add(queue.Dequeue());
        }

        return dequedElements;
    }
}
