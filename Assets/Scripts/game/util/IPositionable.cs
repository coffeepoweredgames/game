
using UnityEngine;

public interface IPositionable{

	void aboveOf(GameObject o);
	
	void beneathOf(GameObject o);
	
	void leftOf(GameObject o);
	
	void rightOf(GameObject o);

	void aboveOf(Component c);
	
	void beneathOf(Component c);
	
	void leftOf(Component c);
	
	void rightOf(Component c);
}
