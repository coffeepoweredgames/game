using UnityEngine;
using System;


public class GameLogger {

	public bool isLog;

	public GameLogger(){
		isLog = true;
	}

	public GameLogger(bool isLog, Type clazz){
        //Debug.Log (string.Format("Logging for class {0} {1}!", clazz, (isLog ? "enabled" : "disabled")));
		this.isLog = isLog;	
	}

	public GameLogger(bool isLog){
		this.isLog = isLog;	
	}

	public void Log(string message){
		if(isLog){
			Debug.Log(message);
		}

	}

    public static void printBlue(String message)
    {
        Debug.Log("<color=blue>" + message + "</color>\n");
    }

    public static void printRed(String message){
		Debug.Log("<color=red>"+message+"</color>\n");
	}

	public static void printGreen(String message){
		Debug.Log("<color=green>"+message+"</color>\n");
	}

}
