﻿using UnityEngine;
using System.Collections;

/// <summary>
/// THERE CAN BE ONLY ONE!
/// Dont override Awake in children, use OnEnable instead of Start in children to init data.
/// </summary>
public class Singleton<T> : MonoBehaviour where T : Component {
    private static T instance;

    public static T Instance {
        get {

            if (instance == null) {
                GameObject obj = new GameObject();
                obj.name = "_" + typeof(T).FullName + "Singleton";
                instance = obj.AddComponent<T>();

                return instance;
            }

            return instance;
        }
    }

    public virtual void Awake() {
        if (instance == null) {
            instance = this as T;
            return;
        }

        if (instance != this) {
            Destroy(gameObject);
        }
    }



}