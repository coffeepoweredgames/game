﻿using System;
using UnityEngine;

class SpriteDescriptor {

    public int id { get; set; }
    public Sprite sprite { get; set; }

    public SpriteDescriptor(int id, Sprite sprite) {
        this.id = id;
        this.sprite = sprite;
    }


}
