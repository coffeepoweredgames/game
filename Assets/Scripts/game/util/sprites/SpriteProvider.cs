﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

class SpriteProvider {

    private static Dictionary<int, Sprite> sprites = new Dictionary<int, Sprite>{
        {1, Resources.Load<Sprite>("game/agent/portraits/agent1")},
        {2, Resources.Load<Sprite>("game/agent/portraits/agent2")},
        {3, Resources.Load<Sprite>("game/agent/portraits/agent3")}
    };


    public static SpriteDescriptor getSprite() {
        int id = UnityEngine.Random.Range(1, sprites.Count + 1);
        Sprite s = sprites[id];
        return new SpriteDescriptor(id, s);
    }

    public static SpriteDescriptor getSprite(int id) {
        Sprite s = sprites[id];
        return new SpriteDescriptor(id, s);
    }


    //TODO request a portrait by agent name, or return random one

}
