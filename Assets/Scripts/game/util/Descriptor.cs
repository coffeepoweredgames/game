using UnityEngine;

public class Descriptor {
	
	public int id {get;  set;}
	
	public string name {get;  set;}
	
	public Descriptor (int id, string name){
		this.id = id;
		this.name = name;
	}
	
	
}

