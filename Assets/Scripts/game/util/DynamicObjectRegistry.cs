﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// References to dynamically created object cannot be saved.
/// Use this (name, reference) lookup when loading data to acquire dynamic references .i.e get the province your agent is located by name when loading
/// </summary>
public class DynamicObjectRegistry {
    public static Dictionary<string, GameObject> STRUCTURES = new Dictionary<string, GameObject>();
    public static Dictionary<string, Country> COUNTRIES = new Dictionary<string, Country>();// keys should be descriptors
    public static Dictionary<string, Province> PROVINCES = new Dictionary<string, Province>();
    public static Dictionary<string, City> CITIES = new Dictionary<string, City>();
    public static Dictionary<string, Agent> AGENTS = new Dictionary<string, Agent>();//TODO: here are only dynamically created ones, old ones are children of Agents components!!


    public static void AddCountry(CountryDescriptor countryDescriptor, Country country) {
        if (!countryDescriptor.isValid()) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: Cannot add invalid {0} country descriptor!", countryDescriptor.countryName));
            return;
        }

        if (COUNTRIES.ContainsKey(countryDescriptor.countryName)) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: Country {0} already present!", countryDescriptor.countryName));
            return;
        }

        COUNTRIES.Add(countryDescriptor.countryName, country);
    }

    public static Country GetCountry(CountryDescriptor countryDescriptor) {
        if (!countryDescriptor.isValid()) {
            return null;
        }

        if (!COUNTRIES.ContainsKey(countryDescriptor.countryName)) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: country {0} not found", countryDescriptor.countryName));
            return null;
        }

        return COUNTRIES[countryDescriptor.countryName];
    }

    public static Country GetCountry(CountryDescriptorData countryDescriptor) {
        if (!countryDescriptor.isValid()) {
            return null;
        }

        if (!COUNTRIES.ContainsKey(countryDescriptor.countryName)) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: country {0} not found", countryDescriptor.countryName));
            return null;
        }

        return COUNTRIES[countryDescriptor.countryName];
    }

    public static void AddProvince(ProvinceDescriptor provinceDescriptor, Province province) {
        if (!provinceDescriptor.isValid()) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: Cannot add invalid {0} province descriptor!", provinceDescriptor.provinceName));
            return;
        }

        if (PROVINCES.ContainsKey(provinceDescriptor.provinceName)) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: Province {0} already present!", provinceDescriptor.provinceName));
            return;
        }

        PROVINCES.Add(provinceDescriptor.provinceName, province);
    }

    public static Province GetProvince(ProvinceDescriptor provinceDescriptor) {
        if (!provinceDescriptor.isValid()) {
            return null;
        }

        if (!PROVINCES.ContainsKey(provinceDescriptor.provinceName)) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: province {0} not found", provinceDescriptor.provinceName));
            return null;
        }

        return PROVINCES[provinceDescriptor.provinceName];
    }

    public static Province GetProvince(ProvinceDescriptorData provinceDescriptor) {
        if (!provinceDescriptor.isValid()) {
            return null;
        }

        if (!PROVINCES.ContainsKey(provinceDescriptor.provinceName)) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: province {0} not found", provinceDescriptor.provinceName));
            return null;
        }

        return PROVINCES[provinceDescriptor.provinceName];
    }

    public static void AddAgent(AgentDescriptor agentDescriptor, Agent agent) {
        if (!agentDescriptor.isValid()) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: Cannot add invalid {0} agent descriptor!", agentDescriptor.agentName));
            return;
        }

        if (AGENTS.ContainsKey(agentDescriptor.agentName)) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: Agent {0} already present!", agentDescriptor.agentName));
            return;
        }

        AGENTS.Add(agentDescriptor.agentName, agent);
    }

    public static Agent GetAgent(AgentDescriptor agentDescriptor) {
        if (!agentDescriptor.isValid()) {
            return null;
        }

        if (!AGENTS.ContainsKey(agentDescriptor.agentName)) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: agent {0} not found", agentDescriptor.agentName));
            return null;
        }

        return AGENTS[agentDescriptor.agentName];
    }

    public static Agent GetAgent(AgentDescriptorData agentDescriptor) {
        if (!agentDescriptor.isValid()) {
            return null;
        }

        if (!AGENTS.ContainsKey(agentDescriptor.agentName)) {
            GameLogger.printRed(string.Format("DynamicObjectRegistry:: agent {0} not found", agentDescriptor.agentName));
            return null;
        }

        return AGENTS[agentDescriptor.agentName];
    }

    public static void RemoveAgent(Agent agentToRemove) {
        Agent agent = GetAgent(agentToRemove.descriptor);

        if (agent != null) {
            AGENTS.Remove(agentToRemove.descriptor.name);
        }
    }

    public static void printRegistryContents() {//TODO TEMP
        foreach (KeyValuePair<string, Country> entry in DynamicObjectRegistry.COUNTRIES) {
            Debug.Log("country: " + entry.Value.descriptor.countryName + "\n");
        }
        foreach (KeyValuePair<string, Province> entry in DynamicObjectRegistry.PROVINCES) {
            Debug.Log("province: " + entry.Value.descriptor.provinceName + "\n");
        }

        foreach (KeyValuePair<string, Agent> entry in DynamicObjectRegistry.AGENTS) {
            Debug.Log("agent: " + entry.Value.descriptor.agentName + "\n");
        }
    }
}

