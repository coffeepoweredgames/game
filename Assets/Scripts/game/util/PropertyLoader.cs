using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PropertyLoader{
	private const string PROPERTY_SEPARATOR = "\r\n";
	private const string KEY_VALUE_SEPARATOR = ":";

	string[] delimiterChars = {PROPERTY_SEPARATOR};
	
	public static Dictionary<string, List<string>> load(string propertyPath){//TODO preskacem komponentu

		TextAsset loadedFile = (TextAsset)Resources.Load(propertyPath, typeof(TextAsset));

		Dictionary<string, List<string>> map = new Dictionary<string,List<string>>();

		/*foreach(string line in loadedFile.text.Split(delimiterChars)) {
			string key = line.Split(KEY_VALUE_SEPARATOR)[0];
			string value = line.Split(KEY_VALUE_SEPARATOR)[1];

			if(map.ContainsKey(key)) {
				map[key].Add(value);
			}
			map.Add(key, new List<string>(value));
		}*/
		string[] provinces = {"FRANCE_PROVINCE_ONE","FRANCE_PROVINCE_TWO","FRANCE_PROVINCE_THREE"};
		map.Add("france.province", new List<String>(provinces));

		return map;
	}
	
}
