﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StringGenerator {

    private static List<string> names = new List<string>() {
			"Zeljko",
			"Marko",
			"Jurica",
			"Ivan",
			"John",
			"James",
			"Edward",
			"Winston",
			"Tajana",
			"Marina",
			"Milica",
			"Nives"
		};

    private static List<string> surnames = new List<string>() {
			"Kunjko",
			"Ford",
			"Dane",
			"Killian",
			"Falk",
			"Stewart",
			"Rancic",
			"Bond",
			"Kopcic",
			"Tomorad",
			"Chamberlain",
			"Helsing",
			"Holmwood"
		};

    private static List<string> portraits = new List<string>() {
			"agent1",
			"agent2",
			"agent3"
		};


    public static string getRandomName() {
        return names[Random.Range(1, names.Count)];

    }

    public static string getRandomSurname() {
        return surnames[Random.Range(1, surnames.Count)];

    }

    public static string getRandomPortraitName() {
        return portraits[Random.Range(1, portraits.Count)];

    }
}
