﻿using UnityEngine;
using System.Collections;

public class BlackColor : MonoBehaviour {
    public Color color;

    void Start() {

        if (color != null) {
            GetComponent<Renderer>().material.color = color;
            return;
        }

        GetComponent<Renderer>().material.color = Color.black;
    }

}
