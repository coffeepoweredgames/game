﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Base class for all game components.
/// Use this class to add useful functionality to all game objects.
/// Or to override existing methods with friendlier syntax/params
/// </summary>
public class GameBase : MonoBehaviour { //TODO maybe store refernces for gui manager i sl.

    //TODO add Invoke
    //TODO add GetInterfaceComponent<I>
    //TODO add FindObjectsOfInterface<I>
	
	private GameLogic gameLogic;

    /// <summary>
    /// Defensive getComponent alternative.
    /// Sometimes forcing dependencies through RequiredComponent can be a pain.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    /*public T GetSafeComponent<T>(this GameObject obj) where T : MonoBehaviour{

        T component = obj.GetComponent<T>();

        if (component = null){
            Debug.LogError("Expected to find component of type" + typeof(T) + " but found none", obj);

        }

        return component;
    }*/

//	public static T FindObjectOfType <T> (this Object unityObject) where T : Object {
//		return Object.FindObjectOfType(typeof(T)) as T;
//	}
//	public static T[] FindObjectsOfType <T> (this Object unityObject) where T : Object {
//		return Object.FindObjectsOfType(typeof(T)) as T[];
//	}

    /// <summary>
    /// Access component in sibling.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj">Object which needs component</param>
    /// <returns></returns>
    public T GetComponentInSibling<T>() where T : MonoBehaviour{
        GameObject parent = this.transform.parent.gameObject;

        if (parent == null) {
            Debug.LogError("Missing parent. Cannot access sibling component!!");
        }

        T[] component = parent.gameObject.GetComponentsInChildren<T>();

        if (component[0] == null){
            Debug.LogError("Failed to find sibling component of type" + typeof(T));
        }
        else {
            Debug.Log("Found component of type " + typeof(T));
        }

        return component[0];
    }

	public GameObject GetGame(){
		return this.transform.root.gameObject;
	}

	public GuiManager GetGuiManager(){
		return GuiManager.Instance;
	}

	public ScreenManager GetScreenManager(){
		return ScreenManager.Instance;
	}

	public InputManager GetInputManager(){
		return InputManager.Instance;
	}

	public MapSelector GetMapSelector(){
		return MapSelector.Instance;
	}

	public GameLogic GetGameLogic(){
		if(gameLogic != null){
			return gameLogic;
		}

		gameLogic = GetGame().GetComponentInChildren<GameLogic>();

		return gameLogic;
	}	

	public Positioner GetPositioner(){
		return Positioner.Instance;
	}

	public Mover GetMover(){
		return Mover.Instance;
	}
}
