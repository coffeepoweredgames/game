﻿using UnityEngine;

public class StructureMisssionPickerLogic : GameBase {
    public Structure structure { get; set; }
    public Agent agent { get; set; }


    public void Infiltrate() {
        //print(string.Format("Agent {0} tries to INFILTRATE {1} structure", agent.descriptor.agentName, structure.structureDescriptor.structureName));

        MissionGoal goal1 = MissionGoalFactory.Instance
        .withDurationSeconds(3)
        .withRetryCount(0)
        .withMissionCritical(false)
        .withGoalName("Goal " + "1")
        .BuildInfiltrateStructureGoal(structure);

        Mission infiltrationMission = MissionFactory.Instance.Build("Infiltrate " + structure.name, MissionType.DESTROY, structure, goal1);

        bool enrollmentResult = MissionManager.Instance.Enroll(agent, infiltrationMission);
        adjustHUDs(enrollmentResult);
    }


    public void Sabotage() {
        //print(string.Format("Agent {0} tries to SABOTAGE {1} structure", agent.descriptor.agentName, structure.structureDescriptor.structureName));

        MissionGoal goal1 = MissionGoalFactory.Instance
        .withDurationSeconds(3)
        .withRetryCount(0)
        .withMissionCritical(false)
        .withGoalName("Goal " + "1")
        .BuildSabotageStructureGoal(structure);

        Mission sabotageMission = MissionFactory.Instance.Build("Sabotage " + structure.name, MissionType.DESTROY, structure, goal1);

        bool enrollmentResult = MissionManager.Instance.Enroll(agent, sabotageMission);
        adjustHUDs(enrollmentResult);
    }


    public void Destroy() {
        //print(string.Format("Agent {0} tries to DESTROY {1} structure", agent.descriptor.agentName, structure.structureDescriptor.structureName));

        MissionGoal goal1 = MissionGoalFactory.Instance
             .withDurationSeconds(3)
             .withRetryCount(0)
             .withMissionCritical(false)
             .withGoalName("Goal " + "1")
             .BuildDestroyStructureGoal(structure);

        Mission destructionMission = MissionFactory.Instance.Build(
            "Destroy " + structure.name,
            MissionType.DESTROY,
            structure,
            goal1
            );

        bool enrollmentResult = MissionManager.Instance.Enroll(agent, destructionMission);
        adjustHUDs(enrollmentResult);
    }

    private void adjustHUDs(bool shouldAdjustHUDs) {
        if (shouldAdjustHUDs) {
            print("ADJUST HUD!");
            GuiManager.Instance.showMissionPickerHud(null, null, false);
            GuiManager.Instance.showAgentHud(agent, true);
            GuiManager.Instance.showStructureStatusHud(structure, true);
            MapSelector.Instance.DeselectStructure(structure);
            structure.trigger.displayMissionPickerHud = false;
        }
    }

}