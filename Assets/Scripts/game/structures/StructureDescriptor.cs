﻿using System;
using UnityEngine;

public class StructureDescriptor : MonoBehaviour {
    public int id { get; set; }
    public string structureName { get; set; }
}
