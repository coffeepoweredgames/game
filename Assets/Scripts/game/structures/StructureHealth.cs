﻿using UnityEngine;
using System;

/// <summary>
/// Health can be incremented or decremented
/// </summary>
public class StructureHealth : MonoBehaviour, IHealth {
    public float health { get; set; }
    private Structure structure { get; set; }

    void Start() {
        structure = GetComponent<Structure>();
    }

    public void ApplyHealing(float healingAmount) {
        health += healingAmount;//TODO clamp

        print("APPLIED " + healingAmount + " amount of repair to structure");

        refreshProgressBar();
    }

    public void ApplyDamage(float damage) {
        health -= damage;

        health = Mathf.Clamp(health, 0, StructureConstants.HEALTH_NORMAL);

        print("APPLIED " + damage + " amount of damage to structure");

        refreshProgressBar();
    }

    public bool isAlive() {
        return health > 0;
    }

    public bool isHealthy() {
        return health >= StructureConstants.HEALTH_STABLE;
    }

    public bool isStable() {
        return (health > StructureConstants.HEALTH_CRITICAL && health < StructureConstants.HEALTH_STABLE);
    }

    public bool isCritical() {
        return health <= StructureConstants.HEALTH_CRITICAL;
    }

    public HealthStatus GetHealthStatus() {

        if (isStable()) {
            return HealthStatus.STABLE;
        }

        if (isCritical()) {
            return HealthStatus.CRITICAL;
        }

        return HealthStatus.HEALTHY;
    }

    private void refreshProgressBar(float health, Color color) {
        if (structure.healthBar == null) {
            return;
        }

        if (!structure.healthBar.GetColor().Equals(color)) {
            structure.healthBar.SetColor(color);
        }

        structure.healthBar.SetValue(health);

        print("Updating health of " + structure.structureDescriptor.structureName + " to " + health + ". Bar display value: " + structure.healthBar.GetValue());
    }

    public void refreshProgressBar() {
        refreshProgressBar(health, getHealthStatusColor());
    }

    private Color getHealthStatusColor() {
        if (structure.health == null) {
            return ColorConstants.PINK_COLOR;//TODO something better
        }

        switch (GetHealthStatus()) {
            case HealthStatus.HEALTHY:
                return ColorConstants.HEALTH_HEALTHY;
            case HealthStatus.STABLE:
                return ColorConstants.HEALTH_STABLE;
            default:
                return ColorConstants.HEALTH_CRITICAL;
        }

    }
}
