﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(StructureDescriptor))]
[RequireComponent(typeof(LocationDescriptor))]
[RequireComponent(typeof(StructureTrigger))]
[RequireComponent(typeof(StructureHealth))]
[RequireComponent(typeof(StructureLogic))]
[RequireComponent(typeof(Intelligence))]
public class Structure : MonoBehaviour, IGuiOwner {
    public StructureDescriptor structureDescriptor { get; set; }
    public LocationDescriptor location { get; set; }
    public ProgressBar intelBar { get; set; }
    public ProgressBar healthBar { get; set; }
    public StructureHealth health { get; set; }
    public Intelligence intel { get; set; }
    public StructureTrigger trigger { get; set; }
    private List<StructureEventListener> structureEventListeners = new List<StructureEventListener>();

    void Awake() {
        structureDescriptor = GetComponent<StructureDescriptor>();
        location = GetComponent<LocationDescriptor>();
        health = GetComponent<StructureHealth>();
        intel = GetComponent<Intelligence>();
        trigger = GetComponent<StructureTrigger>();
    }

    void Start() {
        structureEventListeners.Add(StatisticsManager.Instance);
    }

    public void refreshGui() {
        health.refreshProgressBar();
        intel.refreshProgressBar();
    }

    public void OnStructureCreated(Structure structure) {
        structureEventListeners.ForEach(listener => listener.OnStructureCreated(this));
    }

    public void OnStructureDestroyed(Agent agent) {
        structureEventListeners.ForEach(listener => listener.OnStructureDestroyed(this, agent));

        //will cause an explosion to appear here
        Exploder.Instance.explodeHere(this.transform);

        MapSelector.Instance.DeselectStructure(this);
        GuiManager.Instance.showStructureStatusHud(this, false);

        //destroy structure physically
        DestroyImmediate(this.gameObject, true);
    }



}
