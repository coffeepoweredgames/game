﻿using UnityEngine;
using System.Collections;

interface StructureEventListener {
    void OnStructureCreated(Structure structure);
    void OnStructureDestroyed(Structure structure, Agent agent);
}
