﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(StructureStatusHudLogic))]
[RequireComponent(typeof(Hud))]
public class StructureStatusHud : MonoBehaviour {
    public Hud hud { get; set; }
    public StructureStatusHudLogic logic { get; set; }
    public Text structureName { get; set; }//why not structure descriptor here
    public ProgressBar healthBar { get; set; }
    public ProgressBar intelBar { get; set; }

    void Awake() {
        hud = GetComponent<Hud>();
        logic = GetComponent<StructureStatusHudLogic>();
        intelBar = transform.FindChild("Container").FindChild("ExpBar").GetComponent<ProgressBar>();
        healthBar = transform.FindChild("Container").FindChild("HealthBar").GetComponent<ProgressBar>();
        structureName = transform.FindChild("Container").FindChild("TopImage").GetComponentInChildren<Text>();
    }

    public string GetStructureName() {
        return structureName.text;
    }

    public void SetStructureName(string name) {
        structureName.text = name;//TODO status bar refactor
    }

    public void clear(Structure structure) {
        structureName.text = "";//belong to HUD

        if (healthBar) {//belong to HUD
            healthBar.SetValue(0);
        }

        if (intelBar) {//belongs to HUD
            intelBar.SetValue(0);
        } 

        if (structure) {
            structure.healthBar = null;
            structure.intelBar = null;
        }
    }

}

