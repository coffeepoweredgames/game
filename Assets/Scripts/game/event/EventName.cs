﻿using UnityEngine;
using UnityEngine.UI;

public class EventName : MonoBehaviour {
    public Text textComponent { get; set; }

    void OnEnable() {
        textComponent = GetComponentInChildren<Text>();
    }
}
