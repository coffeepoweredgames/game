﻿using UnityEngine;

using System.Collections.Generic;
using System.Linq;

public class GameEventQueue : MonoBehaviour {

    private FixedSizedQueue<GameEvent> fixSizedQueue;

    void Awake() {
        fixSizedQueue = new FixedSizedQueue<GameEvent>(GuiConstants.EVENT_PANEL_SIZE);
        GameLogger.printRed("Queue CREATED!");
    }

    public void Enqueue(GameEvent gameEvent){
        List<GameEvent> obsoleteEvents = fixSizedQueue.Enqueue(gameEvent);

		gameEvent.transform.SetParent(gameObject.transform);

        if (obsoleteEvents != null && obsoleteEvents.Count > 0) {
            obsoleteEvents.ToList().ForEach(x => GameLogger.printRed("Event "+ x.name + "will be removed from event queue!"));
            obsoleteEvents.ToList().ForEach(x => Destroy(x.gameObject));
        }
    }

	public int Count{
		get {

        //GameLogger.printBlue("fixSizedQueue.Count!: "+fixSizedQueue);
            return fixSizedQueue.Count;}
	}
}