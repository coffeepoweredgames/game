﻿using UnityEngine;
using UnityEngine.UI;

public class EventStatus : MonoBehaviour {
    public Text textComponent { get; set; }

    void OnEnable() {
        textComponent = GetComponentInChildren<Text>();
    }
}