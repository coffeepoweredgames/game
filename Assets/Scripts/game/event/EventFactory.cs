﻿using UnityEngine;
using System.Collections;

public class EventFactory {
    private static Object gameEvent = Resources.Load(PrefabConstants.GAME_EVENT_PREFAB);
    private static Object missionGoalEvent = Resources.Load(PrefabConstants.GAME_EVENT_GOAL_PREFAB);
    private static Object missionEvent = Resources.Load(PrefabConstants.GAME_EVENT_MISSION_PREFAB);

    //general game event
    public static GameEvent Build(string eventName, string eventStatus) {
        return Build(gameEvent, eventName, eventStatus);
    }

    public static GameEvent Build(MissionProgress missionProgress) {
        return Build(
            missionEvent,
            missionProgress.mission.descriptor.missionName,
            missionProgress.status == MissionProgressStatus.SUCCESS ? "SUCCESS" : (missionProgress.status == MissionProgressStatus.FAILURE ? "FAILURE" : "STARTED"));
    }

    public static GameEvent Build(MissionGoal goal) {
        GameEvent gameEvent = Build(
            missionGoalEvent,
            goal.goalName,
            goal.isSuccess() ? "SUCCESS" : "FAILURE");

        return gameEvent;
    }

    private static GameEvent Build(Object prefab, string eventName, string eventStatus) {
        GameEvent gameEvent = (MonoBehaviour.Instantiate(prefab) as GameObject).GetComponent<GameEvent>();
        gameEvent.gameObject.name = "EVENT_" + eventName;
        gameEvent.setEventName(eventName);
        gameEvent.setEventStatus(eventStatus);
        return gameEvent;
    }



}
