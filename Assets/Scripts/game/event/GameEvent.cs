﻿using UnityEngine;
using UnityEngine.UI;

public class GameEvent : MonoBehaviour {
    private Text eventName;
    private Text eventStatus;

    void OnEnable() {
        eventName = GetComponentInChildren<EventName>().GetComponentInChildren<Text>();
        eventStatus = GetComponentInChildren<EventStatus>().GetComponentInChildren<Text>();
    }

    public void setEventName(string name) {
        eventName.text = name;
    }

    public string getEventName() {
        return eventName.text;
    }

    public void setEventStatus(string status) {
        eventStatus.text = status;
    }

    public string getEventStatus() {
        return eventStatus.text;
    }
}
