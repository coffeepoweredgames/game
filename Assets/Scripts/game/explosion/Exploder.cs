﻿using UnityEngine;
using System.Collections;

public class Exploder : Singleton<Exploder> {
    private GameObject explosionPrefab;
    private GameObject explosionInstance;
    private const float EXPLOSION_DURATION = 4;


    void OnEnable() {
        explosionPrefab = Resources.Load(PrefabConstants.EXPLOSION) as GameObject;
    }

    public void explodeHere(Transform target) {

        explosionInstance = MonoBehaviour.Instantiate(explosionPrefab, target.position + (target.forward * (-20)), Quaternion.identity) as GameObject;//TODO positioner.above

        StartCoroutine(DestroyAfter(explosionInstance, EXPLOSION_DURATION));
    }

    /**Destroy*/
    IEnumerator DestroyAfter(GameObject explosion, float delayTime) {
        yield return new WaitForSeconds(delayTime);
        DestroyImmediate(explosion, true);
    }



}
