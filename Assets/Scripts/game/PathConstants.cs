using System;
/// <summary>
/// All paths are relative to Resources directory.
/// </summary>
public class PathConstants{

	public static string MAP_PROPERTIES = "properties/map";
	public static string MESH_PROVINCES = "provinces";
	public static string MESH_COUNTRIES = "countries";
	public static string PREFAB_PROVINCE = "prefabs/map/ProvincePrefab";


}
