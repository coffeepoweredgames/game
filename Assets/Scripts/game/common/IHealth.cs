﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IHealth {

    void ApplyDamage(float damageAmount);
    void ApplyHealing(float healAmount);
    HealthStatus GetHealthStatus();
    bool isAlive();



}

