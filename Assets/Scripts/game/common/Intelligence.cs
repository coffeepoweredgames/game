﻿using UnityEngine;

public class Intelligence : MonoBehaviour {
    public float intelPoints { get; set; }
    private Structure structure { get; set; }

    void Start() {
        structure = GetComponent<Structure>();
    }

    public void AddIntel(float incomingPoints) {
        intelPoints += incomingPoints;

        refreshProgressBar();
    }

    public void DecreaseIntel(float incomingPoints) {

        intelPoints -= incomingPoints;

        if (intelPoints < 0) {
            intelPoints = 0;
        }

        refreshProgressBar();
    }


    /// <summary>
    /// Return the required amount of intel and decrements available intel
    /// </summary>
    /// <param name="requiredAmount"></param>
    /// <returns></returns> required amount of intel or zer0
    public float GetAndDecrement(float requiredAmount) {

        if (intelPoints >= requiredAmount) {
            DecreaseIntel(requiredAmount);
            return requiredAmount;

        }

        print("FAILED TO ACQUIRE " + requiredAmount + "points of intel.");
        return 0;
    }

    public bool isIntelAvailable() {
        return intelPoints > 0;
    }


    public void refreshProgressBar() {
        refreshProgressBar(intelPoints);
    }


    public void refreshProgressBar(float points) {
        if (structure.intelBar == null) {
            return;
        }

        structure.intelBar.SetValue(points);
    }


}
