using UnityEngine;
using System.Collections;

public class InputManager : Singleton<InputManager> {
    private bool temp;
    private GuiManager guiManager;
    private PersistanceManager persistanceManager;

    void Start() {
        guiManager = GuiManager.Instance;
        persistanceManager = PersistanceManager.Instance;
    }

    void Update() {

        if (Input.GetKeyDown(KeyCode.F12)) {
            guiManager.showMainMenu();
        }

        if (Input.GetKeyDown(KeyCode.S)) {
            persistanceManager.SaveGameState();
        }

        if (Input.GetKeyDown(KeyCode.L)) {
            persistanceManager.LoadGameState();
        }

        if (Input.GetKeyDown(KeyCode.R)) {//dynamic registry content
            DynamicObjectRegistry.printRegistryContents();
        }
    }

    public bool isSelectionButtonPressed() {
        return Input.GetMouseButtonDown(0);
    }
}
