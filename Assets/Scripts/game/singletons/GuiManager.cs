using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// TODO remove boiler plate code, replace with generic methods!
/// </summary>
public class GuiManager : Singleton<GuiManager> {
    private GameObject gui;
    private MainMenuScript mainMenu;
    private CountryBorderHud countryBorderHud;
    private AgentStatusHud5 agentStatusHud;
    private StructureMisssionPickerHud structureMisssionPickerHud;
    public SpySchoolHud spySchoolHud { get; set; }//temp onlyW
    private MapSelector mapSelector;
    private ScreenManager screenManager;
    private InGameHud inGameHud;
    private EventPanel eventPanel;
    private LocationPanel locationPanel;
    //private CountryHud countryHud;
    private Text locationText;
    private StructureStatusHud structureStatusHud;
    public StatisticsPanel statisticsPanel { get; set; }

    public GameEventQueue eventQueue;

    private bool isDisplayMenu;

    void Update() {
        showLocationHud(mapSelector.selectedCountry, mapSelector.selectedProvince, mapSelector.selectedCountry != null);

        showMissionEventPanel(eventQueue.Count > 0);
    }

    public void showMissionEventPanel(bool isShowEvents) {
        if (!isShowEvents) {
            eventPanel.gameObject.SetActive(false);
            return;
        }

        eventPanel.gameObject.SetActive(true);
    }

    //GetComponentInChildren - will not find component because instantiated objects are inactive, getcomponent will
    void Awake() {
        base.Awake();

        if (gameObject.transform.FindChild(GuiConstants.GUI) == null) {
            gui = new GameObject();
            gui.name = GuiConstants.GUI;
            gui.transform.parent = gameObject.transform;
        }

        //create ingame menu
        if (gui.transform.FindChild(GuiConstants.INGAME_MENU_NAME) == null) {
            GameObject instantited = Instantiate(Resources.Load(PrefabConstants.INGAME_MENU_GUI_PREFAB)) as GameObject;
            mainMenu = instantited.GetComponent<MainMenuScript>();
            mainMenu.name = GuiConstants.INGAME_MENU_NAME;
            mainMenu.gameObject.SetActive(false);
            mainMenu.transform.SetParent(gui.transform);
        }

        //create ingame hud
        if (gui.transform.FindChild(GuiConstants.INGAME_HUD_NAME) == null) {
            GameObject instantiated = Instantiate(Resources.Load(PrefabConstants.INGAME_HUD_GUI_PREFAB)) as GameObject;
            inGameHud = instantiated.GetComponent<InGameHud>();
            inGameHud.gameObject.name = GuiConstants.INGAME_HUD_NAME;
            inGameHud.transform.SetParent(gui.transform);
            inGameHud.gameObject.SetActive(true);

            eventPanel = instantiated.GetComponentInChildren<EventPanel>();
            locationPanel = instantiated.GetComponentInChildren<LocationPanel>();
            locationText = locationPanel.GetComponentInChildren<CountryHud>().GetComponentInChildren<Text>();
            eventQueue = eventPanel.GetComponentsInChildren<GameEventQueue>(true)[0];

            statisticsPanel = instantiated.GetComponentInChildren<StatisticsPanel>();

            eventPanel.gameObject.SetActive(false);
            locationPanel.gameObject.SetActive(false);
            //statisticsPanel.gameObject.SetActive(false);//TODO
        }

        //create country border hud
        if (gui.transform.FindChild(GuiConstants.COUNTRY_BORDER_HUD_NAME) == null) {
            Object res = Resources.Load(PrefabConstants.COUNTRY_BORDER_HUD_GUI_PREFAB);
            GameObject instantiated = Instantiate(res) as GameObject;
            countryBorderHud = instantiated.GetComponent<CountryBorderHud>();
            countryBorderHud.gameObject.name = GuiConstants.COUNTRY_BORDER_HUD_NAME;
            countryBorderHud.gameObject.SetActive(false);
            countryBorderHud.transform.SetParent(gui.transform);
        }

        //create agent hud
        if (gui.transform.FindChild(GuiConstants.AGENT_STATUS_HUD_NAME) == null) {
            GameObject instantiated = Instantiate(Resources.Load(PrefabConstants.AGENT_HUD_GUI_PREFAB)) as GameObject;
            agentStatusHud = instantiated.GetComponent<AgentStatusHud5>();
            agentStatusHud.gameObject.name = GuiConstants.AGENT_STATUS_HUD_NAME;
            agentStatusHud.gameObject.SetActive(false);
            agentStatusHud.transform.SetParent(gui.transform);
        }

        //create agent hud
        if (gui.transform.FindChild(GuiConstants.STRUCTURE_MISSION_PICKER_HUD_NAME) == null) {
            GameObject instantiated = Instantiate(Resources.Load(PrefabConstants.STRUCTURE_MISSION_PICKER_GUI_PREFAB)) as GameObject;
            structureMisssionPickerHud = instantiated.GetComponent<StructureMisssionPickerHud>();
            structureMisssionPickerHud.gameObject.name = GuiConstants.STRUCTURE_MISSION_PICKER_HUD_NAME;
            structureMisssionPickerHud.gameObject.SetActive(false);
        }

        //create structure status hud
        if (gui.transform.FindChild(GuiConstants.STRUCTURE_STATUS_HUD_NAME) == null) {
            GameObject instantiated = Instantiate(Resources.Load(PrefabConstants.STRUCTURE_STATUS_HUD_GUI_PREFAB)) as GameObject;
            structureStatusHud = instantiated.GetComponent<StructureStatusHud>();
            structureStatusHud.gameObject.name = GuiConstants.STRUCTURE_STATUS_HUD_NAME;
            structureStatusHud.gameObject.SetActive(false);
            structureStatusHud.transform.SetParent(gui.transform);
        }

        if (statisticsPanel == null) {
            statisticsPanel = inGameHud.GetComponentInChildren<StatisticsPanel>();
        }

    }

    void Start() {
        mapSelector = MapSelector.Instance;
        screenManager = ScreenManager.Instance;
    }


    public void showMainMenu() {
        Transform mainMenuTransform = gui.transform.FindChild(GuiConstants.INGAME_MENU_NAME);

        if (mainMenuTransform == null) {
            Debug.LogError("Cannot show main menu. Main menu missing!");
            return;
        }

        isDisplayMenu = !isDisplayMenu;//new state

        if (isDisplayMenu) {
            mainMenuTransform.gameObject.SetActive(true);
            screenManager.pauseGame();//pause game while showing menu
            return;
        }

        mainMenuTransform.gameObject.SetActive(false);
        screenManager.resumeGame();//hide menu and resume game
    }


    public void showStatisticsWindow(bool shouldDisplay){
        if (shouldDisplay){
            print("DISPLAYING STATS WINDOW");
            //statsPanel.gameObject.SetActive(true);
            statisticsPanel.slideDown();
            return;
        }

        print("DISPLAYING STATS WINDOW");
        statisticsPanel.slideUp();

        //statsPanel.gameObject.SetActive(false);
    }

    /// <summary>
    /// Shows the COUNTRY hud.
    /// </summary>
    /// <param name="countryName">Country name.</param>
    public void showLocationHud(Country country, Province province, bool isDisplayLocationHud) {

        if (!isDisplayLocationHud) {
            locationPanel.gameObject.SetActive(false);
            return;
        }

        locationPanel.gameObject.SetActive(true);

        string provinceName = (province != null ? province.descriptor.provinceName : "Unknown province.");

        locationText.text = string.Format("{0}", provinceName);

        return;
    }

    /// <summary>
    /// Shows the AGENT STATUS hud.
    /// </summary>
    /// <param name="isDisplay">If set to <c>true</c> is display.</param>
    public void showAgentHud(Agent agent, bool isDisplay) {//TODO doesnt track for which agent HUD is displayed
        if (!isDisplay) {
            agentStatusHud.transform.SetParent(this.transform);
            agentStatusHud.gameObject.SetActive(false);
            agentStatusHud.clear(agent);
            return;
        }

        agentStatusHud.clear(agent);

        agentStatusHud.transform.SetParent(agent.transform);//parenting

        agentStatusHud.gameObject.SetActive(true);//make visible

        agentStatusHud.hud.aboveOf(agent);//positioning

        agentStatusHud.setAgentName(agent.descriptor.name);//display name

        agent.xpBar = agentStatusHud.xpBar;
        agent.healthBar = agentStatusHud.healthBar;
        agent.refreshGui();

        return;
    }

    public bool showMissionPickerHud(Structure structure, Agent agent, bool isDisplay) {
        if (!isDisplay) {
            structureMisssionPickerHud.gameObject.SetActive(false);
            //GameLogger.printRed("CLOSING MISSION PICKER!");
            structureMisssionPickerHud.transform.SetParent(this.transform);
            return false;
        }

        ////if (structureMisssionPickerHud.gameObject.activeSelf == isDisplay) {//already being displayed TODO structure check must be made
        ////    //GameLogger.printBlue("ALREADY DISPLAYED MISSION PICKER!");
        ////    return isDisplay;
        ////}

        ////GameLogger.printBlue("DISPLAY MISSION PICKER!");

        structureMisssionPickerHud.transform.SetParent(structure.transform);//parenting

        structureMisssionPickerHud.gameObject.SetActive(true);//make visible

        structureMisssionPickerHud.hud.aboveOf(structure);//positioning

        structureMisssionPickerHud.SetStructureName(structure.structureDescriptor.name);//display name
        structureMisssionPickerHud.logic.agent = agent;
        structureMisssionPickerHud.logic.structure = structure;

        agent.transform.position = agent.location.province.positionUnique;//TODO not good, will place you here no matter what structure you choose!!!
        return true;
    }

    public bool showStructureStatusHud(Structure structure, bool isDisplay) {
        if (!isDisplay) {
            structureStatusHud.gameObject.SetActive(false);
            structureStatusHud.clear(structure);
            structureStatusHud.transform.SetParent(this.transform);
            return false;
        }

        //if (structureStatusHud.gameObject.activeSelf == isDisplay) {//already being displayed TODO structure check not prefab check
        //    return isDisplay;
        //}


        if (structureMisssionPickerHud.gameObject.activeSelf && structureMisssionPickerHud.logic.structure.structureDescriptor.Equals(structure.structureDescriptor)){
            return false;
        }

        //TODO check structure health and change color of UI

        //

        structureStatusHud.clear(structure);

        structureStatusHud.transform.SetParent(structure.transform);//parenting

        structureStatusHud.gameObject.SetActive(true);//make visible

        structureStatusHud.hud.aboveOf(structure);//positioning

        structureStatusHud.SetStructureName(structure.structureDescriptor.name);//display name

        structureStatusHud.logic.structure = structure;

        //add progress bar refs
        structure.intelBar = structureStatusHud.intelBar;
        structure.healthBar = structureStatusHud.healthBar;
        structure.refreshGui();

        return true;
    }

    public void hideCountryBorderHud() {
        showCountryBorderHud(null, null, null, false);

    }

    public void showCountryBorderHud(Country country, Province province, Agent agent, bool isDisplay) {
        if (!isDisplay) {
            countryBorderHud.gameObject.SetActive(false);
            return;
        }

        countryBorderHud.logic.country = country;
        countryBorderHud.logic.province = province;
        countryBorderHud.logic.agent = agent;

        Debug.Log(string.Format("gui manager init: c = {0}, p = {1}, a = {2}", countryBorderHud.logic.country, countryBorderHud.logic.province, countryBorderHud.logic.agent));

        countryBorderHud.hud.aboveOf(country);
        countryBorderHud.gameObject.SetActive(true);
    }

    //Create a pool of border crossing huds available for each agent, so you dont need to reset existing border huds
    public bool isCountryBorderHudActiveForAgent(Agent agent) {
        if (!countryBorderHud.isActiveAndEnabled) {
            GameLogger.printBlue("HUD is not active!!");
            return false;
        }

       return countryBorderHud.logic.agent.descriptor.Equals(agent.descriptor);
    }

    public void showSpySchoolHud(SpySchoolStructure school, bool isDisplay) {
        if (!isDisplay) {
            spySchoolHud.gameObject.SetActive(false);
            return;
        }

        spySchoolHud.gameObject.SetActive(true);
        return;
    }
}
