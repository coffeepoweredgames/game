using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Beware when adding or removing scenes from BuildSettings. (scene id's are shifted)!
/// No smart way of getting scene names by name from BuildSettings.
/// </summary>
public class ScreenManager : Singleton<ScreenManager> {

    //SCENE ID's (SCREENS) MANAGED
    public const int COMPANY_LOGO_SCREEN_ID = 0;
    public const int SPLASH_SCREEN_ID = 1;
    public const int MAIN_MENU_SCREEN_ID = 2;
    public const int LEVEL_ONE_SCREEN_ID = 3;
    public const int CREDITS_SCREEN_ID = 4;
    public int currentScreenId = -100;
    private const int LOGO_DURATION = 8;
    private const int SPLASH_DURATION = 8;
    private bool isGamePaused;//TODO flag inside game;
    private bool isSkipCurrentScreen { get; set; }

    void Start() {
        currentScreenId = getCurrentScreenId();
    }

    public static Dictionary<string, int> SCENES = new Dictionary<string, int>() { //MUST BE EQUAL TO BUILD SETTINGS !!
        {"CompanyLogoScreen", COMPANY_LOGO_SCREEN_ID},
        {"SplashScreen", SPLASH_SCREEN_ID},
        {"MainMenuScreen", MAIN_MENU_SCREEN_ID},
        {"Level1", LEVEL_ONE_SCREEN_ID},
        {"CreditsScreen", CREDITS_SCREEN_ID}
    };

    public static Dictionary<int, string> SCENE_NAMES = new Dictionary<int, string>() { 
        { COMPANY_LOGO_SCREEN_ID, "CompanyLogoScreen"},
        { SPLASH_SCREEN_ID,"SplashScreen"},
        { MAIN_MENU_SCREEN_ID, "MainMenuScreen"},
        {LEVEL_ONE_SCREEN_ID, "Level1"},
        {CREDITS_SCREEN_ID, "CreditsScreen"}
    };

    public void loadNextScreen() {
        print("CURRENT SCREEN: " + currentScreenId);
        Application.LoadLevel(getNextScreenId(getCurrentScreenId()));
        print("NEXT SCREEN: " + currentScreenId);
    }

    public int getNextScreenId(int screenId) {
        switch (screenId) {
            case COMPANY_LOGO_SCREEN_ID:
                return SPLASH_SCREEN_ID;
            case SPLASH_SCREEN_ID:
                return MAIN_MENU_SCREEN_ID;
            case CREDITS_SCREEN_ID:
                return MAIN_MENU_SCREEN_ID;
            default:
                print("CANNOT DETERMINE NEXT SCREEN ID!");
                return -1; //TODO return funny screen "You shouldn't be here" :)
        }
    }

    private int getCurrentScreenId() {
        if (!SCENES.ContainsKey(Application.loadedLevelName)) {
            GameLogger.printRed("UNKNOWN SCENE NAME!");
            return -1;
        }

        currentScreenId = SCENES[Application.loadedLevelName];
        return currentScreenId;
    }

    public void exitGame() {
        Application.Quit();
    }

    public void pauseGame() {
        isGamePaused = true;
        Time.timeScale = 0f;
    }

    public void resumeGame() {
        isGamePaused = false;
        Time.timeScale = 1.0f;
    }

    void Update() {

        //check if screen change input available
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1)) {
            this.isSkipCurrentScreen = true;
        }
        else {
            isSkipCurrentScreen = false;
        }

        //skip certains screens on keypress
        if (currentScreenId == COMPANY_LOGO_SCREEN_ID || currentScreenId == SPLASH_SCREEN_ID || currentScreenId == CREDITS_SCREEN_ID) {
            if (isSkipCurrentScreen) {
                loadNextScreen();
                return;
            }
        }

        //some screens display on for a certain amount of time (logo, splash screen etc.)
        if (currentScreenId == COMPANY_LOGO_SCREEN_ID || currentScreenId == SPLASH_SCREEN_ID) {

            if (Time.timeSinceLevelLoad > getLogoDuration()) {
                loadNextScreen();
                return;
            }
        }
    }

    public int getCompanyLogoScreen() {
        return COMPANY_LOGO_SCREEN_ID;
    }

    public int getSplashScreen() {
        return SPLASH_SCREEN_ID;
    }

    public int getMainMenuScreen() {
        return MAIN_MENU_SCREEN_ID;
    }

    public int getLevelOneScreen() {
        return LEVEL_ONE_SCREEN_ID;
    }

    public int getCreditsScreen() {
        return CREDITS_SCREEN_ID;
    }

    public int getLogoDuration() {
        return LOGO_DURATION;
    }

    public int getSplashDuration() {
        return SPLASH_DURATION;
    }

    public void loadLogoScreen() {
        Application.LoadLevel(COMPANY_LOGO_SCREEN_ID);
    }

    public void loadSplashScreen() {
        Application.LoadLevel(SPLASH_SCREEN_ID);
    }

    public void loadMenuScreen() {
        Application.LoadLevel(MAIN_MENU_SCREEN_ID);
    }

    public void loadLevelOne() {
        Application.LoadLevel(LEVEL_ONE_SCREEN_ID);
    }

    public void loadCreditsScreen() {
        Application.LoadLevel(CREDITS_SCREEN_ID);
    }


}
