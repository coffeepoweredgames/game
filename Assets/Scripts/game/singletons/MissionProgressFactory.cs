﻿using UnityEngine;
using System.Collections;

public class MissionProgressFactory : Singleton<MissionProgressFactory> {//TODO klasa : MonoBehaviour + static Build method, should work instead of singleton

    private Object missionProgressPrefab;

    void Start() {
        missionProgressPrefab = Resources.Load(PrefabConstants.MISSION_PROGRESS_PREFAB);
    }

    public MissionProgress Build(Agent agent, Mission mission) {
        MissionProgress progress = (Instantiate(missionProgressPrefab) as GameObject).GetComponent<MissionProgress>();
		progress.agent = agent;
		progress.mission = mission;
        progress.goalsToComplete = mission.goalList;
        progress.gameObject.name = "MISSION_PROGRESS_"+mission.descriptor.name;

        mission.descriptor.agent = agent;

        return progress;
    }

    
}
