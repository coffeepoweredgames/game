﻿using UnityEngine;
//using UnityEditor;
using System.Collections;
using System.Collections.Generic;


public class PersistanceManager : Singleton<PersistanceManager> {

    public void SaveGameState() {
        saveAgentData();
        saveSelectorData();
        saveMissionData();
    }

    public void LoadGameState() {
        loadAgentData();
        loadSelectorData();
        loadMissionData();
    }

    public ScriptableObject Load(string path, System.Type type) {
        //return AssetDatabase.LoadAssetAtPath(path, type) as ScriptableObject;//TODO commented (NOT ALLOWED IN BUILDS)
        return null;
    }

    public void Save(ScriptableObject scriptableObject) {
        //EditorUtility.SetDirty(scriptableObject);//will trigger saving to dis //TODO commented (NOT ALLOWED IN BUILDS)

    }

    public ScriptableObject CreateAsset<T>(string path) where T : ScriptableObject {
        T scriptableObject = ScriptableObject.CreateInstance<T>();
        //AssetDatabase.CreateAsset(scriptableObject, path); //TODO commented (NOT ALLOWED IN BUILDS)
        //AssetDatabase.SaveAssets();//TODO commented (NOT ALLOWED IN BUILDS)
        //AssetDatabase.Refresh();//TODO commented (NOT ALLOWED IN BUILDS)
        return scriptableObject;
    }

    public bool isAssetOfTypeExists(string assetPath, System.Type assetType) {
        return Load(assetPath, assetType) != null;
    }

    private void saveAgentData() {
        AgentSaveData savedData;

        savedData = Load(AssetConstants.SAVED_DATA_AGENTS, typeof(AgentSaveData)) as AgentSaveData;

        if (savedData == null) {
            savedData = CreateAsset<AgentSaveData>(AssetConstants.SAVED_DATA_AGENTS) as AgentSaveData;
        }

        List<AgentData> agentsToSave = new List<AgentData>();

        foreach (KeyValuePair<string, Agent> entry in DynamicObjectRegistry.AGENTS) {
            //Debug.Log("Saving agent with descriptor: " + entry.Value.descriptor.agentName);
            agentsToSave.Add(PersistenceMapper.fromComponent(entry.Value));
        }

        savedData.savedAgentList = agentsToSave;

        //save highlighted
        AgentDescriptor selectedAgentDescriptor = MapSelector.Instance.selectedAgent != null ? MapSelector.Instance.selectedAgent.descriptor : null;
        savedData.selectedAgentDescriptor = PersistenceMapper.fromComponent(selectedAgentDescriptor);

        Save(savedData);
    }

    private void loadAgentData() {
        AgentSaveData savedData;

        savedData = Load(AssetConstants.SAVED_DATA_AGENTS, typeof(AgentSaveData)) as AgentSaveData;

        if (savedData == null) {
            GameLogger.printRed("Failed to load agents!");
            return;
        }

        //when loading, reload only saved agents
        foreach (AgentData agentData in savedData.savedAgentList) {
            Agent agentToReplace = DynamicObjectRegistry.GetAgent(agentData.descriptorData);

            if (agentToReplace != null) {
                Debug.Log("Destroying agent with descriptor: " + agentData.descriptorData.agentName);
                DynamicObjectRegistry.RemoveAgent(agentToReplace);
                Destroy(agentToReplace.gameObject);
            }
        }

        //re-instantiate saved agents
        foreach (AgentData agentData in savedData.savedAgentList) {
            Debug.Log("Loading agent with descriptor: " + agentData.descriptorData.agentName);
            AgentFactory.Instance.Rebuild(agentData);
        }
    }

    private void saveSelectorData() {
        MapSelectorSaveData savedData;

        savedData = Load(AssetConstants.SAVED_DATA_MAP_SELECTOR, typeof(MapSelectorSaveData)) as MapSelectorSaveData;

        if (savedData == null) {
            savedData = CreateAsset<MapSelectorSaveData>(AssetConstants.SAVED_DATA_MAP_SELECTOR) as MapSelectorSaveData;
        }

        CountryDescriptor cd = MapSelector.Instance.selectedCountry != null ? MapSelector.Instance.selectedCountry.descriptor : null;
        ProvinceDescriptor pd = MapSelector.Instance.selectedProvince != null ? MapSelector.Instance.selectedProvince.descriptor : null;
        AgentDescriptor ad = MapSelector.Instance.selectedAgent != null ? MapSelector.Instance.selectedAgent.descriptor : null;

        savedData.countryDescriptor = PersistenceMapper.fromComponent(cd);
        savedData.provinceDescriptor = PersistenceMapper.fromComponent(pd);
        savedData.agentDescriptor = PersistenceMapper.fromComponent(ad);

        Save(savedData);
    }

    private void loadSelectorData() {
        MapSelectorSaveData savedData;

        savedData = Load(AssetConstants.SAVED_DATA_MAP_SELECTOR, typeof(MapSelectorSaveData)) as MapSelectorSaveData;

        if (savedData == null) {
            GameLogger.printRed("Failed to load selector data!");
            return;
        }

        //re-select country
        if (savedData.countryDescriptor != null && savedData.countryDescriptor.isValid()) {
            Country countryToSelect = DynamicObjectRegistry.COUNTRIES[savedData.countryDescriptor.countryName];
            MapSelector.Instance.selectCountry(countryToSelect);
        }

        //re-select province
        if (savedData.provinceDescriptor != null && savedData.provinceDescriptor.isValid()) {
            Province provinceToSelect = DynamicObjectRegistry.PROVINCES[savedData.provinceDescriptor.provinceName];
            MapSelector.Instance.markProvinceSelected(provinceToSelect);
        }

        //re-select city
        if (savedData.cityDescriptor != null && savedData.cityDescriptor.isValid()) {
            City cityToSelect = DynamicObjectRegistry.CITIES[savedData.cityDescriptor.cityName];
            MapSelector.Instance.selectCity(cityToSelect);
        }

        //re-select agent
        if (savedData.agentDescriptor != null && savedData.agentDescriptor.isValid()) {
            Agent agentToSelect = DynamicObjectRegistry.AGENTS[savedData.agentDescriptor.agentName];
            MapSelector.Instance.selectAgent(agentToSelect, MapSelector.ALLOW_RESELECT_TRUE);

            GuiManager.Instance.showAgentHud(agentToSelect, true);
        }
    }


    private void loadMissionData() {
        //TODO load missions + progress data
    }

    private void saveMissionData() {
        //TODO save missions + progress data
    }



}