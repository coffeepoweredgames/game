using UnityEngine;
using System.Collections.Generic;


/// <summary>
/// Used to remember which agent is moving where.
/// Sometimes a gui dialog is prompted, waiting for player response, so an agent is moving somewhere
/// and should not attempt to move again to that same location.
/// 
/// </summary>
public class MovementContextManager : Singleton<MovementContextManager> {
    public static Dictionary<AgentDescriptor, Province> agentAndLocationContext;

    public Mover mover;
    private GuiManager guiManager;
    private MapSelector mapSelector;

    void OnEnable() {
        agentAndLocationContext = new Dictionary<AgentDescriptor, Province>();
    }

    void Start() {
        mover = Mover.Instance;
        guiManager = GuiManager.Instance;
        mapSelector = MapSelector.Instance;
    }

    public void Add(AgentDescriptor agent, Province province) {
        agentAndLocationContext.Add(agent, province);

    }

    public void Remove(AgentDescriptor agent, Province province) {
        agentAndLocationContext.Remove(agent);
    }


    public bool isMovingAlreadyThere(Agent agent, Province province) {
        if (agent == null) {
            //Debug.Log ("isMovingAlreadyThere");
            return false;
        }

        Province foundProvince;
        agentAndLocationContext.TryGetValue(agent.descriptor, out foundProvince);

        if (foundProvince == null || !province.Equals(foundProvince)) {
            //Debug.Log(string.Format("agent {0} is not already moving into {1}", agent, location));
            return false;
        }

        //Debug.Log(string.Format("agent {0} is already moving into {1}", agent, location));
        return true;
    }


    public void CountryBorderAccept(Agent agent, Province province) {
        Debug.Log(string.Format("Agent {0} APPROVED ENTRY to {1} on frame {2}!", agent, province.country, Time.frameCount));

        //Debug.Log(string.Format("c = {0}, p = {1}, a = {2}", province.country, province, agent));

        //add agent to country
        province.country.OnCountryBorderEnter(province, agent);
        province.country.OnCountryEnter(agent);

        //add agent to province
        province.OnProvinceBorderEnter(agent);
        province.OnProvinceEnter(agent);

        //close border dialog hud
        clearMovementContextAndHuds(agent);
    }

    public void CountryBorderDeny(Agent agent, Province province) {
        //Debug.Log(string.Format("Agent {0} DENIED ENTRY to {1} on frame {2}!", agent, province.country, Time.frameCount));

        //Debug.Log(string.Format("c = {0}, p = {1}, a = {2}", province.country, province, agent));

        clearMovementContextAndHuds(agent);

        if (agent.draggingContext) {
            agent.mouseDragger.resetPositionCausedByDragging();
        }
    }

    public void clearMovementContextAndHuds(Agent agent) {
        if (agent == null || agentAndLocationContext.ContainsKey(agent.descriptor) == false) {
            return;
        }

        if (guiManager.isCountryBorderHudActiveForAgent(agent)) { 
            guiManager.hideCountryBorderHud();
        }


        this.Remove(agent.descriptor, agent.location.province);

        mapSelector.DeSelectProvince();
        mapSelector.DeSelectCountry();
    }


}
