using System;
using UnityEngine;



/// <summary>
/// Utility for positioning two object relative to each other.
/// Specify 'who' you want to position relative to a 'where'
/// Specify above, beneath... (at a specified 'distance' along a 'normal')
/// NOTE:
/// TODO All models should have the same orientation aka UP should be up for all entities.
/// TODO All meshes should have the PIVOT at the bottom, for simpler logic.
/// TODO at trace level warn if you are positioning over a object with aa non unit value 3.5 > 1 scale (scale inheriting)
/// </summary>
public class Positioner : Singleton<Positioner> {

    public enum POS {
        ABOVE,
        BENEATH,
        LEFT,
        RIGHT
    }

    //All meshes should have pivots at bottom. Especially HUD's
    public void above(GameObject who, GameObject where) {
        Vector3 normal;
        Vector3 targetPosition;
        Quaternion targetRotation;

        if (who.GetComponent<Agent>() != null) {
            Agent agent = who.GetComponent<Agent>();//pivot at bottom (ok)
            normal = getNormalAndMagnitude(where);
            targetRotation = Quaternion.FromToRotation(agent.transform.up, normal) * agent.transform.rotation;
            targetPosition = agent.transform.position;

            who.transform.position = targetPosition;
            who.transform.rotation = targetRotation;

            return;
        }

        if (who.GetComponent<Hud>() != null) {
            Hud hud = who.GetComponent<Hud>();
            normal = getNormalAndMagnitude(where);
            targetRotation = Quaternion.FromToRotation(hud.transform.up, normal) * hud.transform.rotation;
            targetPosition = where.transform.position + normal;

            who.transform.position = targetPosition;
            who.transform.rotation = targetRotation;

            return;
        }


        //defaults
        normal = getNormalAndMagnitude(where);
        targetRotation = Quaternion.FromToRotation(who.transform.up, normal) * who.transform.rotation;
        targetPosition = where.transform.position + normal;

        who.transform.position = targetPosition;
        who.transform.rotation = targetRotation;
        return;

    }

    private Vector3 getNormalAndMagnitude(GameObject go) {
        Vector3 normal;
        float magnitude;

        if (go.GetComponent<Province>() != null) {
            Province province = go.GetComponent<Province>();
            normal = (-1) * province.transform.forward;
            magnitude = province.transform.localScale.z / 2;
            return normal * magnitude;
        }
        else if (go.GetComponent<Agent>() != null) {
            normal = go.transform.up;
            magnitude = 35;
            return normal * magnitude;
        }

        return (-1) * go.transform.forward * 15;
    }

}