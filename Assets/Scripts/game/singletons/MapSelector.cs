using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/**
 * TODO dodaj mouse button pressed callback, mislim da je bolje rijesenje
 * TODO dodaj PLACE funkcionalnost da mozes na selection spot dodati neki objekt(bitno)
 * */
public class MapSelector : Singleton<MapSelector> {
    public LayerMask COLLISION_LAYERS_MASK;
    public const bool ALLOW_RESELECT_TRUE = true;
    public const bool ALLOW_RESELECT_FALSE = false;

    public TileMapImpl map;
    private Vector3 currentTileCoord;
    public Country selectedCountry { get; set; }
    public Agent selectedAgent { get; set; }
    public Province selectedProvince { get; set; }
    public City selectedCity { get; set; }
    private const bool ALLOW_MULTIPLE_COUNTRY_SELECTION = true;
    private SpySchoolStructure selectedSpySchool;
    private GameLogger LOGGER;
    private InputManager inputManager;
    private GuiManager guiManager;
    private MovementContextManager movementManager;
    public Structure selectedStructure { get; set; }
    private static Ray rayThatCausedSelection;
    private bool suspendMouseClicks = false;
    public List<GraphicRaycaster> rayCastersList = new List<GraphicRaycaster>();

    void OnEnable() {
        LOGGER = new GameLogger(true, this.GetType());
        COLLISION_LAYERS_MASK = 1 << LayerMask.NameToLayer(LayerTagConstants.MAP_LAYER);// &1 << LayerMask.NameToLayer(LayerTagConstants.STRUCTURE_LAYER);
    }

    void Start() {
        inputManager = InputManager.Instance;
        guiManager = GuiManager.Instance;
        movementManager = MovementContextManager.Instance;
        map = GameObject.Find("Game").GetComponentInChildren<TileMapImpl>();//TODO better way
        
    }

    void Update() {
        selectSomething();
    }

    public void selectSomething() {
        if (!inputManager.isSelectionButtonPressed()) {
            return;
        }

        RaycastHit hit;
        if (!isSomethingHit(out hit, COLLISION_LAYERS_MASK, null)) {
            return;
        }

        print("HIT OBJECT: " + hit.transform.name);

        if (isHitUI()) {
            return;
        }

        if (hit.transform.tag.Equals(LayerTagConstants.TILE_MAP_TAG)) {
            deSelectProvince(selectedProvince);
            deSelectCountry(selectedCountry);
        }

        Agent agent = hit.transform.gameObject.GetComponentInParent<Agent>();
        Area area = hit.transform.gameObject.GetComponentInParent<Area>();
        Province province = hit.transform.gameObject.GetComponentInParent<Province>();
        SpySchoolStructure spySchool = hit.transform.gameObject.GetComponent<SpySchoolStructure>();//TODO implement more generalized structure selection
        Structure structure = hit.transform.gameObject.GetComponent<Structure>();

        if (agent != null) {
            selectAgent(agent, ALLOW_RESELECT_FALSE);
        }
        else if (spySchool != null) {
            selectSpySchool(spySchool);
        }
        else if (structure != null) {
            print("gonna select structure");
            SelectStructure(structure);
        }
        else if (area != null) {            //area represent country
            selectCountry(area.GetCountry());
        }
        else if (province != null) {
            markProvinceSelected(province);

            if (selectedProvince == null) {//province was deselected, deselect corresponding country
                deSelectCountry(selectedCountry);
            }
            else {//province was selected, select corresponding country
                selectCountry(selectedProvince.country, ALLOW_MULTIPLE_COUNTRY_SELECTION);
            }
        }

        //notify gui manager
        guiManager.showSpySchoolHud(selectedSpySchool, selectedSpySchool != null);
        guiManager.showAgentHud(selectedAgent, selectedAgent != null);
        guiManager.showStructureStatusHud(selectedStructure, selectedStructure != null);

        //notify mover
        //mover.TryMoving();//NO AGENT MOVEMENT USING CLICKS

    }

    private void SelectStructure(Structure structureToSelect) {
        if (selectedStructure == null) {
            selectedStructure = structureToSelect;
            print("selected " + structureToSelect.name);
            return;
        }

        //select was selected again, so you deselect it
        if (selectedStructure.structureDescriptor.structureName.Equals(structureToSelect.structureDescriptor.structureName)) {
            print("selected again " + structureToSelect.name + " gonna deselect it!");
            DeselectStructure(selectedStructure);
            return;
        }

        //selected and already selected are not equal, deselect old and select new
        if (!selectedStructure.structureDescriptor.structureName.Equals(structureToSelect.structureDescriptor.structureName)) {
            print("New structure selected " + structureToSelect.name + " gonna deselect old one: " + selectedStructure.name);
            DeselectStructure(selectedStructure);
            selectedStructure = structureToSelect;
            return;
        }
    }

    public void DeselectStructure(Structure structureToDeselect) {
        if (structureToDeselect == null || selectedStructure == null) {
            return;
        }

        if (selectedStructure.structureDescriptor.structureName.Equals(structureToDeselect.structureDescriptor.structureName)) {
            selectedStructure = null;
            print("deselected " + structureToDeselect.name);
            return;
        }

        print("DESELECT FAILED!");
    }


    public Province selectProvince(List<Ray> customSelectionRay) {

        RaycastHit hit;
        if (!isSomethingHit(out hit, COLLISION_LAYERS_MASK, customSelectionRay)) {
            return null;
        }

        //print("HIT NAME:" + hit.transform.name);//TODO temp
        //print("HIT position:" + hit.transform.position);//TODO temp

        Province province = hit.transform.gameObject.GetComponentInParent<Province>();
        markProvinceSelected(province);
        return province;
    }

    public void markProvinceSelected(Province provinceToSelect) {
        if (provinceToSelect == null) {
            return;
        }

        if (selectedProvince == null) {
            provinceToSelect.gameObject.GetComponentInChildren<MeshRenderer>().sharedMaterial.color = ColorConstants.SELECTED_COLOR;//TODO not here
            selectedProvince = provinceToSelect;
            LOGGER.Log(string.Format("Province {0} selected.", provinceToSelect.descriptor.provinceName));
            return;
        }

        //DESELECT ALREADY SELECTED
        if (selectedProvince.gameObject.name.Equals(provinceToSelect.gameObject.name)) {
            deSelectProvince(selectedProvince);
            return;
        }

        //DESELECT OLD AND SELECT NEW
        if (selectedProvince.descriptor.provinceName.Equals(provinceToSelect.descriptor.provinceName) == false) {
            LOGGER.Log(string.Format("Province {0} selected.", provinceToSelect.descriptor.provinceName));
            deSelectProvince(selectedProvince);//old

            provinceToSelect.gameObject.GetComponentInChildren<MeshRenderer>().sharedMaterial.color = ColorConstants.SELECTED_COLOR;//new

            selectedProvince = provinceToSelect;
            return;
        }
    }

    private void deSelectProvince(Province provinceToDeselect) {
        if (provinceToDeselect == null) {//dont know what to deselect
            return;
        }

        if (selectedProvince == null) {//nothing is selected
            return;
        }

        if (selectedProvince.descriptor.provinceName.Equals(provinceToDeselect.descriptor.provinceName)) {
            LOGGER.Log(string.Format("Province {0} deselected.", selectedProvince.descriptor.provinceName));
            selectedProvince.GetComponentInChildren<MeshRenderer>().sharedMaterial.color = ColorConstants.PROVINCE_COLOR;//TODO not here
            selectedProvince = null;
            return;
        }

        Debug.Log("Cannot deselect " + provinceToDeselect.descriptor.provinceName + "!");//province to deselect doesnt match selected province
    }

    /// <summary>
    /// Select country.
    /// </summary>
    /// <param name="countryToSelect">Country to select.</param>
    private void selectCountry(Country countryToSelect, bool allowMultipleSelectOfSame) {
        //1. click onto non-country (a.k.a ocean)
        if (countryToSelect == null) {
            deSelectCountry(selectedCountry);
            return;
        }

        //2. no country selected
        if (selectedCountry == null) {
            map.SelectCountry(countryToSelect);//TODO mapa je pravo mjesto gdje se cuva kaj je selektirano
            selectedCountry = countryToSelect;
            LOGGER.Log(string.Format("Country {0} selected.", selectedCountry.descriptor.countryName));
            return;
        }

        //3. same country selected again
        if (selectedCountry.descriptor.countryName.Equals(countryToSelect.descriptor.countryName)) {
            if (!allowMultipleSelectOfSame) {
                deSelectCountry(selectedCountry);
                return;
            }
            LOGGER.Log(string.Format("Country {0} repeatedly selected.", selectedCountry.descriptor.countryName));//TODO temp
        }


        //4. deselect old and select new
        deSelectCountry(selectedCountry);

        //deselect provinces in old country(province is located france, you selected britain)
        if (selectedProvince != null && countryToSelect.descriptor.countryName.Equals(selectedProvince.country.descriptor.countryName) == false) {
            deSelectProvince(selectedProvince);
        }

        map.SelectCountry(countryToSelect);//TODO mapa je pravo mjesto gdje se cuva kaj je selektirano
        selectedCountry = countryToSelect;
        LOGGER.Log(string.Format("Country {0} selected.", selectedCountry.descriptor.countryName));
        return;

    }

    public void selectCountry(Country country) {
        selectCountry(country, false);
    }

    /// <summary>
    /// Deselect country
    /// </summary>
    /// <param name="country">Country.</param>
    private void deSelectCountry(Country country) {
        if (country == null) {
            return;
        }

        if (selectedCountry == null) {//nothing is selected
            return;
        }

        if (selectedCountry.descriptor.countryName.Equals(country.descriptor.countryName)) {
            map.DeSelectCountry(selectedCountry);
            LOGGER.Log(string.Format("Country {0} deselected.", selectedCountry.descriptor.countryName));
            selectedCountry = null;
            return;
        }

        Debug.Log("Cannot deselect " + country.descriptor.countryName + "!");//country to deselect doesnt match selected country
    }

    public void selectAgent(Agent agent, bool allowReselectAlreadySelected) {
        if (agent == null) {
            return;
        }

        //select agent
        if (selectedAgent == null) {
            agent.gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.magenta;//TODO not here
            selectedAgent = agent;
            LOGGER.Log(string.Format("Agent {0} selected.", agent.descriptor.agentName));

            return;
        }

        //agent was selected, deselect him
        if (selectedAgent.name.Equals(agent.name)) {
            if (allowReselectAlreadySelected) {
                return;
            }

            DeselectAgent(selectedAgent);
            return;
        }

        //different agent selected
        DeselectAgent(selectedAgent);
        selectAgent(agent, ALLOW_RESELECT_FALSE);

    }

    public void DeselectAgent(Agent agent) {
        if (agent == null || selectedAgent == null) {
            return;
        }

        //currently each agent shares a single border crossing hud element.
        //When you deselect an already selected agent
        //you should also clear its hud and movement context
        movementManager.clearMovementContextAndHuds(selectedAgent);

        selectedAgent.GetComponentInChildren<MeshRenderer>().material.color = Color.white;//TODO agent selection and coloring should be done like for countries!!
        selectedAgent = null;

        //TODO should not do this manually, but dont want to perform this check in UPDATE
        guiManager.showAgentHud(selectedAgent, selectedAgent != null);

        LOGGER.Log(string.Format("Agent {0} deselected.", agent.descriptor.agentName));
    }

    private void selectSpySchool(SpySchoolStructure structure) {
        if (selectedSpySchool == null) {
            selectedSpySchool = structure;
            selectedSpySchool.gameObject.GetComponent<Renderer>().material.color = Color.blue;//TODO use constants class
            LOGGER.Log("Spy school selected.");
            return;
        }
        selectedSpySchool.gameObject.GetComponent<Renderer>().material.color = Color.grey;
        selectedSpySchool = null;
        LOGGER.Log("Spy school deselected.");
    }

    public void DeselectAgent() {//TODO temp
        DeselectAgent(selectedAgent);
        Debug.Log("TMS.deselect agent from outside");
    }

    public void DeSelectProvince() {//TODO temp
        deSelectProvince(selectedProvince);
        Debug.Log("TMS.deselect province from outside");
    }

    public void DeSelectCountry() {
        deSelectCountry(selectedCountry);
    }

    public void selectCity(City city) {
        //TODO NOT IMPLEMENTED!
    }

    /// <summary>
    /// Cannot pass selection Ray as null value, so it's wrapped into a nullable type
    /// </summary>
    public static bool isSomethingHit(out RaycastHit hit, LayerMask layerMask, List<Ray> customSelectionRay) {
        Ray ray = (customSelectionRay == null ? Camera.main.ScreenPointToRay(Input.mousePosition) : customSelectionRay[0]);

        rayThatCausedSelection = ray;//TODO temp
        return Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask);
    }

    public static bool isSomethingHit(out RaycastHit hit) {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        return Physics.Raycast(ray, out hit, Mathf.Infinity);
    }

    private bool isHitUI() {
        PointerEventData param1 = new PointerEventData(EventSystem.current);
        param1.position = Input.mousePosition;

        List<RaycastResult> param2 = new List<RaycastResult>();

        foreach (GraphicRaycaster rayCaster in rayCastersList) {
            rayCaster.Raycast(param1, param2);

            if (param2.Count > 0) {
                //print(rayCaster.gameObject.name + " WAS HIT BY MOUSE RAY");
                return true;
            }

        }

        return false;
    }


    //TODO do like MapSelector.GUI.GetComponentsInChildren(true)
    public void RegisterGraphicRaycaster(GraphicRaycaster raycaster) {
        //print("Adding raycaster: " + raycaster.name);
        rayCastersList.Add(raycaster);
    }
}
