﻿using UnityEngine;
using System.Linq;

public class MissionFactory : Singleton<MissionFactory> {
    private static Object missionPrefab;

    void OnEnable() {
        missionPrefab = Resources.Load(PrefabConstants.MISSION_PREFAB);
    }

    public Mission Build(string missionName, MissionType missionType, Structure structure, params MissionGoal[] goals) {

        if (!(missionType == MissionType.DESTROY)) {
            throw new System.ArgumentException();
        }

        Mission mission = (Instantiate(missionPrefab) as GameObject).GetComponent<Mission>();
        mission.descriptor.targetStructure = structure;

        mission.gameObject.name = missionName;
        mission.descriptor.missionName = missionName;
        mission.descriptor.type = missionType;

        mission.AddGoals(goals);

        return mission;
    }

}
