﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tester : Singleton<Tester> {

    void Update() {

        if (Input.GetKeyDown(KeyCode.B)) {
            if (MapSelector.Instance.selectedAgent != null) {
                MapSelector.Instance.selectedAgent.xp.AddXP(10);
            }
        }

        if (Input.GetKeyDown(KeyCode.X)) {
            if (MapSelector.Instance.selectedStructure != null && MapSelector.Instance.selectedAgent != null) {
                MapSelector.Instance.selectedStructure.health.ApplyDamage(30);
            }

        }

        if (Input.GetKeyDown(KeyCode.O)) {
            GuiManager.Instance.showStatisticsWindow(true);
        }

        if (Input.GetKeyDown(KeyCode.P)) {
            GuiManager.Instance.showStatisticsWindow(false);
        }

        if (Input.GetKeyDown(KeyCode.G)) {
            GuiManager.Instance.statisticsPanel.tableAgentsSummary
                .AddRow(TableRowFactory.Instance.Build(MapSelector.Instance.selectedAgent));
        }

    }


}
