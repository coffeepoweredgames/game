﻿using UnityEngine;


public class TableRowFactory : Singleton<TableRowFactory> {
    private static Object rowAgentsSummary;

    void OnEnable() {
        rowAgentsSummary = Resources.Load(PrefabConstants.ROW_AGENTS_SUMMARY_PREFAB);
    }

    public RowAgentsSummary Build(Agent agent) {
        RowAgentsSummary row = (Instantiate(rowAgentsSummary) as GameObject).GetComponent<RowAgentsSummary>();

        if (agent == null) { return row; }

        //each row tied to an agent
        row.agent = agent;

        row.SetValue(agent);

        return row;
    }

}
