﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Mover : Singleton<Mover> {
	public MovementContextManager movementManager{get;set;}
	private MapSelector mapSelector;
	private GuiManager guiManager;
	
	void Start(){
		movementManager = MovementContextManager.Instance;
		mapSelector = MapSelector.Instance;
		guiManager = GuiManager.Instance;
	}

	public void TryMoving(){

		Agent agent = mapSelector.selectedAgent;
		
		if(agent == null){
			return;
		}
		
		Province selectedProvince =  mapSelector.selectedProvince;
		
		if(selectedProvince == null){
			return;
		}
		
		if(agent.isLocatedHere(selectedProvince)){
            //Debug.Log ("Agent already located here: "+Time.frameCount);
			return;
		}

        //Debug.Log (string.Format("AM.movingToProvince from {0} to {1} in frame {2}", 
        //           agent.location.province.descriptor.provinceName, 
        //           selectedProvince.descriptor.provinceName,
        //           Time.frameCount));
		
		MoveAgentToProvince(agent, selectedProvince, true);
	}

    /// <summary>
    /// Agents are moved from province to province.
    /// </summary>
    /// <param name="agent"></param>
    /// <param name="province"></param>
    /// <param name="isInteractive"></param>
	public void MoveAgentToProvince(Agent agent, Province province, bool isInteractive){
		if(movementManager.isMovingAlreadyThere(agent, province)){
			return;
		}

		//update movement context
		movementManager.Add(agent.descriptor, province);

		if(!agent.isLocatedHere(province.country)){

			//player interaction needed
			if(isInteractive){
				guiManager.showCountryBorderHud(province.country, province, agent, true);
				return;
			}

			//no player interaction needed
			province.country.OnCountryBorderEnter(province, agent);//TODO funny api
			province.country.OnCountryEnter(agent);
		}

		//province callbacks
		province.OnProvinceBorderEnter(agent);
		province.OnProvinceEnter(agent);

		movementManager.Remove(agent.descriptor, province);
	}

    /// <summary>
    /// Used when spawning agents inside provinces, no border checking
    /// </summary>
    /// <param name="agent"></param>
    /// <param name="province"></param>
	public void AddAgentToProvince(Agent agent, Province province){//TODO move elsewhere
		province.OnProvinceEnter(agent);
	}
}
