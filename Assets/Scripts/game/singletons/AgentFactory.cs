﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Agent factory build Agents
/// </summary>
public class AgentFactory : Singleton<AgentFactory> {
    private static Object agentPrefab;

    void OnEnable() {
        agentPrefab = Resources.Load(PrefabConstants.AGENT_PREFAB);
    }

    /*
     This method is used for ingame agent creation
     */
    public Agent Build(string name, string surname, string portraitName) {
        Agent agent = (Instantiate(agentPrefab) as GameObject).GetComponent<Agent>();
        agent.name = name + " " + surname;

        AgentDescriptor descriptor = agent.descriptor;
        descriptor.agentName = name + " " + surname; //!=name

        descriptor.portraitId = SpriteProvider.getSprite().id;//assign a random sprite

        agent.health.health = AgentConstants.HEALTH_NORMAL;

        DynamicObjectRegistry.AddAgent(descriptor, agent);

        GuiManager.Instance.statisticsPanel.tableAgentsSummary.AddRow(TableRowFactory.Instance.Build(agent));

        agent.OnAgentCreated();

        return agent;
    }

    /*
     This method is used for rebuilding game data from saved data.
     */
    public Agent Rebuild(AgentData agentData) {
        Agent agent = (Instantiate(agentPrefab) as GameObject).GetComponent<Agent>();

        //initialize agent with static data
        PersistenceMapper.toComponent(agentData, agent);

        agent.name = agent.descriptor.agentName;

        print("agent.descriptor.agentName: " + agent.descriptor.agentName);
        print("agent.descriptor.name: " + agent.descriptor.name);

        //resolve dynamic data
        print("Looking up province " + agent.location.province.descriptor.provinceName + "in DynamicObjectRegistry");
        Province originalProvince = DynamicObjectRegistry.GetProvince(agent.location.province.descriptor);

        DynamicObjectRegistry.AddAgent(agent.descriptor, agent);

        //restore agent to original province
        Mover.Instance.MoveAgentToProvince(agent, originalProvince, false);

        return agent;
    }

}
