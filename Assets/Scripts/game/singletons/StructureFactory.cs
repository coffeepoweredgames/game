﻿using UnityEngine;

public class StructureFactory : Singleton<StructureFactory> {
    private static Object factoryPrefab;

    void OnEnable() {
        factoryPrefab = Resources.Load(PrefabConstants.FACTORY_STRUCTURE_PREFAB);
    }

    public Structure Build(StructureType structureType, string structureName) {
        Structure structure = null; ;

        switch (structureType) {
            case StructureType.FACTORY: {
                    structure = (Instantiate(factoryPrefab) as GameObject).GetComponent<Structure>();
                    structure.health.health = StructureConstants.HEALTH_NORMAL;
                    structure.intel.intelPoints = StructureConstants.INTEL_OWNED_BY_STRUCTURES;
                    //structure.gameObject.transform.localScale = new Vector3(1.05f, 1.05f, 1.05f);//TODO TEMP
                    break;

            }
            case StructureType.AIRPORT: {
                    break;

            }
            default: {
                    break;
                }
        }



        structure.structureDescriptor.id = -1;
        structure.structureDescriptor.structureName = structureName;

        structure.gameObject.name = structureName;

        return structure;
    }
}
