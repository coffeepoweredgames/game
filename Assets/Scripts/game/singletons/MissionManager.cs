﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This component manages all mission related stuff.
/// Call Enroll (agent, mission) to assign, start mission and track its progress.
/// Provides callbacks for events OnMissionComplete, OnMissionFailure
/// </summary>
public class MissionManager : Singleton<MissionManager> {

    //mission manager state goes here
    private MissionManagerSaveData saveDataManager;

    //mission list save data
    private MissionListData missionList;

    private List<Mission> inProgressMissions;

    private List<Mission> completedMissions;

    private Dictionary<AgentDescriptor, MissionProgress> progressMap = new Dictionary<AgentDescriptor, MissionProgress>();

    public bool Enroll(Agent agent, Mission mission) {
        if (MissionManager.Instance.isAgentOnMission(agent)) {
            print("Agent already enrolled on mission " + progressMap[agent.descriptor].mission.descriptor.missionName + " size: " + progressMap.Count);
            Destroy(mission.gameObject);
            return false; ;
        }

        print(string.Format("Agent {0} enrolled on mission {1}", agent.descriptor.agentName, mission));

        if (agent == null || mission == null) {
            GameLogger.printRed(string.Format("Cannot enroll invalid {0} {1}", agent, mission));
            return false;
        }

        MissionProgress progress = MissionProgressFactory.Instance.Build(agent, mission);

        //track agent and enrolled mission
        progressMap.Add(agent.descriptor, progress);

        Notifier.Instance.notify(EventFactory.Build(progress));

        return true;
    }

    public bool isAgentOnMission(Agent agent) {
        return progressMap.ContainsKey(agent.descriptor);
    }

    public bool isAgentOnMissionHere(Agent agent, Structure structure) { 
        if(!isAgentOnMission(agent) ){
            return false;
        }

        MissionProgress progress = progressMap[agent.descriptor];

        //print(string.Format("Agents {0}", agent.descriptor.agentName, structure.structureDescriptor.structureName));
        //print(string.Format("Incoming structure {0}", structure.structureDescriptor.structureName));
        //print(string.Format("Mission structure {0}", progress.mission.descriptor.targetStructure.structureDescriptor.structureName));

        bool result = false;
        if (progress.mission.descriptor.type.isStructureRelated()) {
            result = structure.structureDescriptor.Equals(progress.mission.descriptor.targetStructure.structureDescriptor);
        }


        if (result) {
            //print(string.Format("Agent {0} is already on a mission related to the structure {1} ", agent.descriptor.agentName, structure.structureDescriptor.structureName));
            return true;
        }
        else {
            //print("NOT RELATED STRUCTURE");
        }

        return false;    
    }

    public void OnGoalComplete(MissionGoal goal) {
        Notifier.Instance.notify(EventFactory.Build(goal));
    }

    public void OnMissionComplete(MissionProgress progress) {
        print("MM.OnMissionComplete");

        Notifier.Instance.notify(EventFactory.Build(progress));

        deleteProgress(progress);
    }

    public void OnMissionFailure(MissionProgress progress) {
        //TODO update global stats
        //TODO update stats for agent

        print("MM.OnMissionFailure");

        Notifier.Instance.notify(EventFactory.Build(progress));

        deleteProgress(progress);
    }

    private void deleteProgress(MissionProgress progress) {
        progressMap.Remove(progress.agent.descriptor);
        Destroy(progress.mission.gameObject);
        Destroy(progress.gameObject);
    }
}
