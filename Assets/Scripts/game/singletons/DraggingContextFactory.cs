﻿using System.Collections;
using UnityEngine;


public class DraggingContextFactory : Singleton<DraggingContextFactory> {
    private static Object draggingContextPrefab;

    void OnEnable() {
        draggingContextPrefab = Resources.Load(PrefabConstants.DRAGGING_CONTEXT_PREFAB);
    }

    public DraggingContext Build(Agent agent) {
        DraggingContext dc = (Instantiate(draggingContextPrefab) as GameObject).GetComponent<DraggingContext>();
        dc.agent = agent;
        return dc;
    }

}