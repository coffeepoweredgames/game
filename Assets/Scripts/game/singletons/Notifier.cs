﻿using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Notifies game that some event occured.
/// </summary>
public class Notifier : Singleton<Notifier>, AgentEventListener {
    private GuiManager guiManager;

    void OnEnable() {
        guiManager = GuiManager.Instance;
    }

    public void notify(GameEvent gameEvent) {

        guiManager.eventQueue.Enqueue(gameEvent);
    }

    void AgentEventListener.OnAgentCreated(Agent agent) {
        //TODO ignored
    }

    void AgentEventListener.OnAgentKilled(Agent agent) {
        notify(EventFactory.Build("Agent killed", agent.descriptor.agentName));    
    }

    void AgentEventListener.OnAgentCaptured(Agent agent) {
        //TODO ignored
    }
}