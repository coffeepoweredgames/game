﻿using UnityEngine;

public class DraggingContext : GameBase {
    public Agent agent { get; set; }//owner of context aka the entity being dragged
    private Vector3 agentsLastPosition;
    private const float CHANGE_THRESHOLD = 2f;
    public bool isDragInProgress { get; set; }

    void Start() {
        InvokeRepeating(CoroutineConstants.STORE_LAST_POSITION, 0, 0.2f); //TODO use coroutine
    }
    
    //void Update() {
    //    Vector3 inboundVector = (france.transform.position - agent.transform.position).normalized;
    //    Vector3 draggingDirectionVector = agent.draggingContext.getDraggingDirection();

    //    bool b = CountryBorder.isInboundMovement(agent, france);

    //    Debug.DrawRay(agent.transform.position + new Vector3(0, 0, -10), agent.draggingContext.getDraggingDirection() * 10, (b ? Color.blue : Color.red));
    //    Debug.DrawRay(agent.transform.position + new Vector3(0, 0, -10), (france.transform.position - agent.transform.position).normalized * 10, Color.blue);
    //}                                                            


    private void StoreLastPosition() {
        Vector3 currentPosition = agent.transform.position;
        if (isPositionChangeRelevant(agentsLastPosition, currentPosition)) {
            //GameLogger.printBlue("Updated DraggingContext.StoreLastPosition" + "last:" + agentsLastPosition + " current: " + currentPosition);
            agentsLastPosition = currentPosition;
        }
    }

    public Vector3 getDraggingDirection() {
        Vector3 currentPosition = agent.transform.position;
        if (currentPosition == agentsLastPosition) {
            GameLogger.printRed("WARN:: DraggingContext.getDraggingDirection IS ZERO VECTOR!");
        }

        return (currentPosition - agentsLastPosition).normalized;
    }


    private bool isPositionChangeRelevant(Vector3 oldPosition, Vector3 newPosition) {
        if (oldPosition == Vector3.zero) { 
            return true;
        }

        if (Mathf.Abs(oldPosition.x - newPosition.x) >= CHANGE_THRESHOLD) { return true; }
        if (Mathf.Abs(oldPosition.y - newPosition.y) >= CHANGE_THRESHOLD) { return true; }
        if (Mathf.Abs(oldPosition.z - newPosition.z) >= CHANGE_THRESHOLD) { return true; }

        return false;
    }


}

