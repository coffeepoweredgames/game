﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Structures : GameBase {
	
	public  List<Structure> GetStructureList(){
        return new List<Structure>(GetComponentsInChildren<Structure>());//TODO not like this
	}

    public int GetStructureCount() {
        return transform.childCount;
    }
	
}

