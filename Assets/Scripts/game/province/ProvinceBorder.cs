﻿using UnityEngine;
using System.Collections;

public class ProvinceBorder : GameBase {
    private Province province;

    void Awake() {
        province = gameObject.GetComponent<Province>();
    }

    public void OnProvinceBorderEnter(Agent agent) {
        //string eventMessage = string.Format("Agent {0} crossed {1} border on frame {2}!", agent.descriptor.name, province.descriptor.provinceName, Time.frameCount);

        //Debug.Log("OnProvinceBorderEnter: " + eventMessage);

        //Notifier.Instance.notify(EventFactory.Build(province.descriptor.provinceName, "ENTERED"));
    }

    public void OnProvinceBorderStay(Agent agent) {
        Debug.Log(string.Format("Agent {0} stays on {1} province!", agent, province.descriptor.provinceName));
    }

    public void OnProvinceBorderExit(Agent agent) {
        agent.transform.parent = null;//detach from parent
        agent.location.Clear();

        string eventMessage = string.Format("Agent {0} exited {1} province!", agent, province.descriptor.provinceName);

        Debug.Log("OnProvinceBorderExit: " + eventMessage);


        //Notifier.Instance.notify(EventFactory.Build(province.descriptor.provinceName, "EXITED"));

    }
}
