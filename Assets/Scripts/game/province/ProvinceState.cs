﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProvinceState : GameBase {
    private Dictionary<string, GameObject> cities { get; set; }

    private Province province;

    void Awake() {
        province = gameObject.GetComponentInParent<Province>();
    }

    public void OnProvinceEnter(Agent agent) {
        //remove old location
        agent.location.Clear();
        agent.transform.parent = null;//detach from parent

        //add new location
        agent.location.SetLocation(province.country, province, null);

        //parent
        agent.transform.parent = province.agents.gameObject.transform;
        
        //position
        agent.aboveOf(province.gameObject);

        //Debug.Log(string.Format("Agent {0} entered province {1}", agent.GetComponent<AgentDescriptor>().agentName, province.descriptor.provinceName));

    }

    public void OnProvinceExit(Agent agent) {
        //Debug.Log("Called ProvinceState.OnProvinceExit.");
    }


    public void AddStructure(Structure structure) {
        structure.transform.parent = null;

        //add new location
        structure.location.SetLocation(province.country, province, null);

        //parent
        structure.transform.parent = province.structures.gameObject.transform;

        float tempPosition = province.structures.GetStructureCount() > 0 ? province.structures.GetStructureCount() - 1 * 0.7f : 0f;
        structure.transform.localPosition = new Vector3(tempPosition, tempPosition, 0);//TODO

        //TODO dont adjust scale here
        structure.gameObject.transform.localScale = new Vector3(1.05f, 1.05f, 1.05f);

    }

}
