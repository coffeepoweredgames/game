﻿using UnityEngine;
using System.Collections;

public class ProvinceDescriptor  : GameBase{

	public int id {get;  set;}
	
	public string provinceName {get;  set;}

    public bool isValid() {
        if (provinceName == null || provinceName.Equals("")) {
            return false;
        }
        return true;
    }

}
