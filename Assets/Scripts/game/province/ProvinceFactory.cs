using UnityEngine;
using System.Collections;

/// <summary>
/// Builds provinces.
/// Doesnt position provinces.
/// </summary>
public class ProvinceFactory : Singleton<ProvinceFactory> {

    private static Object provincePrefab;

    void OnEnable() {
        provincePrefab = Resources.Load(PathConstants.PREFAB_PROVINCE);
    }

    public Province Build(int id, string provinceName) {
        GameObject instance = Instantiate(provincePrefab) as GameObject;
        instance.name = provinceName;

        ProvinceDescriptor descriptor = instance.GetComponent<Province>().descriptor;
        descriptor.id = id;
        descriptor.provinceName = provinceName;

        Province province = instance.GetComponent<Province>();

        province.entityMesh.SetupMesh(Resources.Load<Mesh>(PathConstants.MESH_PROVINCES + "/" + provinceName), ColorConstants.PROVINCE_COLOR);

        DynamicObjectRegistry.AddProvince(descriptor, province);

        return province;
    }

    /// <summary>
    /// TODO temporary method. Used for building dummy provinces which have the same shape as the parent country.
    /// When we draw all provinces manually, delete this factory method
    /// </summary>
    public Province Build(int id, string provinceName, string countryMeshAsProvinceMesh) {//TODO temp countryMeshAsProvinceMesh
        Province province = (Instantiate(provincePrefab) as GameObject).GetComponent<Province>();
        province.name = provinceName;

        province.descriptor.id = id;
        province.descriptor.provinceName = provinceName;

        province.entityMesh.SetupMesh(Resources.Load<Mesh>(countryMeshAsProvinceMesh), ColorConstants.PROVINCE_COLOR);

        DynamicObjectRegistry.AddProvince(province.descriptor, province);

        return province;
    }
}
