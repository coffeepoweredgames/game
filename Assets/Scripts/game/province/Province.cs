﻿using UnityEngine;


[RequireComponent(typeof(ProvinceBorder))]
[RequireComponent(typeof(ProvinceDescriptor))]
public class Province : GameBase {
    public ProvinceState state { get; set; }
    public EntityMesh entityMesh { get; set; }
    public ProvinceDescriptor descriptor { get; set; }
    public Country country { get; set; }
    public Agents agents { get; set; }
    public Structures structures { get; set; }
    public ProvinceBorder border { get; set; }
    public Vector3 positionUnique { get { return transform.position + getOffset(); } }//coordinate relative to country

    void Awake() {
        state = GetComponentInChildren<ProvinceState>();
        entityMesh = GetComponentInChildren<EntityMesh>();
        descriptor = GetComponentInChildren<ProvinceDescriptor>();
        border = GetComponentInChildren<ProvinceBorder>();
        agents = GetComponentInChildren<Agents>();
        structures = GetComponentInChildren<Structures>();
    }

    //province interface
    public void OnProvinceEnter(Agent agent) {
        state.OnProvinceEnter(agent);
    }

    public void OnProvinceExit(Agent agent) {
        state.OnProvinceExit(agent);
    }

    //border entry event
    public void OnProvinceBorderEnter(Agent agent) {
        border.OnProvinceBorderEnter(agent);
    }

    public void OnProvinceBorderExit(Agent agent) {
        border.OnProvinceBorderExit(agent);
    }

    public void AddStructure(Structure structure) {
        state.AddStructure(structure);
    }

    //Currently all provinces share same world space position
    //This offset in world space is used to create a unique position per province <code>(provincePositionWS = transform.position + offset)</code>
    //@see CountryConstants.PROVINCE_WORLDSPACE_OFFSETS
    private Vector3 getOffset() {
        if (CountryConstants.PROVINCE_WORLDSPACE_OFFSETS.ContainsKey(descriptor.provinceName)) {
            return CountryConstants.PROVINCE_WORLDSPACE_OFFSETS[descriptor.provinceName];
        }

        return Vector3.zero;
    }

}
