﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Provinces : MonoBehaviour {

	public void AddProvinces(List<Province> provinces){
		foreach(Province p in provinces){
			p.transform.parent = gameObject.transform;
		}
	}

    public List<Province> GetProvinceList() {
        return new List<Province>(GetComponentsInChildren<Province>());//TODO not like this
    }


}
