﻿using UnityEngine;
using System.Collections;

public class CountrySelector : MonoBehaviour {

    private Color originalColor;
    bool selected = false;
    public void select() {
        //originalColor = transform.parent.gameObject.renderer.material.color;
        //transform.parent.gameObject.renderer.material.color = TileMapConstants.COLOR_TILE_SELECTED;
        originalColor = gameObject.GetComponent<Renderer>().material.color;
        gameObject.GetComponent<Renderer>().material.color = MapConstants.COLOR_TILE_SELECTED;
        Debug.Log("MeshSelector selected.");
    }
    public void deselect() {
        //transform.parent.gameObject.renderer.material.color = originalColor;
        gameObject.GetComponent<Renderer>().material.color = originalColor;
        Debug.Log("MeshSelector deselected.");
        return;
    }

    //void OnMouseOver() {

    //}


    void OnMouseDown() {
        if (selected) {
            deselect();
            selected = false;
        }
        else {
            select();
            selected = true;
        }
    }
}
