﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(CountryDescriptor))]
[RequireComponent(typeof(CountryProperties))]
public class Country : GameBase {
	private const bool SEARCH_INACTIVE = true;
	
	public Area area;
	public CountryBorder border;
	public CountryState state;
	public Provinces provinces;
	public CountryDescriptor descriptor;

	void Awake(){
		area = gameObject.GetComponentInChildren<Area>();
		border = gameObject.GetComponentInChildren<CountryBorder>();
		state = gameObject.GetComponentInChildren<CountryState>();
		provinces = gameObject.GetComponentInChildren<Provinces>();
		descriptor = gameObject.GetComponent<CountryDescriptor>();
	}

	//country interface
	public void OnCountryEnter(Agent agent){
		state.OnCountryEnter(agent);
	}

	//country border callbacks
	public void OnCountryBorderEnter(Province provinceInCountryToBeEntered, Agent agent){

        border.OnCountryBorderEnter(provinceInCountryToBeEntered, agent);
	}
	
	public void OnCountryBorderStay(Agent agent){
		border.OnCountryBorderStay(agent);
	}
	
	public void OnCountryBorderExit(Agent agent){
		border.OnCountryBorderExit(agent);
	}


}
