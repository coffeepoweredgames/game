﻿using UnityEngine;
using System.Collections;

/// <summary>
/// List of all countries
/// </summary>
public class Countries{
	public static readonly Descriptor CROATIA = new Descriptor (1, "CROATIA");
	public static readonly Descriptor SERBIA = new Descriptor (2, "SERBIA");
	public static readonly Descriptor BOSNIA = new Descriptor (3, "BOSNIA");
	public static readonly Descriptor HUNGARY = new Descriptor (4, "HUNGARY");
	public static readonly Descriptor BULGARIA = new Descriptor (5, "BULGARIA");
	public static readonly Descriptor ROMANIA = new Descriptor (6, "ROMANIA");
	public static readonly Descriptor ALBANIA = new Descriptor (7, "ALBANIA");
	public static readonly Descriptor GREECE = new Descriptor (8, "GREECE");
	public static readonly Descriptor TURKEY = new Descriptor (9, "TURKEY");
	public static readonly Descriptor GERMANY = new Descriptor (10, "GERMANY");
	public static readonly Descriptor AUSTRIA = new Descriptor (11, "AUSTRIA");
	public static readonly Descriptor SWITZERLAND = new Descriptor (12, "SWITZERLAND");
	public static readonly Descriptor ITALY = new Descriptor (13, "ITALY");
	public static readonly Descriptor FRANCE = new Descriptor (14, "FRANCE");
	public static readonly Descriptor SPAIN = new Descriptor (15, "SPAIN");
	public static readonly Descriptor PORTUGAL = new Descriptor (16, "PORTUGAL");
	public static readonly Descriptor BELGIUM = new Descriptor (17, "BELGIUM");
	public static readonly Descriptor DENMARK = new Descriptor (18, "DENMARK");
	public static readonly Descriptor NETHERLANDS = new Descriptor (19, "NETHERLANDS");
	public static readonly Descriptor NORWAY = new Descriptor (20, "NORWAY");
	public static readonly Descriptor SWEDEN = new Descriptor (21, "SWEDEN");
	public static readonly Descriptor FINLAND = new Descriptor (22, "FINLAND");
	public static readonly Descriptor RUSSIA = new Descriptor (23, "RUSSIA");
	public static readonly Descriptor BRITAIN = new Descriptor (24, "BRITAIN");
	public static readonly Descriptor SCOTLAND = new Descriptor (25, "SCOTLAND");
	public static readonly Descriptor IRELAND = new Descriptor (26, "IRELAND");
	public static readonly Descriptor NORTHERN_IRELAND = new Descriptor (27, "NORTHERN_IRELAND");
}
