﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CountryState : GameBase{

	public CountryDescriptor GetDescriptor(){
		return gameObject.GetComponent<CountryDescriptor>();
	}

	public void OnCountryEnter(Agent agent){

	}

	public Dictionary<string, GameObject> provinces  = new Dictionary<string, GameObject>();//provinceName-provincePrefab
	public Dictionary<string, GameObject> citiesMap = new Dictionary<string, GameObject>();//cityName - cityPrefab
	public Dictionary<string, GameObject> agentsMap = new Dictionary<string, GameObject>();//agentName - agentPrefab
	
	private Dictionary<string, string> provinceCityMap = new Dictionary<string, string>();//provinceName-cityName
	//private Dictionary<string, string> agentCityMap = new Dictionary<string, string>();//agentName-cityName
	
	public void AddProvinceAndCity(GameObject province, GameObject city){
		//add province
		provinces.Add(province.GetComponent<ProvinceDescriptor>().provinceName, province);
		
		//add city
		citiesMap.Add(city.GetComponent<CityDescriptor>().cityName, city);
		
		//map province and city
		provinceCityMap.Add(province.GetComponent<ProvinceDescriptor>().provinceName, city.GetComponent<CityDescriptor>().cityName);
	}
	
	public Dictionary<string, GameObject> getProvinces(){return provinces;}
	
	public Dictionary<string, GameObject> getCities(){return citiesMap;}
	
	public Dictionary<string, GameObject> getAgents(){return agentsMap;}
	
	public Dictionary<string, GameObject> Provinces {
		get {
			return provinces;
		}
		set {
			provinces = value;
		}
	}
	
	public Dictionary<string, GameObject> Cities {
		get {
			return citiesMap;
		}
		set {
			citiesMap = value;
		}
	}
	
	public Dictionary<string, string> ProvinceCityMap {
		get {
			return provinceCityMap;
		}
		set {
			provinceCityMap = value;
		}
	}


	
}
