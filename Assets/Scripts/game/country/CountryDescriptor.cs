﻿using UnityEngine;
using System.Collections;

public class CountryDescriptor : GameBase {
    public int id { get; set; }

    public string countryName { get; set; }

    public bool isValid() {
        if (countryName == null || countryName.Equals("")) {
            return false;
        }
        return true;
    }

}
