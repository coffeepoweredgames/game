﻿using UnityEngine;
using System.Collections;

public class CountryProperties : GameBase {
	public BorderProperties borderProperties;
	public ProvinceProperties provinceProperties;

	private Country country;
	private Provinces provinces;//country.GetProvinces() doesnt find component on inactive g.o's, because of that i have a member

	void Start(){
		borderProperties = new BorderProperties();
		provinceProperties = new ProvinceProperties();
		country = GetComponent<Country>();
		provinces = country.provinces;
	}
	
	void Update () {
		if(provinceProperties.displayProvinces != provinces.gameObject.activeSelf ){
			provinces.gameObject.SetActive(provinceProperties.displayProvinces);
		}

		if(borderProperties.displayBorders != country.border.GetMeshRenderer().enabled){
			country.border.GetMeshRenderer().enabled = borderProperties.displayBorders;
		}
	
	}
}
