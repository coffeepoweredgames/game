using UnityEngine;
using System.Collections;

/// <summary>
///  After redesign of country prefab, there will be only one builder method 
/// </summary>
using System.Collections.Generic;

public class CountryFactory : Singleton<CountryFactory> {
    private static Object countryPrefab;
    private Dictionary<string, List<string>> mapProperties;
    private ProvinceFactory provinceFactory;

    public void OnEnable() {
        countryPrefab = Resources.Load(PrefabConstants.COUNTRY_PREFAB);
        mapProperties = PropertyLoader.load(PathConstants.MAP_PROPERTIES);
        provinceFactory = ProvinceFactory.Instance;
    }

    public Country Build(Descriptor incomingCntryDescriptor) {
        if (!CountryConstants.allowedCountries.Contains(incomingCntryDescriptor)) {
            Debug.LogError("CountryFactory.build error: " + incomingCntryDescriptor.name + " build not defined!!");
            return null;
        }


        Country country = (Instantiate(countryPrefab) as GameObject).GetComponent<Country>();
        country.gameObject.name = incomingCntryDescriptor.name;

        country.descriptor.id = incomingCntryDescriptor.id;
        country.descriptor.countryName = incomingCntryDescriptor.name;

        //area 
        Area area = country.area;
        area.GetMesh().SetupMesh(Resources.Load<Mesh>(country.name + "_AREA"), ColorConstants.COUNTRY_COLOR);

        //border
        CountryBorder border = country.border;//TODO replace border mesh, Unity5 doesnt allow concave triggers
        //border.mesh.SetupMesh(Resources.Load<Mesh>("borders/" + country.name + "_COUNTRY_BORDER"), ColorConstants.BORDER_COLOR);

        //provinces
        Provinces provinces = country.provinces;
        provinces.AddProvinces(BuildProvinces(country));
        provinces.AddProvinces(BuildDummyProvince(country));//TODO temp by name

        //state
        CountryState state = country.state;
        state.GetDescriptor().id = incomingCntryDescriptor.id;
        state.GetDescriptor().countryName = incomingCntryDescriptor.name;

        DynamicObjectRegistry.AddCountry(country.descriptor, country);

        return country;
    }

    private List<Province> BuildProvinces(Country country) {
        string provinceKey = (country.descriptor.countryName + ".province").ToLower();

        List<Province> provinces = new List<Province>();

        if (!mapProperties.ContainsKey(provinceKey)) {
            return provinces;
        }

        foreach (string provinceName in mapProperties[provinceKey]) {
            Province createdProvince = provinceFactory.Build(1, provinceName);
            createdProvince.country = country;
            provinces.Add(createdProvince.GetComponent<Province>());
        }

        return provinces;
    }

    private List<Province> BuildDummyProvince(Country country) {//TODO temp, every country will have a dummy province of same shape
        if (country.descriptor.countryName.Equals("FRANCE")) {
            return new List<Province>();
        }

        List<Province> dummyProvinces = new List<Province>();
        Province createdProvince = provinceFactory.Build(1, "PROVINCE_" + country.descriptor.countryName, country.descriptor.countryName + "_AREA");
        createdProvince.country = country;
        dummyProvinces.Add(createdProvince.GetComponent<Province>());

        return dummyProvinces;
    }



}