﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Nekaj : MonoBehaviour {
    public GameObject sampleButton = Resources.Load(PrefabConstants.SAMPLE_BUTTON_PREFAB) as GameObject;
    public GameObject contentPanel;


    void Start() {

        for (int i = 0; i < 5; i++) { 
           GameObject prefabInstance = Instantiate(sampleButton) as GameObject;
           SampleButtonTemp b = prefabInstance.GetComponent<SampleButtonTemp>();
           b.transform.SetParent(contentPanel.transform);//add event to panel
        }
    }



}

[System.Serializable]
public class Item {
    public Sprite icon;
    public string name;
    public string itemType;
}
