﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MissionConstants {

    public const float MISSION_DURATION_DEFAULT_SECONDS = 60;
    public const int XP_PER_GOAL = 30;
    public const int INTEL_PER_GOAL = 30;
    public const int DAMAGE_PER_GOAL = 100;



}
