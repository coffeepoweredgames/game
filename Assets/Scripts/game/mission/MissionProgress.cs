﻿
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Stores contextual info about mission.
/// - every completed goal earns you intel
/// - every completed goal earns you xp
/// - every failed goal costs you health
/// - goal with success = false but with more retries left is not considered failed
/// </summary>
public class MissionProgress : GameBase {

    public Mission mission { get; set; }
    public Agent agent { get; set; }
    private MissionGoal currentGoal;
    public List<MissionGoal> goalsToComplete { get; set; }
    public List<MissionGoal> completedGoals = new List<MissionGoal>();
    public List<MissionGoal> successfulGoals = new List<MissionGoal>();
    public List<MissionGoal> failedGoals = new List<MissionGoal>();

    public bool isProgressCompleted { get; set; }//for current mission, all goals have completed
    public MissionProgressStatus status { get; set; }

    void Awake() {
        status = MissionProgressStatus.STARTED;
    }

    void Update() {

        if (isProgressCompleted) {//progres completed with some result
            return;
        }

        if (currentGoal == null && goalsToComplete.Count == 0) {
            print("FINAL MISSION RESULT BY COMPARING failed vs. completed count!");
            if (successfulGoals.Count >= failedGoals.Count) { notifyMissionSuccess(); }//TODO better resolution
            else { notifyMissionFailure(); }
            return;
        }

        if (currentGoal == null) {
            currentGoal = getNextGoal();
            
        }

        if (currentGoal != null && currentGoal.isCompleted()) {
            if (currentGoal.isSuccess()) {
                print("SUCCESS FOR " + currentGoal.goalName);
                notifyGoalSuccess(currentGoal);
                currentGoal = null;
                return;
            }

            //failure

            if (currentGoal.isMissionCritical()) {
                print("mission critical failure " + currentGoal.goalName);
                notifyGoalFailure(currentGoal);
                notifyMissionFailure();
                return;
            }

            if (currentGoal.isRetriable()) {
                print("Retrying " + currentGoal.goalName);
                currentGoal.retry();
                return;
            }

            print("FAILURE FOR " + currentGoal.goalName);
            notifyGoalFailure(currentGoal);//all retries exhausted, was not mission critical
            currentGoal = null;
        }

    }

    private MissionGoal getNextGoal() {
        if (goalsToComplete.Count == 0) {
            return null;
        }

        MissionGoal g = goalsToComplete.First();
        goalsToComplete.Remove(g);
        return g;
    }

    private void notifyMissionSuccess() {
        isProgressCompleted = true;

        status = MissionProgressStatus.SUCCESS;

        print("NOTIFY MISSION SUCCESS");

        MissionManager.Instance.OnMissionComplete(this);
    }

    private void notifyMissionFailure() {
        isProgressCompleted = true;

        status = MissionProgressStatus.FAILURE;

        print("NOTIFY MISSION FAILURE");

        MissionManager.Instance.OnMissionFailure(this);
    }

    private void notifyGoalSuccess(MissionGoal goal) {
        if (!goal.isSuccess()) {
            GameLogger.printRed("notifyGoalSuccess called on unsuccesful goal!");
            return;
        }

        completedGoals.Add(currentGoal);
        successfulGoals.Add(currentGoal);

        agent.xp.AddXP(MissionConstants.XP_PER_GOAL);

        print(string.Format("{0} earned {1} XP points!", agent.descriptor.name, MissionConstants.XP_PER_GOAL));

        float intelEarned = mission.descriptor.targetStructure.intel.GetAndDecrement(MissionConstants.INTEL_PER_GOAL);

        print(string.Format("{0} earned {1} points of intel!", agent.descriptor.name, intelEarned));

        MissionManager.Instance.OnGoalComplete(currentGoal);
    }

    private void notifyGoalFailure(MissionGoal goal) {
        if (goal.isSuccess()) {
            GameLogger.printRed("notifyGoalFailure called on successful goal!");
            return;
        }

        completedGoals.Add(currentGoal);
        failedGoals.Add(currentGoal);

        agent.health.ApplyDamage(MissionConstants.DAMAGE_PER_GOAL);

        if (!agent.health.isAlive()) {
            agent.OnAgentKilled();
        }

        print(string.Format("{0} received {1} points of damage!", agent.descriptor.name, MissionConstants.DAMAGE_PER_GOAL));

        MissionManager.Instance.OnGoalComplete(currentGoal);
    }

}
