﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
///  Mission has multiple goals
/// </summary>
/// 
[RequireComponent(typeof(MissionDescriptor))]
public class Mission : GameBase {
    public MissionDescriptor descriptor { get; set; }

    public List<MissionGoal> goalList { get; set; }

    void Awake() {
        if (descriptor == null) {
            descriptor = GetComponent<MissionDescriptor>();//TODO dont like this
        }

        goalList = new List<MissionGoal>();
    }

    public void AddGoal(MissionGoal g) {
        g.mission = this;
        goalList.Add(g);
    }

    public void AddGoals(MissionGoal[] goals) {
        foreach (var g in goals) {
            g.mission = this;
        }
        goalList.AddRange(goals);
    }






}
