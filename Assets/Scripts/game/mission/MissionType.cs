﻿public enum MissionType {
    INFILTRATE,
    STEAL,
    SPY,
    ASSASINATE,
    DESTROY,
    SABOTAGE
}

/// <summary>
/// Enums cannot contain methods.
/// Utility class provides custom methods.
/// </summary>
static class MissionTypeUtil {

    public static bool isStructureRelated(this MissionType type) {
        switch (type) {
            case MissionType.INFILTRATE:
                return true;
            case MissionType.SABOTAGE:
                return true;
            case MissionType.DESTROY:
                return true;
            case MissionType.SPY:
                return true;
            case MissionType.ASSASINATE:
                return false;
            default:
                UnityEngine.MonoBehaviour.print("MissionType.Util.isStructureRelated :: Unknown mission type!");
                return false;
        }
    }

    public static bool isRetriable(this MissionType type) {
        switch (type) {
            case MissionType.INFILTRATE:
                return true;
            case MissionType.SPY:
                return true;
            case MissionType.ASSASINATE:
                return true;
            default:
                return false;
        }
    }
}
