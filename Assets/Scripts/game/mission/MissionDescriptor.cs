﻿using UnityEngine;
using System.Collections;

public class MissionDescriptor : GameBase {
    public string missionName { get; set; }

    public MissionType type { get; set; }

    public Structure targetStructure { get; set; }

    public Agent agent { get; set; }
}
