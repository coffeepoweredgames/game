﻿
public enum MissionProgressStatus {
    STARTED,
    IN_PROGRESS,
    SUCCESS,
    FAILURE
}

