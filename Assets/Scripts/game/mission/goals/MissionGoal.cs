﻿using UnityEngine;
using System.Collections;

public abstract class MissionGoal {

    /// <summary>
    /// Each goal is part of a mission.
    /// </summary>
    public Mission mission;

    /// <summary>
    /// Goal name.
    /// </summary>
    public string goalName;

    /// <summary>
    ///     How many times can this particular goal be retried (i.e. infiltration attempts etc.)
    /// </summary>
    public int retryCount;

    /// <summary>
    ///     How long can the goal execution take place.
    /// </summary>
    public int durationSeconds;

    /// <summary>
    ///  If success = -1, then goal is not completed yet. 
    /// </summary>
    public int successMarker = -1;

    /// <summary>
    /// When did goal execution start
    /// </summary>
    public float startTimeSeconds;

    /// <summary>
    /// When did goal execution end
    /// </summary>
    public float endTimeSeconds;

    /// <summary>
    ///     Mission critical goal failure triggers mission failure.
    /// </summary>
    public bool missionCritical;

    /// <summary>
    ///     Is goal completed (may be success or failure)
    /// </summary>
    /// <returns></returns>
    public abstract bool isCompleted();

    /// <summary>
    ///     Is goal completed successfully
    /// </summary>
    /// <returns></returns>
    public abstract bool isSuccess();

    /// <summary>
    ///     Mission critical goal failure triggers mission failure.
    /// </summary>
    /// <returns></returns>
    public abstract bool isMissionCritical();

    /// <summary>
    ///     Is goal retriable.
    /// </summary>
    /// <returns>true if allowedRetryCount > 0 </returns>
    public abstract bool isRetriable();

    public abstract bool retry();


}
