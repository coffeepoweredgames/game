﻿using UnityEngine;
using System.Collections;

public class MissionGoalFactory : Singleton<MissionGoalFactory> {
    private string goalName;

    private int retryCount;

    private int durationSeconds;

    private bool missionCritical;

    public MissionGoalFactory withGoalName(string goalName) {
        this.goalName = goalName;
        return this;
    }

    public MissionGoalFactory withRetryCount(int retryCount) {
        this.retryCount = retryCount;
        return this;
    }

    public MissionGoalFactory withDurationSeconds(int durationSeconds) {
        this.durationSeconds = durationSeconds;
        return this;
    }

    public MissionGoalFactory withMissionCritical(bool missionCritical) {
        this.missionCritical = missionCritical;
        return this;
    }

    public DestroyStructureGoal BuildDestroyStructureGoal(Structure targetStructure) {
        DestroyStructureGoal goal = BuildCommon(new DestroyStructureGoal()) as DestroyStructureGoal;

        goal.target = targetStructure;

        return goal;
    }

    public SabotageStructureGoal BuildSabotageStructureGoal(Structure targetStructure) {
        SabotageStructureGoal goal = BuildCommon(new SabotageStructureGoal()) as SabotageStructureGoal;

        goal.target = targetStructure;

        return goal;
    }

    public InfiltrateStructureGoal BuildInfiltrateStructureGoal(Structure targetStructure) {
        InfiltrateStructureGoal goal = BuildCommon(new InfiltrateStructureGoal()) as InfiltrateStructureGoal;

        goal.target = targetStructure;

        return goal;
    }

    private MissionGoal BuildCommon(MissionGoal goal) {
        goal.goalName = goalName;
        goal.retryCount = retryCount;
        goal.durationSeconds = durationSeconds;
        goal.missionCritical = missionCritical;
        clear();
        return goal;
    }

    /// <summary>
    /// Reset builder properties after goal built.
    /// </summary>
    private void clear() {
        goalName = null;
        retryCount = 0;
        durationSeconds = 0;
        missionCritical = false;
    }

}
