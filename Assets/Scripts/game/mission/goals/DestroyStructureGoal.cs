﻿using UnityEngine;

public class DestroyStructureGoal : MissionGoal {
    public Structure target { get; set; }

    public override bool isCompleted() {
        if (endTimeSeconds > 0) {//already ended
            return true;
        }

        if (startTimeSeconds == 0) {//not started, will be started
            startTimeSeconds = Time.time;
            return false;
        }

        if (Time.time - startTimeSeconds >= durationSeconds) {
            endTimeSeconds = Time.time;
            return true;
        }

        return false;
    }

    public override bool isMissionCritical() {
        return missionCritical;
    }

    public override bool isRetriable() {
        return retryCount > 0;
    }

    public override bool isSuccess() {

        if (successMarker == 0) { return false; }

        if (successMarker == 1) { return true; }

        int randomNumber = -1;

        if (successMarker == -1) {
            randomNumber = UnityEngine.Random.Range(0, 2) % 2;//TODO custom logic goes here, try destroying structure and check structure status
        }

        if (true) {//TODO hardcoded true, should be only on randomNumber == 0 
            target.health.ApplyDamage(100);

            if (!target.health.isAlive()) {
                target.OnStructureDestroyed(this.mission.descriptor.agent);
                successMarker = 1;//marker used because no boolean reference type null(-1), true(1), false(0)
                return true;
            }

        }

        successMarker = 0;
        return false;
    }

    public override bool retry() {
        if (!isRetriable()) {
            return false;
        }

        startTimeSeconds = 0;
        endTimeSeconds = 0;
        successMarker = -1;
        retryCount--;
        return true;
    }
}
