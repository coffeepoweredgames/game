﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class EntityMesh : GameBase {

    public void SetupMesh(Mesh mesh, Color color) {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        MeshCollider meshCollider = GetComponent<MeshCollider>();
        meshCollider.sharedMesh = mesh;

        gameObject.GetComponent<Renderer>().material.color = color;
    }
}
