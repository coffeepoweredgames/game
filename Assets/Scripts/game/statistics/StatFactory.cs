﻿using UnityEngine;
using System.Collections;

public class StatFactory : Singleton<StatFactory> {
    private static Object agentStatPrefab;

    void OnEnable() {
        agentStatPrefab = Resources.Load(PrefabConstants.AGENT_STAT_PREFAB);
    }

    public AgentStats Build(Agent agent) {
        AgentStats stat = (Instantiate(agentStatPrefab) as GameObject).GetComponent<AgentStats>();
        stat.name += "_" + agent.descriptor.agentName;
        stat.agent = agent;
        return stat;
    }

}
