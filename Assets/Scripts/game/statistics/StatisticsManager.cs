﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StatisticsManager : Singleton<StatisticsManager>, AgentEventListener, StructureEventListener {
    public Dictionary<AgentDescriptor, AgentStats> TRACKED_AGENTS;

    public int activeAgentCount { get; set; }
    public int killedAgentCount { get; set; }
    public int structuresDestroyed { get; set; }
    private StatFactory statFactory;

    void Start() {
        TRACKED_AGENTS = new Dictionary<AgentDescriptor, AgentStats>();
        statFactory = StatFactory.Instance;
    }

    void AgentEventListener.OnAgentCreated(Agent agent) {
        trackIfNotAlreadyTracked(agent);

        activeAgentCount++;
    }

    void AgentEventListener.OnAgentCaptured(Agent agent) {
        throw new NotImplementedException();
    }

    void AgentEventListener.OnAgentKilled(Agent agent) {
        killedAgentCount++;
        activeAgentCount--;

        print("Agent " + agent.descriptor.agentName + " was killed!");
    }

    void StructureEventListener.OnStructureCreated(Structure structure) {
        throw new NotImplementedException();
    }

    void StructureEventListener.OnStructureDestroyed(Structure structure, Agent agent) {
        TRACKED_AGENTS[agent.descriptor].structuresDestroyedCount++;

        structuresDestroyed++;

        print("Agent " + agent.descriptor.agentName + " destroyed the structure " + structure.structureDescriptor.structureName);
    }

    private void trackIfNotAlreadyTracked(Agent agent) {
        if (TRACKED_AGENTS.ContainsKey(agent.descriptor)) {
            return;
        }

        TRACKED_AGENTS.Add(agent.descriptor, statFactory.Build(agent));
    }

    public bool areStatsTracked(Agent agent) {
        return TRACKED_AGENTS.ContainsKey(agent.descriptor);
    }
}
