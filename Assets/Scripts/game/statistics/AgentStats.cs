﻿using UnityEngine;
using UnityEngine.UI;

public class AgentStats : MonoBehaviour {
    public Agent agent { get; set; }
    public int structuresDestroyedCount { get; set; }
    public int structuresInfiltratedCount { get; set; }
    public int killedEnemySoldiersCount { get; set; }
}
