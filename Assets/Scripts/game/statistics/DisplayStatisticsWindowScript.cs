﻿using UnityEngine;

public class DisplayStatisticsWindowScript : MonoBehaviour {
    private GuiManager guiManager;
    private bool shouldDisplay;

    void OnEnable() {
        guiManager = GuiManager.Instance;
    }

    public void showWindow() {
        shouldDisplay = !shouldDisplay;

        guiManager.showStatisticsWindow(shouldDisplay);

    }


}
