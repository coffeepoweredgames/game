﻿using UnityEngine;
using System.Collections;

public class AgentDescriptor : GameBase {

    public int id { get; set; }

    public string agentName { get; set; }

    public int portraitId { get; set; }

    public bool isValid() {
        if (agentName == null || agentName.Equals("")) {
            return false;
        }
        return true;
    }


    public override bool Equals(object obj) {
        var other = obj as AgentDescriptor;

        if (other == null) {
            return false;
        }

        return this.id.Equals(other.id) && this.agentName.Equals(other.agentName) && this.portraitId.Equals(other.portraitId);
    }

    public override int GetHashCode() {
        unchecked // Overflow is fine, just wrap
        {
            int hash = 17;
            hash = hash * 23 + this.id.GetHashCode();
            hash = hash * 23 + (this.agentName != null ? this.agentName.GetHashCode() : 1);
            hash = hash * 23 + this.portraitId.GetHashCode();
            return hash;
        }
    }




}
