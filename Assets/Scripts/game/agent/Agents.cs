﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Agents : GameBase {
	
	public  List<Agent> GetAgentList(){
		return new List<Agent>(GetComponentsInChildren<Agent>());//TODO not like this
	}
	
	public int GetAgentCount(){
		return transform.childCount;
	}
	
}
