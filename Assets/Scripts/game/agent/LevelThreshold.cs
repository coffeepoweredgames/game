﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum LevelThreshold {
    Zero = 0,
    One = 100,
    Two = 200,
    Three = 300,
    Four = 400,
    Five = 500
}

static class Util {

    public static LevelThreshold nextLevel(this LevelThreshold type) {
        switch (type) {
            case LevelThreshold.One:
                return LevelThreshold.Two;
            case LevelThreshold.Two:
                return LevelThreshold.Three;
            case LevelThreshold.Three:
                return LevelThreshold.Four;
            default:
                return LevelThreshold.Five;
        }
    }

    public static LevelThreshold precedingLevel(this LevelThreshold type) {
        switch (type) {
            case LevelThreshold.One:
                return LevelThreshold.Zero;
            case LevelThreshold.Two:
                return LevelThreshold.One;
            case LevelThreshold.Three:
                return LevelThreshold.Two;
            case LevelThreshold.Four:
                return LevelThreshold.Three;
            default:
                return LevelThreshold.Four;
        }
    }
}
