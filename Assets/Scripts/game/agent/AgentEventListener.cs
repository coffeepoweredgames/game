﻿
interface AgentEventListener {

    void OnAgentCreated(Agent agent);

    void OnAgentKilled(Agent agent);

    void OnAgentCaptured(Agent agent);
}

