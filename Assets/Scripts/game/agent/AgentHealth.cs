﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class AgentHealth : MonoBehaviour, IHealth {
    public float health { get; set; }
    private Agent agent { get; set; }
    private bool heal;

    void Start() {
        agent = GetComponent<Agent>();
    }

    void Update() {
        healOnButtonPressDebug(); //TODO testing purpose
    }

    public void ApplyDamage(float damage) {
        health -= damage;
        health = Mathf.Clamp(health, 0, AgentConstants.HEALTH_NORMAL);
        print(string.Format("Apllied {0} points of damage to {1}. Bar should display {2}/{3}", damage, agent.descriptor.agentName, health, AgentConstants.HEALTH_NORMAL));
        refreshProgressBar();
    }

    public void ApplyDamage(Agent attacker, float damageAmount) {
        throw new NotImplementedException();
    }

    public void ApplyHealing(float healingAmount) {
        health += healingAmount;
        health = Mathf.Clamp(health, 0, AgentConstants.HEALTH_NORMAL);
        print(string.Format("Apllied {0} points of health to {1}. Bar should display {2}/{3}", healingAmount, agent.descriptor.agentName, health, AgentConstants.HEALTH_NORMAL));
        refreshProgressBar();
    }

    public bool isAlive() {
        return health > 0;
    }

    public HealthStatus GetHealthStatus() {
        if (isStable()) {
            return HealthStatus.STABLE;
        }

        if (isCritical()) {
            return HealthStatus.CRITICAL;
        }

        if (!isAlive()) {
            return HealthStatus.DEAD;
        }

        return HealthStatus.HEALTHY;
    }

    public void refreshProgressBar() {
        refreshProgressBar(this.health, GetHealthStatusColor());
    }

    private void refreshProgressBar(float health, Color color) {
        if (!agent.health) {
            return;
        }

        if (!agent.healthBar.GetColor().Equals(color)) {
            agent.healthBar.SetColor(color);
            agent.healthBar.SetPointsTextColor(color);
        }

        agent.healthBar.SetValue(health);

        agent.healthBar.SetPointsText(health, AgentConstants.HEALTH_NORMAL);
    }

    private bool isHealthy() {
        return health >= AgentConstants.HEALTH_STABLE;
    }

    private bool isStable() {
        return (health > AgentConstants.HEALTH_CRITICAL && health < AgentConstants.HEALTH_STABLE);
    }

    private bool isCritical() {
        return 0 < health && health <= AgentConstants.HEALTH_CRITICAL;
    }

    public Color GetHealthStatusColor() {
        switch (GetHealthStatus()) {
            case HealthStatus.HEALTHY:
                return ColorConstants.HEALTH_HEALTHY;
            case HealthStatus.STABLE:
                return ColorConstants.HEALTH_STABLE;
            default:
                return ColorConstants.HEALTH_CRITICAL;
        }
    }

    private void healOnButtonPressDebug() {
        if (Input.GetKeyDown(KeyCode.H)) {
            heal = true;
        }

        if (heal && agent.healthBar != null && agent.healthBar.GetValue() < AgentConstants.HEALTH_NORMAL) {
            agent.health.ApplyHealing(Mathf.MoveTowards(agent.health.health, AgentConstants.HEALTH_NORMAL, 1));
        }

        if (heal && agent.healthBar != null && agent.healthBar.GetValue() == AgentConstants.HEALTH_NORMAL) {
            print(string.Format("Autoheal for {0} completed!", agent.descriptor.agentName));
            heal = false;
        }
    }
}
