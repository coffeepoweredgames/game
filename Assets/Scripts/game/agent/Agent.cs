﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(AgentDescriptor))]
[RequireComponent(typeof(LocationDescriptor))]
[RequireComponent(typeof(AgentHealth))]
[RequireComponent(typeof(Document))]
[RequireComponent(typeof(WirelessRadioImpl))]
[RequireComponent(typeof(Experience))]
public class Agent : GameBase, IGuiOwner {
    public LocationDescriptor location { get; set; }
    public AgentDescriptor descriptor { get; set; }
    public Country country { get; set; }
    public MouseDragger mouseDragger { get; set; }
    public bool isCountryEntryApproved;
    public DraggingContext draggingContext;//IDraggable
    public Experience xp { get; set; }
    public AgentHealth health { get; set; }
    public ProgressBar xpBar { get; set; }
    public ProgressBar healthBar { get; set; }
    private StatisticsManager statManager;
    private List<AgentEventListener> agentEventListeners = new List<AgentEventListener>();

    void Awake() {
        country = GetComponentInParent<Country>(); // replace with location.country?;
        location = GetComponent<LocationDescriptor>();
        descriptor = GetComponent<AgentDescriptor>();
        mouseDragger = GetComponent<MouseDragger>();
        xp = GetComponent<Experience>();
        health = GetComponent<AgentHealth>();
    }

    void OnEnable() {
        agentEventListeners.Add(StatisticsManager.Instance);
        agentEventListeners.Add(Notifier.Instance);
    }

    public bool isLocatedHere(Country c) {
        return location.isLocationInCountry(c);
    }

    public bool isLocatedHere(Province p) {
        return location.isLocationInProvince(p);
    }

    public void aboveOf(GameObject targetObject) {//TODO replace with singleton calls in code
        Debug.Log(gameObject.transform.gameObject.name);

        GetPositioner().above(gameObject, targetObject);
    }

    public void refreshGui() {
        health.refreshProgressBar();
        xp.refreshProgressBar();
    }

    public void OnAgentCreated() {
        agentEventListeners.ForEach(listener => listener.OnAgentCreated(this));
    }

    public void OnAgentCaptured() {
        agentEventListeners.ForEach(listener => listener.OnAgentCaptured(this));
    }

    public void OnAgentKilled() {
        agentEventListeners.ForEach(listener => listener.OnAgentKilled(this));

        MapSelector.Instance.DeselectAgent();

        gameObject.SetActive(false);//stats need to be displayed, cannot just destroy
    }

    public void clear() {
        if (xpBar) {
            xpBar = null;
        }

        if (healthBar) {
            healthBar = null;
        }
    }



}
