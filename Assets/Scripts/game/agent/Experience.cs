﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Experience : MonoBehaviour, IExperience {
    public int xp { get; set; }
    private Agent agent { get; set; }

    void Start() {
        agent = GetComponent<Agent>();
    }

    public void AddXP(int xp) {
        if (isLevelUp(this.xp, xp)) {
            print("LEVEL UP for " + agent.descriptor.agentName + " !");
        }

        this.xp = Mathf.Clamp(this.xp + xp, 0, (int)LevelThreshold.Five);

        refreshProgressBar();
    }

    public LevelThreshold GetLevel(float xp) {
        if (0 <= xp && xp <=(int)LevelThreshold.One) {
            return LevelThreshold.One;
        }

        if ((int)LevelThreshold.One < xp && xp <= (int)LevelThreshold.Two) {
            return LevelThreshold.Two;
        }

        if ((int)LevelThreshold.Two < xp && xp <= (int)LevelThreshold.Three) {
            return LevelThreshold.Three;
        }

        if ((int)LevelThreshold.Three < xp && xp <= (int)LevelThreshold.Four) {
            return LevelThreshold.Four;
        }

        return LevelThreshold.Five;
    }

    public bool isLevelUp(int oldXp, int newXp) {
        if (!GetLevel(xp).Equals(GetLevel(newXp))) {
            return true;
        }
        return false;
    }


    public void refreshProgressBar() {
        refreshProgressBar(this.xp);
    }

    private void refreshProgressBar(float xp) {
        if (!agent.xp) {
            return;
        }

        LevelThreshold level = GetLevel(this.xp);
        agent.xpBar.SetValueRange((int)level.precedingLevel(), (int)level);
        agent.xpBar.SetValue(xp);
        agent.xpBar.SetPointsText(xp, (int)GetLevel(xp));

        //print("Progress bar: "+agent.xpBar.slider.minValue + " - " + agent.xpBar.GetValue() + " - " + agent.xpBar.slider.maxValue+" dadad"+Time.deltaTime);

        print("Updating xp of " + agent.descriptor.agentName + " to " + xp + ". Bar should display value: " + agent.xpBar.GetValue());
    }
}
