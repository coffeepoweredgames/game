﻿using UnityEngine;
using System.Collections;

public class LocationDescriptor : MonoBehaviour {

	public Country country{get;set;}
	public Province province{get;set;}
	public City city{get;set;}
	
	public void SetLocation (Country country, Province province, City city){
		this.country = country;
		this.province = province;
		this.province = province;
	}
	
	public void Clear(){
		this.country = null;
		this.province = null;
		this.city = null;
	}

	public bool isLocationInProvince(Province province){
		if(province == null) {
			return false;
		}

		return this.province.descriptor.provinceName.Equals(province.descriptor.provinceName);
	}

	public bool isLocationInCountry(Country country){
		if(country == null) {
			return false;
		}
		
		return this.country.descriptor.countryName.Equals(country.descriptor.countryName);
	}

	public override string ToString (){
		return string.Format ("{0}, {1}, {2}", country, province, city);
	}
}
