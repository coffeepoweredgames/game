﻿using UnityEngine;
using System.Collections;

public class Document : GameBase {
	int validity = 100;
	
	public bool DoDocumentCheck(int scrutinyLevel){
		int temp = validity - scrutinyLevel;
		if(temp <0 ){
			return false;
		}
		return true;
	}
}
