﻿using UnityEngine;
using System.Collections;

public class MapSelectionInfo : MonoBehaviour {

	private GameObject selectedCountry;
	private Vector3 selectionNormal;


	MapSelectionInfo(GameObject selectedCountry, Vector3 selectionNormal){
		this.selectedCountry = selectedCountry;
		this.selectionNormal = selectionNormal;
	}

	public void setSelectedCountry(GameObject selectedCountry){
		this.selectedCountry = selectedCountry;
	}

	public void setSelectionNormal(Vector3 selectionNormal){
		this.selectionNormal = selectionNormal;
	}

	
}
