﻿using UnityEngine;
using System.Collections;

public class BorderHandler : GameBase {

	private const bool BORDER_ENTRY_APPROVED = true;
	private const bool BORDER_ENTRY_DENIED = false;


	//COUNTRY PROVINCE EVENTS
	public bool OnCountryBorderEnter(Border border, Agent agent){
		if(agent == null){
			//Debug.Log (string.Format ("Agent {0} entered {1} border!", agent, border));
			return BORDER_ENTRY_DENIED;
		}

		if (Random.Range(0, 2) == 1){
			return BORDER_ENTRY_APPROVED;
		}

		//Debug.Log (string.Format ("Agent {0} entered {1} border!", agent, border.GetCountry()));
		return BORDER_ENTRY_DENIED;
	}
	
	public void OnCountryBorderStay(Border border, Agent agent){
		if(agent == null){
			Debug.Log (string.Format ("Agent {0} stays on {1} border!", null, border.GetCountry()));
			return;
		}

		Debug.Log (string.Format ("Agent {0} stays on {1} border!", agent, border.GetCountry()));
	}
	
	public void OnCountryBorderExit(Border border, Agent agent){
		if(agent == null){
			Debug.Log (string.Format ("Agent {0} exited {1} border!", null, border.GetCountry()));
			return;
		}

		Debug.Log (string.Format ("Agent {0} exited {1} border!", agent, border.GetCountry()));
	}

	
}
