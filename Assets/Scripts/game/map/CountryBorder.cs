﻿using UnityEngine;
using System.Collections;

public class CountryBorder : GameBase {
	public Country country {get;set;}
	public EntityMesh mesh {get;set;}

    void Awake(){
		country = GetComponentInParent<Country>();
		mesh = GetComponentInChildren<EntityMesh>();
    }

	public MeshRenderer GetMeshRenderer(){
		return gameObject.GetComponentInChildren<MeshRenderer>();
	}


    public void OnCountryBorderEnter(Province province, Agent agent) {
        Notifier.Instance.notify(EventFactory.Build(country.descriptor.name, "ENTERED"));
        return;
    }

    public void OnCountryBorderStay(Agent agent) {
        if (agent == null) {
            Debug.Log(string.Format("Agent {0} stays on {1} border!", null, country));
            return;
        }

        Debug.Log(string.Format("Agent {0} stays on {1} border!", agent, country));
    }

    public void OnCountryBorderExit(Agent agent) {
        if (agent == null) {
            Debug.Log(string.Format("Agent {0} exited {1} border!", null, country));
            return;
        }

        Debug.Log(agent.location.country.descriptor.countryName + ".OnCountryBorderExit()");

        Debug.Log(string.Format("Agent {0} exited {1} border!", agent, country));


        Notifier.Instance.notify(EventFactory.Build(country.descriptor.name, "EXITED"));
    }


    //TODO maybe should be called internally
    public static bool isInboundMovement(Agent agent, Country country) {
        Vector3 inboundVector = (country.transform.position - agent.transform.position).normalized;
        Vector3 draggingDirectionVector = agent.draggingContext.getDraggingDirection();

        float dotProduct = Vector3.Dot(draggingDirectionVector, inboundVector);
        //print("dotProduct: " + dotProduct);
        return dotProduct >= 0;
    }
}
