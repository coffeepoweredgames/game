﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MeshSelectorImpl : MonoBehaviour {
    private Color originalColor;
    bool selected = false;


    public void select() {
        originalColor = gameObject.GetComponent<Renderer>().material.color;
        gameObject.GetComponent<Renderer>().material.color = MapConstants.COLOR_TILE_SELECTED;
    }

    public void deselect() {
        if (originalColor != null) {
            gameObject.GetComponent<Renderer>().material.color = originalColor;
            return;
        }
        gameObject.GetComponent<Renderer>().material.color = MapConstants.COLOR_TILE_MISSING_COLOR;
    }

    void OnMouseDown() {
        if (selected) {
            deselect();
            selected = false;
        }
        else {
            select();
            selected = true;
        }
    }
}