using UnityEngine;
using System.Collections;

[RequireComponent (typeof(TileMapImpl))]
public class TileMapMouse : MonoBehaviour {
    private TileMapImpl tileMap;
    private Vector3 currentTileCoord;
    public Transform selectionCube;

	void Start () {
        tileMap = GetComponent<TileMapImpl>();
	}
	
	void Update () {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity))
        {
            Vector3 mapRelativeHitPoint = tileMap.GetMapRelativeCoordinates(hitInfo.point);
            int x = Mathf.FloorToInt(mapRelativeHitPoint.x) / tileMap.GetTileSize();
            int y = Mathf.FloorToInt(mapRelativeHitPoint.y) / tileMap.GetTileSize();
            int z = Mathf.FloorToInt(-5);//TODO dodaj TileSelector.floatHight

            currentTileCoord.x = x;
            currentTileCoord.y = y;
            currentTileCoord.z = z;

            //update selection
            selectionCube.transform.position = tileMap.GetMapOrigin() + mapRelativeHitPoint;

            //Vector3 mapRelativeHitPoint = hitInfo.transform.worldToLocalMatrix.MultiplyPoint(hitInfo.point);
            Debug.Log("hitinfo(worldspace): " + hitInfo.point + " mapRelativeHitPoint:" + mapRelativeHitPoint + " selectioncube = " + selectionCube.transform.position);
        }


        Debug.Log(currentTileCoord);
	}
}
