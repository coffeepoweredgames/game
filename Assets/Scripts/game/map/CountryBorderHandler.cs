﻿using UnityEngine;
using System.Collections;

public class CountryBorderHandler : GameBase {
	private const bool BORDER_ENTRY_APPROVED = true;
	private const bool BORDER_ENTRY_DENIED = false;

	private Country country;

	void Start(){
		country = GetComponentInParent<Country>();
	}


	//COUNTRY PROVINCE EVENTS


	public void OnCountryBorderEnter(Province province, Agent agent){
		Debug.Log (province.country.descriptor.countryName+".OnCountryBorderEnter()");
		return;
	}
	
	public void OnCountryBorderStay(Agent agent){
		if(agent == null){
			Debug.Log (string.Format ("Agent {0} stays on {1} border!", null, country));
			return;
		}

		Debug.Log (string.Format ("Agent {0} stays on {1} border!", agent, country));
	}
	
	public void OnCountryBorderExit(Agent agent){
		if(agent == null){
			Debug.Log (string.Format ("Agent {0} exited {1} border!", null, country));
			return;
		}
		Debug.Log (agent.location.country.descriptor.countryName+".OnCountryBorderExit()");
		Debug.Log (string.Format ("Agent {0} exited {1} border!", agent, country));
	}

	
}
