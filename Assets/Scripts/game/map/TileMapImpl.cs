﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(MapFactory))]
public class TileMapImpl : GameBase, TileMap {
    public Mesh mesh;
    public Texture2D texture;
    public int tileCountHorizontal;
    public int tileCountVertical;
    public int tileSize;
    private Vector3 mapNormal;

    private Color originalColor;
    private Country selectedCountry;

    void Awake() {
        tileCountHorizontal = MapConstants.TILE_COUNT_HORIZONTAL;
        tileCountVertical = MapConstants.TILE_COUNT_VERTICAL;
        tileSize = MapConstants.TILE_SIZE_IN_PX;

        mesh = GetComponent<MapFactory>().BuildMapMesh(2079, 1269, tileCountHorizontal, tileCountVertical);//TODO remove hardcoded dimension

        SetMapNormal(MapConstants.MAP_NORMAL);

        MeshFilter meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        MeshCollider meshCollider = GetComponent<MeshCollider>();
        meshCollider.sharedMesh = mesh;

        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();

        transform.position = MapConstants.MAP_ORIGIN;
    }

    public Vector3 GetMapOrigin() {//lower left
        return transform.position;
    }

    /*Map middle.*/
    public Vector3 GetMapCenter()//TODO 2 umj 3
    {
        return new Vector3(Mathf.FloorToInt(GetMapWidth() / 3), Mathf.FloorToInt(GetMapHeight() / 3), 0);//TODO ne bi trebao raditi floor jer je parno sve
    }

    public Vector3 GetUpperRightCoord() {
        return new Vector3(GetMapOrigin().x + GetMapWidth(), 0, GetMapOrigin().z + GetMapHeight());
    }

    /*Tile size is symetric, width and height are equal.*/
    public int GetTileSize() {
        return tileSize;
    }

    /*Get map width in pixels*/
    public int GetMapWidth() {
        return tileCountHorizontal * GetTileSize();
    }

    /*Get map height in pixels*/
    public int GetMapHeight() {
        return tileCountVertical * GetTileSize();
    }

    /*World coordinates are converted to mouse relative coordinates. Collision results are in worldspace. Render also.*/
    public Vector3 GetMapRelativeCoordinates(Vector3 worldCoordinates) {
        return worldCoordinates - GetMapOrigin();
    }

    /*TileMap*/
    public Vector3 GetWorldSpaceCoordinates(Vector3 tileCoordinate) {
        Vector3 tileMapPixelCoordinate = new Vector3(tileCoordinate.x * MapConstants.TILE_SIZE_IN_PX, tileCoordinate.y * MapConstants.TILE_SIZE_IN_PX, tileCoordinate.z);
        return GetMapOrigin() + tileMapPixelCoordinate;
    }

    /**
     * Used for converting map middle coordinates in pixels etc. to world space
     * */
    public Vector3 GetWorldSpaceCoordinatesFromPixels(Vector3 tileMapPixelCoord) {
        return GetMapOrigin() + tileMapPixelCoord;
    }

    /*Underlying map mesh*/
    public Mesh GetMapMesh() {
        return mesh;
    }

    /**
     * Return up direction of map.
     * */
    public Vector3 GetMapNormal() {
        return mapNormal;
    }

    private void SetMapNormal(Vector3 normal) {

        this.mapNormal = normal;
    }

    public GameObject GetCountry(Transform incomingTransform) {
        if (transform == null) {
            Debug.Log("TileMap.GetCounty invalid argument: " + incomingTransform);
            return null;
        }

        GameObject area;
        GameObject country;
        Transform countryTransform;

        Debug.Log("TileMap.GetCounty incoming transform with name: " + incomingTransform.name);

        if (incomingTransform.gameObject.name == "EntityMesh") {//child of area
            area = incomingTransform.parent.gameObject;
            Debug.Log("ico: " + area.name);
            country = area.transform.parent.gameObject;
            logCountrySearch(country.transform, "EntityMesh");
            return country;
        }

        if (incomingTransform.gameObject.name == "area") {//child of country
            country = incomingTransform.parent.gameObject;
            logCountrySearch(country.transform, "area");
            return country;
        }

        Transform tileMap = gameObject.transform;
        Transform allCountries = tileMap.FindChild("Countries");//TODO hardcoded, looks only immediate children

        //country transform
        countryTransform = allCountries.Find(incomingTransform.name.ToUpper());//TODO suzi listu na samo country transforms, potencijalan false positive
        logCountrySearch(countryTransform, "countryTransform");
        country = countryTransform != null ? countryTransform.gameObject : null;
        return country;
    }

    public Country SelectCountry(Country country) {//TODO update game state
        if (country == null) {
            Debug.LogError("Cannot select null country!");
            return null;
        }

        colorCountry(country, ColorConstants.SELECTED_COLOR);
        selectedCountry = country;

        return selectedCountry;
    }

    public void DeSelectCountry(Country country) {
        if (country == null) {
            Debug.LogError("Cannot deselect null country!");
            return;
        }

        colorCountry(country, originalColor);
        selectedCountry = null;
    }

    private void colorCountry(Country country, Color color) {
        GameObject entityMeshObject = country.gameObject.GetComponentInChildren<EntityMesh>().gameObject;
        originalColor = entityMeshObject.GetComponent<Renderer>().material.color;
        entityMeshObject.GetComponent<Renderer>().material.color = color;
    }

    private void logCountrySearch(Transform transform, string method) {
        if (transform != null) {
            Debug.Log("Found country " + transform.gameObject.name + " via " + method + ".");
            return;
        }

        Debug.Log("Failed to find country " + " via " + method + ".");
    }
}
