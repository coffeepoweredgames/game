﻿using UnityEngine;
using System.Collections;

public interface MeshSelector
{
    void select();
    void deselect();
}
