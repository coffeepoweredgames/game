﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapFactory : GameBase{
	
	public Mesh BuildMapMesh(int mapWidth, int mapHeight, int tileCountHorizontal, int tileCountVertical)
	{
		int no_vertex_x = tileCountHorizontal + 1;
		int no_vertex_y = tileCountVertical + 1;
		int totalVertices = no_vertex_x * no_vertex_x;
		int no_triangles = tileCountHorizontal * tileCountVertical * 2;//za svaki tile trebas 2 trokuta

		int tileSizeX = (int)mapWidth / no_vertex_x;
		int tileSizeY = (int)mapHeight / no_vertex_y;
		
		//Mesh data
		Vector3[] vertices = new Vector3[totalVertices];
		Vector3[] normals = new Vector3[totalVertices];
		Vector2[] uv = new Vector2[totalVertices];
		int[] triangles = new int[no_triangles * 3];//1 triangle is an int triple
		float uv_offset_x = (float)1 / tileCountHorizontal; // uv always normalized    tileCountHorizontal * (1 / tileCountHorizontal) = 1
		float uv_offset_y = (float)1 / tileCountVertical;
		
		//Generate vertices (prirodan smjer, odozdo prema gore, redak po redak), uv koordinate isto lower left
		for (int y = 0; y < no_vertex_y; y++)
		{
			for (int x = 0; x < no_vertex_x; x++)
			{
				vertices[y * no_vertex_x + x] = new Vector3(x * tileSizeX, y * tileSizeY, 0);
				normals[y * no_vertex_x + x] = transform.forward;//sa ovime exact tex color, -forward bude bijelo??
				uv[y * no_vertex_x + x] = new Vector2(x * uv_offset_x, y * uv_offset_y);
			}
		}
		
		//SINGLE TILE aka 2 TRIANGLES
		// _______
		//| \ 2nd |
		//|  \    |
		//|   \   |
		//|    \  |
		//| 1st \ |
		//|______\|
		
		//Generate triangles(from vertices), LEFT FRAME, use left hand rule (kaziprst x, srednji je y, palac je x cross y)
		for (int y = 0; y < tileCountVertical; y++)
		{
			for (int x = 0; x < tileCountHorizontal; x++)
			{
				int squareIndex = y * tileCountHorizontal + x;
				int triOffset = squareIndex * 6;  //bucket size 6
				//1st triangle
				triangles[triOffset + 0] = (y + 1) * no_vertex_x + x;//upper left
				triangles[triOffset + 1] = y * no_vertex_x + x + 1;//lower right
				triangles[triOffset + 2] = y * no_vertex_x + x;//lower left
				
				//2nd triangle
				triangles[triOffset + 3] = (y + 1) * no_vertex_x + x;//upper left
				triangles[triOffset + 4] = (y + 1) * no_vertex_x + x + 1;//upper right
				triangles[triOffset + 5] = y * no_vertex_x + x + 1;//lower right
				
				//[triOffset + 0] //upper left
				//[triOffset + 1] //lower right
				//[triOffset + 2] ;//lower left
				
				////2nd triangle
				//[triOffset + 3] //upper left
				//[triOffset + 4] //upper right
				//[triOffset + 5] //lower right
				
				//1
				//0
				//2
				//5
				//4
				//3
			}
		}
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.normals = normals;
		mesh.uv = uv;
		
		//Debug.Log("Map mesh generated!");
		return mesh;
	}

    public Texture2D BuildMapTexture(int tileCountHorizontal, int tileCountVertical, int tileSize)//TODO generiran skriptu ali gdje ovo settiram, dali je visak?
    {
        Texture2D texture = null;
        int texWidth = tileCountHorizontal * tileSize; //100 horizontal tiles * 16px = 1600 px
        int texHeight = tileCountVertical * tileSize; //50 vertical tiles * 16px = 800 px
        texture = new Texture2D(texWidth, texHeight);

        texture.filterMode = FilterMode.Point;
        texture.Apply();// Apply all previous SetPixel and SetPixels changes.
        texture.wrapMode = TextureWrapMode.Clamp;//way to handle uv coords >1

        //MeshRenderer mesh_renderer = GetComponent<MeshRenderer>();
        //mesh_renderer.sharedMaterials[0].mainTexture = texture; 
        Debug.Log("Map texture built!");
        return texture;
    }
}