﻿using UnityEngine;

//Vector3[] normals = new Vector3[totalVertices];
//Vector2[] uv = new Vector2[totalVertices];
//int[] triangles = new int[no_triangles * 3];//1 triangle is an int triple
public class MeshFactory : MonoBehaviour {

    public static Mesh getMesh() {//deprecated
        //Mesh data
        Vector3[] vertices = new Vector3[10];

        vertices[0] = new Vector3(27 * MapConstants.TILE_SIZE_IN_PX, 4 * MapConstants.TILE_SIZE_IN_PX, -5);

        vertices[1] = new Vector3(1 * MapConstants.TILE_SIZE_IN_PX, 5 * MapConstants.TILE_SIZE_IN_PX, -5);

        vertices[2] = new Vector3(3 * MapConstants.TILE_SIZE_IN_PX, 7 * MapConstants.TILE_SIZE_IN_PX, -5);

        vertices[3] = new Vector3(6 * MapConstants.TILE_SIZE_IN_PX, 11 * MapConstants.TILE_SIZE_IN_PX, -5);

        vertices[4] = new Vector3(9 * MapConstants.TILE_SIZE_IN_PX, 8 * MapConstants.TILE_SIZE_IN_PX, -5);

        vertices[5] = new Vector3(12 * MapConstants.TILE_SIZE_IN_PX, 2 * MapConstants.TILE_SIZE_IN_PX, -5);

        vertices[6] = new Vector3(15 * MapConstants.TILE_SIZE_IN_PX, 4 * MapConstants.TILE_SIZE_IN_PX, -5);

        vertices[7] = new Vector3(18 * MapConstants.TILE_SIZE_IN_PX, 15 * MapConstants.TILE_SIZE_IN_PX, -5);

        vertices[8] = new Vector3(21 * MapConstants.TILE_SIZE_IN_PX, 20 * MapConstants.TILE_SIZE_IN_PX, -5);

        vertices[9] = new Vector3(24 * MapConstants.TILE_SIZE_IN_PX, 15 * MapConstants.TILE_SIZE_IN_PX, -5);

        Mesh mesh = new Mesh();
        mesh.vertices = vertices;

        return mesh;
    }
}