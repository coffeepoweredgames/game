﻿using UnityEngine;
using System.Collections;

public class Border : GameBase {

	public EntityMesh GetMesh(){
		return gameObject.GetComponentInChildren<EntityMesh>();
	}

	public Country GetCountry(){
		return GetComponentInParent<Country>();//will search all parents for component (upward search)
	}

	public MeshRenderer GetMeshRenderer(){
		return gameObject.GetComponentInChildren<MeshRenderer>();
	}
}
