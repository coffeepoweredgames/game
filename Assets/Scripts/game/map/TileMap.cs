﻿using UnityEngine;
using System.Collections;

public interface TileMap {
    GameObject GetCountry(Transform transform);//TODO return Country component
    Country SelectCountry(Country country);
    void DeSelectCountry(Country country);	

    Vector3 GetMapOrigin();
    Vector3 GetUpperRightCoord();
    int GetTileSize();
    int GetMapWidth();
    int GetMapHeight();
    Vector3 GetMapCenter();
    Vector3 GetMapNormal();
    Vector3 GetMapRelativeCoordinates(Vector3 screenCoordinates);
    Vector3 GetWorldSpaceCoordinates(Vector3 tileCoordinate);
    Vector3 GetWorldSpaceCoordinatesFromPixels(Vector3 tileMapPixelCoord);
    Mesh GetMapMesh();
}
