﻿using UnityEngine;
using System.Collections;

public class Area : GameBase {

	public EntityMesh GetMesh(){
		return gameObject.GetComponentInChildren<EntityMesh>();
	}

	public Country GetCountry(){//TODO dodati ovu metodu u neki LocationAware interface??
		return GetComponentInParent<Country>();//will search all parents for component (upward search)
	}

	public Border GetBorder(){
		return GetComponentInSibling<Border>();
	}
}
