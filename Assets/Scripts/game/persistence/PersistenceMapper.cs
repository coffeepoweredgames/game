﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PersistenceMapper {

    public static LocationDescriptorData fromComponent(LocationDescriptor descriptor) {
        if (descriptor == null) {
            return null;
        }

        LocationDescriptorData descriptorData = new LocationDescriptorData();
        descriptorData.countryDescriptorData = fromComponent(descriptor.country.descriptor);
        descriptorData.provinceDescriptorData = fromComponent(descriptor.province.descriptor);
        //descriptorData.cityDescriptorData = fromComponent(descriptor.city.descriptor);
        return descriptorData;
    }

    public static void toComponent(LocationDescriptorData locationData, LocationDescriptor location) {

        //TODO for all this, use factories
        if (locationData.countryDescriptorData != null) {
            location.country = DynamicObjectRegistry.COUNTRIES[locationData.countryDescriptorData.countryName];//TODO use getter
        }

        if (locationData.provinceDescriptorData != null) {
            location.province = DynamicObjectRegistry.PROVINCES[locationData.provinceDescriptorData.provinceName];
        }

        if (locationData.cityDescriptorData != null) {
            //location.country = DynamicObjectRegistry.CITIES[locationData.cityDescriptorData.name];
        }
    }

    public static AgentDescriptorData fromComponent(AgentDescriptor descriptor) {
        if (descriptor == null) {
            return null;
        }

        AgentDescriptorData descriptorData = new AgentDescriptorData();
        descriptorData.id = descriptor.id;
        descriptorData.agentName = descriptor.agentName;
        descriptorData.portraitId = descriptor.portraitId;
        return descriptorData;
    }

    public static void toComponent(AgentDescriptorData descriptorData, AgentDescriptor descriptor) {
        descriptor.id = descriptorData.id;
        descriptor.agentName = descriptorData.agentName;
        descriptor.portraitId = descriptorData.portraitId;
    }

    public static CountryDescriptorData fromComponent(CountryDescriptor descriptor) {
        if (descriptor == null) {
            return null;
        }

        CountryDescriptorData descriptorData = new CountryDescriptorData();
        descriptorData.id = descriptor.id;
        descriptorData.countryName = descriptor.countryName;
        return descriptorData;
    }

    public static void toComponent(CountryDescriptorData descriptorData, CountryDescriptor descriptor) {
        descriptor.id = descriptorData.id;
        descriptor.countryName = descriptorData.countryName;
    }

    public static ProvinceDescriptorData fromComponent(ProvinceDescriptor descriptor) {
        if (descriptor == null) {
            return null;
        }

        ProvinceDescriptorData descriptorData = new ProvinceDescriptorData();
        descriptorData.id = descriptor.id;
        descriptorData.provinceName = descriptor.provinceName;
        return descriptorData;
    }

    public static void toComponent(ProvinceDescriptorData descriptorData, ProvinceDescriptor descriptor) {
        descriptor.id = descriptorData.id;
        descriptor.provinceName = descriptorData.provinceName;
    }

    public static CityDescriptorData fromComponent(CityDescriptor descriptor) {
        if (descriptor == null) {
            return null;
        }

        CityDescriptorData descriptorData = new CityDescriptorData();
        descriptorData.id = descriptor.id;
        descriptorData.cityName = descriptor.cityName;
        return descriptorData;
    }

    public static void toComponent(CityDescriptorData descriptorData, CityDescriptor descriptor) {
        descriptor.id = descriptorData.id;
        descriptor.cityName = descriptorData.cityName;
    }

    public static MissionDescriptorData fromComponent(MissionDescriptor descriptor) {
        if (descriptor == null) {
            return null;
        }

        MissionDescriptorData descriptorData = new MissionDescriptorData();
        descriptorData.name = descriptor.name;
        descriptorData.type = descriptor.type;
        return descriptorData;
    }

    public static void toComponent(MissionDescriptorData descriptorData, MissionDescriptor descriptor) {
        descriptor.name = descriptorData.name;
        descriptor.type = descriptorData.type;
    }

    //public static MissionGoalData fromComponent(TimedMissionGoal missionGoal) {
    //    if (missionGoal == null) {
    //        return null;
    //    }

    //    MissionGoalData missionGoalData = new MissionGoalData();
    //    missionGoalData.name = missionGoal.goalName;
    //    return missionGoalData;
    //}

    //public static List<MissionGoalData> fromComponent(List<TimedMissionGoal> missionGoalList) {
    //    if (missionGoalList == null || missionGoalList.Count == 0) {
    //        return new List<MissionGoalData>();
    //    }

    //    List<MissionGoalData> goalDataList = new List<MissionGoalData>();
    //    foreach (TimedMissionGoal goal in missionGoalList) {
    //        goalDataList.Add(fromComponent(goal));
    //    }

    //    return goalDataList;
    //}

    //public static void toComponent(MissionGoalData missionGoalData, TimedMissionGoal missionGoal) {
    //    missionGoal.goalName = missionGoalData.name;
    //}


    // AGENT_DATA <-> AGENT
    public static AgentData fromComponent(Agent agent) {
        if (agent == null) {
            return null;
        }

        AgentData agentData = new AgentData();
        agentData.descriptorData = fromComponent(agent.descriptor);
        agentData.locationData = fromComponent(agent.location);
        return agentData;
    }

    public static void toComponent(AgentData agentData, Agent agent) {
        toComponent(agentData.descriptorData, agent.descriptor);
        toComponent(agentData.locationData, agent.location);
    }

    // COUNTRY_DATA <-> COUNTRY
    public static CountryData fromComponent(Country country) {
        if (country == null) {
            return null;
        }

        CountryData countryData = new CountryData();
        countryData.descriptorData = fromComponent(country.descriptor);
        return countryData;
    }

    public static void toComponent(CountryData countryData, Country country) {
        toComponent(countryData.descriptorData, country.descriptor);
    }

    // PROVINCE_DATA <-> PROVINCE
    public static ProvinceData fromComponent(Province province) {
        if (province == null) {
            return null;
        }

        ProvinceData provinceData = new ProvinceData();
        provinceData.descriptorData = fromComponent(province.descriptor);
        return provinceData;
    }

    public static void toComponent(ProvinceData provinceData, Province province) {
        toComponent(provinceData.descriptorData, province.descriptor);
    }

    // CITY_DATA <-> CITY
    public static CityData fromComponent(City city) {
        if (city == null) {
            return null;
        }

        CityData cityData = new CityData();
        cityData.descriptorData = fromComponent(city.descriptor);
        return cityData;
    }

    public static void toComponent(CityData cityData, City city) {
        toComponent(cityData.descriptorData, city.descriptor);
    }

    //// MISSION_DATA <-> MISSION
    //public static MissionData fromComponent(Mission mission) {
    //    if (mission == null) {
    //        return null;
    //    }

    //    MissionData missionData = new MissionData();
    //    missionData.descriptorData = fromComponent(mission.descriptor);
    //    missionData.goals = fromComponent(mission.goals);
    //    return missionData;
    //}

    //public static void toComponent(MissionData missionData, Mission mission) {

    //    //descriptor data
    //    toComponent(missionData.descriptorData, mission.descriptor);

    //    //goal list
    //    List<MissionGoal> goals = new List<MissionGoal>();
    //    foreach (MissionGoalData gd in missionData.goals) {
    //        MissionGoal g = new MissionGoal();
    //        g.goalName = gd.name;
    //        goals.Add(g);
    //    }

    //    mission.goals = goals;
    //}



}
