﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Represents all persistent state of an Agent component.
/// </summary>
[System.Serializable]
public class AgentData {

    [SerializeField]
    public AgentDescriptorData descriptorData { get; set; }

    [SerializeField]
    public LocationDescriptorData locationData { get; set; }
    

}
