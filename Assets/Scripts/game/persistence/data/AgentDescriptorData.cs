﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AgentDescriptorData {

    [SerializeField]
    public int id { get; set; }

    [SerializeField]
    public string agentName { get; set; }

    [SerializeField]
    public int portraitId { get; set; }

    public bool isValid() {
        if (agentName == null || agentName.Equals("")) {
            return false;
        }
        return true;
    }
}
