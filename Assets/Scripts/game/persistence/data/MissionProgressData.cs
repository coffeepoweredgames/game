﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class MissionProgressData {
    [SerializeField]
    public AgentData agentData { get; set; }
    [SerializeField]
    public MissionData missionData { get; set; }
    [SerializeField]
    public List<MissionGoalData> missionGoals;
    [SerializeField]
    public List<MissionGoalData> completedGoals;
    [SerializeField]
    public MissionGoalData currentGoal;
    [SerializeField]
    public bool isProgressCompleted { get; set; }
}
