﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Represents all persistent state of an Country component.
/// </summary>
[System.Serializable]
public class CountryData {

    [SerializeField]
    public CountryDescriptorData descriptorData;


}
