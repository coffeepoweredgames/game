﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LocationDescriptorData  {
    [SerializeField]
    public CountryDescriptorData countryDescriptorData { get; set; }

    [SerializeField]
    public ProvinceDescriptorData provinceDescriptorData { get; set; }

    [SerializeField]
    public CityDescriptorData cityDescriptorData { get; set; }
}
