﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Represents all persistent state of an Province component.
/// </summary>
[System.Serializable]
public class ProvinceData {

    [SerializeField]
    public ProvinceDescriptorData descriptorData { get; set; }


}
