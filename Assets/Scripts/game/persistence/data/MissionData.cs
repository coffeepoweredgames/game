﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class MissionData {

    [SerializeField]
    public MissionDescriptorData descriptorData;

    [SerializeField]
    public List<MissionGoalData> goals { get; set; }
}
