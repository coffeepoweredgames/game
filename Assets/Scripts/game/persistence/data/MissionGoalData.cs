﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MissionGoalData {
    [SerializeField]
    public string name { get; set; }
}
