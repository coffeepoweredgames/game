﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MissionDescriptorData {

    [SerializeField]
    public string name { get; set; }

    [SerializeField]
    public MissionType type { get; set; }

}
