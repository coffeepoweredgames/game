﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CountryDescriptorData {

    [SerializeField]
    public int id { get; set; }

    [SerializeField]
    public string countryName { get; set; }

    public bool isValid() {
        if (countryName == null || countryName.Equals("")) {
            return false;
        }
        return true;
    }

}
