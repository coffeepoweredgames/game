﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ProvinceDescriptorData {

    [SerializeField]
    public int id { get; set; }

    [SerializeField]
    public string provinceName { get; set; }

    public bool isValid() {
        if (provinceName == null || provinceName.Equals("")) {
            return false;
        }
        return true;
    }

}
