﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Represents all persistent state of an City component.
/// </summary>
[System.Serializable]
public class CityData {

    [SerializeField]
    public CityDescriptorData descriptorData { get; set; }

}
