﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CityDescriptorData {

    [SerializeField]
    public int id { get; set; }

    [SerializeField]
    public string cityName { get; set; }

    public bool isValid() {
        if (cityName == null || cityName.Equals("")) {
            return false;
        }
        return true;
    }
}
