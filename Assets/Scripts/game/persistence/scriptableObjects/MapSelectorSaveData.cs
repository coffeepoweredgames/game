﻿using UnityEngine;
using System.Collections;

public class MapSelectorSaveData : ScriptableObject {

    public CountryDescriptorData countryDescriptor { get; set; }
    public ProvinceDescriptorData provinceDescriptor { get; set; }
    public AgentDescriptorData agentDescriptor { get; set; }
    public CityDescriptorData cityDescriptor { get; set; }
}
