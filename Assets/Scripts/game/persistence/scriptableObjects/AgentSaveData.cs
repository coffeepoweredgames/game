﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AgentSaveData : ScriptableObject {

    public List<AgentData> savedAgentList;

    public AgentDescriptorData selectedAgentDescriptor;

}


