using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MissionListData : ScriptableObject  {//TODO mssion list data
	[SerializeField]
	public List<Mission> missions {get;set;}

	[SerializeField]
	public Mission[] temp {get;set;}
	
	void OnEnable() {
		if(missions == null){
			missions = new List<Mission>();
		}
	}
	
	public void Add(Mission m){
		missions.Add(m);
		temp = missions.ToArray();
	}

	public void printMissions(){
		if(missions == null){
			Debug.Log("Cannot print missions because scriptableObject.missions is null!!\n");
		}
		
		//LIST MISSIONS
		if(missions !=null && missions.Count > 0){
			foreach(Mission m in missions){
				if(m == null){
					Debug.Log("Mission item is null in missionList!!\n");
					continue;
				}
				
				if(m.descriptor == null){
					Debug.Log("Descriptor is nul!!\n");
					continue;
				}
				
				Debug.Log ("mission: "+ m.descriptor.name+" \n");
			}
		}
	}

}

