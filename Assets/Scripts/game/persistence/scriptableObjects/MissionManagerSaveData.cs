﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class MissionManagerSaveData : ScriptableObject {

    public Dictionary<AgentDescriptorData, MissionProgressData> agentMissionMap { get; set; }

}

