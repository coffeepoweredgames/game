using UnityEngine;
using System.Collections;

public class CityFactory : GameBase{

	private static Object cityPrefab = Resources.Load("prefabs/map/City");

	public GameObject Build(int id, string name){
		GameObject instance = Instantiate(cityPrefab) as GameObject;
		instance.name = name;

		CityDescriptor descriptor = instance.GetComponent<CityDescriptor>();
		descriptor.id = id;
		descriptor.cityName = name;

		return instance;
	}
}
