using UnityEngine;

public class Cities {
	public static readonly Descriptor LONDON = new Descriptor (1, "LONDON");
	public static readonly Descriptor PARIS = new Descriptor (2, "PARIS");
}
