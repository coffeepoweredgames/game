﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CityDescriptor))]
[RequireComponent(typeof(CityState))]
public class City : GameBase {

    public CityDescriptor descriptor { get; set; }

    void Awake() {
        descriptor = gameObject.GetComponent<CityDescriptor>();
    }


}
