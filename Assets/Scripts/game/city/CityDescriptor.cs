﻿using UnityEngine;
using System.Collections;

public class CityDescriptor : GameBase{

	public int id {get;  set;}
	
	public string cityName {get;  set;}

    public bool isValid() {
        if (cityName == null || cityName.Equals("")) {
            return false;
        }
        return true;
    }

}
