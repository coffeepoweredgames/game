﻿using UnityEngine;
using System.Collections;

public class LightFade : MonoBehaviour {

    private Light mylight;

    public void OnEnable() {
        mylight = GetComponent<Light>();
    }
	
	void Update () {
        mylight.range = Mathf.Lerp(mylight.range, 0, Time.deltaTime);
	}
}
