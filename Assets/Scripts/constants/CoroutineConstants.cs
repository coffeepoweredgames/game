﻿using UnityEngine;

public class CoroutineConstants  {
    public static string STORE_LAST_POSITION ="StoreLastPosition";
    public static string REFRESH_AGENTS_SUMMARY_TABLE = "RefreshAgentsSummaryTable";

}
