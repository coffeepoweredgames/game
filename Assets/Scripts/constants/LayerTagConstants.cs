using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Layer constants.
/// Keep this values in sync with those used in editor.
/// </summary>
public class LayerTagConstants{
	public static string MAP_LAYER = "mapLayer";
    public static string UI_LAYER = "UI";
    public static string STRUCTURE_LAYER = "structureLayer";//why cannot structures be on map layer?

	public static string PROVINCE_TAG = "provinceTag";
	public static string AREA_TAG = "areaTag";
	public static string BORDER_TAG = "borderTag";
	public static string STATE_TAG = "stateTag";
	public static string AGENT_TAG = "agentTag";
	public static string TILE_MAP_TAG = "tileMapTag";//background mesh tag, to differentiate via tag not name, mesh located on MAP_LAYER	
}	
