﻿using System;
using UnityEngine;

public class StructureConstants {
    public static readonly Vector3 IDENTIY_ROTATION = new Vector3(0, 0, 0);
    public static readonly Vector3 IDENTITY_SCALE = new Vector3(1, 1, 1);

    public static readonly Vector3 SPY_SCHOOL_LOCAL_POSITION = new Vector3(2, 2, 0);
    public static readonly Vector3 SPY_SCHOOL_LOCAL_ROTATION = IDENTIY_ROTATION;
    public static readonly Vector3 SPY_SCHOOL_LOCAL_SCALE = IDENTITY_SCALE;

    public static readonly String SPY_SCHOOL = "SpySchool";


    public const int HEALTH_NORMAL = 100;
    public const int HEALTH_STABLE = 75;
    public const int HEALTH_CRITICAL = 25;

    public const int INTEL_OWNED_BY_STRUCTURES = 100;

}

