﻿using UnityEngine;
using System;


public class SoundConstants {
    public const string COIN_SOUND = "sounds/smb_coin";
    public const string COFFEE_POUR_SOUND = "sounds/joined";
}
