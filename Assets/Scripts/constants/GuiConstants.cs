using UnityEngine;
using System.Collections;

public class GuiConstants {
	
	public static string GUI = "GUI";
    public static string INGAME_MENU_NAME = "InGameMenu";
	public static string INGAME_HUD_NAME = "InGameHud";
	public static string COUNTRY_BORDER_HUD_NAME = "CountryBorderHud";
	public static string AGENT_STATUS_HUD_NAME = "AgentStatusHud";
    public static string SPY_SCHOOL_HUD_NAME = "SpySchoolHud";
    public static string STRUCTURE_MISSION_PICKER_HUD_NAME = "StructureMisssionPickerHud";
	public static string GUI_HUD_LOGIC_PROVIDER_NAME = "HudLogicProvider";
    public static string STRUCTURE_STATUS_HUD_NAME = "StructureStatusHud2";


    public static string MAIN_MENU_SCREEN_GUI = "MainMenuScreenMenu";

    public static int EVENT_PANEL_SIZE = 10;

}