using UnityEngine;
using System.Collections;

public class AssetConstants{

	public static readonly float BLENDER_SCALE_LEVEL_FIX = 38.5f;

    public static readonly string SAVED_DATA_MISSIONS_LIST = "Assets/SaveData/MissionsSaveData.asset";

    public static readonly string SAVED_DATA_MISSION_MANAGER = "Assets/SaveData/MissionManagerSaveData.asset";

    public static readonly string SAVED_DATA_AGENTS = "Assets/SaveData/AgentsSaveData.asset";

    public static readonly string SAVED_DATA_MAP_SELECTOR = "Assets/SaveData/MapSelectorSaveData.asset";

}


