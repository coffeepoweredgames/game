﻿using UnityEngine;
using System.Collections;

public class AgentConstants {
    public const int HEALTH_NORMAL = 100;
    public const int HEALTH_STABLE = 75;
    public const int HEALTH_CRITICAL = 25;
}


