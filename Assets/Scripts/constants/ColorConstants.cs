using UnityEngine;
using System.Collections;

public class ColorConstants : MonoBehaviour {
	
	public static Color COUNTRY_COLOR = new Color(0.714f, 0.714f, 0.745f);
	public static Color BORDER_COLOR = Color.white;
	public static Color PROVINCE_COLOR = new Color(0.031f, 0.282f, 0.278f); //new Color(r/255.0F,g/255.0F,b/255.0F,a/255.0F);
	public static Color SELECTED_COLOR = Color.green;
	public static Color DE_SELECTED_COLOR = Color.white;//TODO ima li smisla osim za agente?

    //public static Color STRUCTURE_COLOR = Color.red;

    public static Color HEALTH_HEALTHY = Color.green;
    public static Color HEALTH_STABLE = Color.yellow;
    public static Color HEALTH_CRITICAL = Color.red;

    public static Color PINK_COLOR = new Color(255F, 105f, 180f); //PINK COLOR WHEN STUFF GOES WRONG
}

