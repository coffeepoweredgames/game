﻿using UnityEngine;
using System.Collections;

public class PrefabConstants : MonoBehaviour {

	//INGAME PREFABS
	public static string COUNTRY_PREFAB = "prefabs/map/CountryPrefab";
	public static string AGENT_PREFAB = "prefabs/actors/AgentPrefabWithMesh";
	public static string SPY_SCHOOL_PREFAB = "prefabs/map/SpySchoolStructurePrefab";

    //MOVEMENT PREFABS
    public static string DRAGGING_CONTEXT_PREFAB = "prefabs/drag/DraggingContextPrefab";

	//GUI PREFABS
	public static string AGENT_HUD_GUI_PREFAB = "prefabs/gui/AgentStatusHud5";
    public static string COUNTRY_HUD_GUI_PREFAB = "prefabs/gui/TestHud";
	public static string COUNTRY_BORDER_HUD_GUI_PREFAB = "prefabs/gui/CountryBorderHud";
    public static string INGAME_MENU_GUI_PREFAB = "prefabs/gui/InGameMenu";
    public static string INGAME_HUD_GUI_PREFAB = "prefabs/gui/InGameHud";
    public static string SPY_SCHOOL_GUI_PREFAB = "prefabs/gui/SpySchoolHud";
    public static string STRUCTURE_STATUS_HUD_GUI_PREFAB = "prefabs/gui/structure/StructureStatusHud2";
    public static string STRUCTURE_MISSION_PICKER_GUI_PREFAB = "prefabs/gui/StructureMisssionPickerHud";
	public static string HUD_LOGIC_PROVIDER_PREFAB = "prefabs/gui/HudLogicProviderPrefab";

    //MISSION PREFAB
    public static string MISSION_PREFAB = "prefabs/mission/MissionPrefab";
    public static string MISSION_PROGRESS_PREFAB = "prefabs/mission/MissionProgressPrefab";
    public static string SAMPLE_BUTTON_PREFAB = "prefabs/mission/SampleButton";

    //GAME EVENTS
    public static string GAME_EVENT_PREFAB = "prefabs/gui/event/GameEventPrefab";
    public static string GAME_EVENT_GOAL_PREFAB = "prefabs/gui/event/MissionGoalEventPrefab";
    public static string GAME_EVENT_MISSION_PREFAB = "prefabs/gui/event/MissionEventPrefab";

    //EVENT QUEUE
    public static string GAME_EVENT_QUEUE_PREFAB = "prefabs/gui/event/GameEventQueuePrefab";


    //STRUCTURES
    public static string FACTORY_STRUCTURE_PREFAB = "prefabs/structures/FactoryPrefab";

    //ROWS
    public static string ROW_AGENTS_SUMMARY_PREFAB = "prefabs/gui/statistics/RowAgentsSummary";

    //STATS
    public static string AGENT_STAT_PREFAB = "prefabs/statistics/AgentStatPrefab";

    public static string EXPLOSION = "prefabs/explosions/Explosion1Prefab";







}
