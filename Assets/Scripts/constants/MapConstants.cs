﻿using UnityEngine;
using System.Collections;

public enum CardSuit {
    Clubs,
    Hearts,
    Spades,
    Diamonds
}

/**
 * C# interfaces cannot store fields.
 * Tilemap constants are defined here.
 */
public class MapConstants {
    public const int TILE_MAP_WIDTH_PX = 2079;
    public const int TILE_MAP_HEIGHT_PX = 1269;
    public const int TILE_COUNT_HORIZONTAL = 50;
    public const int TILE_COUNT_VERTICAL = 50;
    public const int TILE_SIZE_IN_PX = 32;//not used for now
    public static Vector3 MAP_ORIGIN = new Vector3(500, 500, 0);
    public static Vector3 MAP_NORMAL = (-1) * Vector3.forward;
    public const int CAMERA_DISTANCE_TO_MAP = 200;
    public static Color COLOR_TILE_SELECTED = Color.red;
    public static Color COLOR_TILE_MISSING_COLOR = Color.blue;

    //TODO add custom zoom levels tipa WHOLE EUROPE

}
