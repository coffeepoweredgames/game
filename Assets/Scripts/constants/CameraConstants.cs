﻿using System;

public class CameraConstants {
    public static readonly float MOUSE_SENSITIVITY = 10f;
    public static readonly float CAMERA_MIN_FOV = 15f;
    public static readonly float CAMERA_STARTING_FOV = 55f;
    public static readonly float CAMERA_MAX_FOV = 90f;
    public static readonly int MAX_PITCH_ANGLE = 40;
    public static readonly int MIN_PITCH_ANGLE = -40;
    public static readonly float VERTICAL_ROTATION_SPEED = 0.4f;

}

