﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
public class Anim : MonoBehaviour {

    private SpriteRenderer SpriteRenderer { get; set; }
    private Animation Animation { get; set; }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void Awake()
    {
        SpriteRenderer = GetComponent<Renderer>() as SpriteRenderer;
        Animation = GetComponent<Animation>() as Animation;
    }

    public void Play(string animationName)
    {
        Animation.Play(animationName);
    }

    public void SetNextSprite(Sprite sprite)
    {
        SpriteRenderer.sprite = sprite;
    }
}
