﻿using UnityEngine;
using System.Collections;

public class ShowMenuF12 : MonoBehaviour {
    bool isMenuOpen = false;
	// Use this for initialization

    public GameObject prefab;
    GameObject menu;

	void Start () {

        Debug.Log("ShowMenuF12.start(...)");
	
	}
	
	// Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.F12))
        {
            if (isMenuOpen)
            {
                Destroy(menu);
                isMenuOpen = false;
                Debug.Log("Menu destroyed.");
                return;
            }

            menu = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
            isMenuOpen = true;
            Debug.Log("Menu created.");
        }//end F12


    }//update

}
