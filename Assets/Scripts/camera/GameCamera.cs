﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour {
    private bool isMoveInputAvailable = false;
    private bool isRotationInputAvailable = false;
    private bool isZoomInputAvailable = false;
    //private bool isScrollInputAvailable = false;
    private float scrollSpeed = 5f;
    private float vAxis = 0;
    private float hAxis = 0;
    private Vector3 moveDir = Vector3.zero;
    private float fov;
    float x = 0.0f;
    float y = 0.0f;

    void Awake() {
        fov = CameraConstants.CAMERA_STARTING_FOV;
        Camera.main.fieldOfView = fov;
    }

    void Update() {
        adjustMovement();
        adjustZoom();
        adjustRotation();
    }

    void LateUpdate() {
        if (isMoveInputAvailable) {
            transform.Translate(moveDir * scrollSpeed, Space.World);
            isMoveInputAvailable = false;
        }

        if (isZoomInputAvailable) {
            Camera.main.fieldOfView = fov;
            isZoomInputAvailable = false;
        }

        if (isRotationInputAvailable) {
            Camera.main.transform.rotation = Quaternion.Euler(y, 0, 0);
            isRotationInputAvailable = false;
        }

    }

    void adjustZoom() {
        if (Input.GetAxis("Mouse ScrollWheel") != 0) {
            fov = Camera.main.fieldOfView;
            fov += Input.GetAxis("Mouse ScrollWheel") * CameraConstants.MOUSE_SENSITIVITY;
            fov = Mathf.Clamp(fov, CameraConstants.CAMERA_MIN_FOV, CameraConstants.CAMERA_MAX_FOV);
            isZoomInputAvailable = true;
        }
    }

    void adjustMovement() {
        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0) {
            vAxis = Input.GetAxis("Vertical");
            hAxis = Input.GetAxis("Horizontal");
            moveDir = (Vector3.right * hAxis + Vector3.up * vAxis).normalized;
            isMoveInputAvailable = true;
        }
    }

    void adjustRotation() {
        if (Input.GetKey(KeyCode.LeftAlt)) {
            y -= Input.GetAxis("Mouse Y") * CameraConstants.VERTICAL_ROTATION_SPEED;
            y = ClampAngle(y, CameraConstants.MIN_PITCH_ANGLE, CameraConstants.MAX_PITCH_ANGLE);
            isRotationInputAvailable = true;
        }
    }

    public static float ClampAngle(float angle, float min, float max) {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}
