﻿using UnityEngine;
using UnityEditor;

public static class DebugMenu

    //todo dodaj ove prpo
{
    [MenuItem("CPG/World and local display")]
    public static void DebugPosition()
    {
        if (Selection.activeGameObject != null)
        {
            Debug.Log(

                " world pos: " + Selection.activeGameObject.transform.position +
                " local pos: " + Selection.activeGameObject.transform.localPosition +
                " world rotation: " + Selection.activeGameObject.transform.rotation +
                " local rotation: " + Selection.activeGameObject.transform.localRotation);
			Selection.activeGameObject = null;
        }
    }


    [MenuItem("CPG/Draw axes")]
    public static void DebugRotation()
    {
        if (Selection.activeGameObject != null)
        {
            Selection.activeGameObject.AddComponent<DrawAxes>();
            return;
        }

        GameObject.Destroy(Selection.activeGameObject.GetComponent<DrawAxes>());

    }
}
