using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor (typeof(TileMapOriginal))]
public class TileMapInspector : Editor {

    public float v = .5f;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        EditorGUILayout.BeginHorizontal();
        v = EditorGUILayout.Slider(v, 0, 2);
        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Refresh map"))
        {
            ((TileMapOriginal)target).BuildMesh();
        }
    }
    

}
