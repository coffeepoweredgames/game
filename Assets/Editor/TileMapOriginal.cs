﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class TileMapOriginal : MonoBehaviour {
	public int tileWidth = 1;
	public int no_tile_x;//koliko ih zelim imati
	public int no_tile_z;
	//public Texture2D myTerrainTilesImage;
	public int tileResolution;//16 pixels//tile size in px
	
	// Use this for initialization
	void Start () {
		BuildMesh();	
	}
	
	
	public void BuildTexture(){
		Texture2D texture = null;
		int texWidth  = no_tile_x * tileResolution; //100 * 16 = 1600 px
		int texHeight = no_tile_z * tileResolution; // 50 * 16= 800 px
		texture = new Texture2D(texWidth, texHeight);
		
		//Color[][] tiles = LoadTileColors();
		
		/*for (int z = 0; z < no_tile_z; z++)//loop tile by tile
        {
            for (int x = 0; x < no_tile_x; x++)
            {
                Color[] color = tiles[Random.Range(0, 4)];
                texture.SetPixels(x * tileResolution, z * tileResolution, tileResolution, tileResolution, color);
            }
        }*/
		
		texture.filterMode = FilterMode.Point;
		texture.Apply();// Apply all previous SetPixel and SetPixels changes.
		texture.wrapMode = TextureWrapMode.Clamp;//nacin kako handleati uv kooordinate >1
		
		MeshRenderer mesh_renderer = GetComponent<MeshRenderer>();
		//mesh_renderer.sharedMaterials[0].mainTexture = texture; 
		Debug.Log("Build Texture!");
		
	}
	
	public void BuildMesh() {
		
		int no_vertex_x = no_tile_x + 1;
		int no_vertex_z = no_tile_z + 1;
		int totalVertices = no_vertex_x * no_vertex_x;
		int no_triangles = no_tile_x * no_tile_z * 2;//za svaki tile trebas 2 trokuta
		
		//Generate the mesh data
		Vector3[] vertices = new Vector3[totalVertices];
		Vector3[] normals = new Vector3[totalVertices];
		Vector2[] uv = new Vector2[totalVertices];
		int[] triangles = new int[no_triangles * 3];//triangle is a int triple
		float uv_offset_x = (float)1 / no_tile_x;
		float uv_offset_z = (float)1 / no_tile_z;
		
		//Generate vertices
		for (int z = 0; z < no_vertex_z; z++)
		{
			for (int x = 0; x < no_vertex_x; x++)
			{
				vertices[z * no_vertex_x + x] = new Vector3(x * tileWidth, 0, z * tileWidth);
				normals[z * no_vertex_x + x] = transform.up;
				uv[z * no_vertex_x + x] = new Vector2(x * uv_offset_x, z *uv_offset_z);
				
				//print("v = "+vertices[z * no_vertex_x + x]+" uv = " + uv[z * no_vertex_x + x]);
				//print("x * uv_offset_x = " + x + " * " + uv_offset_x + " = " + x * uv_offset_x);
			}
		}
		
		//Generate triangles
		for (int z = 0; z < no_tile_z; z++)
		{
			for (int x = 0; x < no_tile_x; x++)//svaka iteracija ovdje stvara 2 trokuta
			{
				int squareIndex = z * no_tile_x + x;
				int triOffset = squareIndex * 6;
				triangles[triOffset + 0] = (z+1) * no_vertex_x + x;
				triangles[triOffset + 1] =     z * no_vertex_x + x+1;
				triangles[triOffset + 2] =     z * no_vertex_x + x;
				
				triangles[triOffset + 3] =   (z+1) * no_vertex_x + x;
				triangles[triOffset + 4] =   (z+1) * no_vertex_x + x+1;
				triangles[triOffset + 5] =      z  * no_vertex_x + x+1;
				
				/*
                triangles[triOffset + 0] = z * no_vertex_x + x;
                triangles[triOffset + 1] = (z + 1) * no_vertex_x + x;
                triangles[triOffset + 2] = (z + 1) * no_vertex_x + x + 1;
                triangles[triOffset + 3] = z * no_vertex_x + x;
                triangles[triOffset + 4] = (z + 1) * no_vertex_x + x + 1;
                triangles[triOffset + 5] = z * no_vertex_x + x + 1;
                 */
			}
			
		}
		
		//Create a new Mesh and populate it with data
		Mesh mesh = new Mesh();//Mesh je obican objekt koji cuva par arraya u sebi.
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.normals = normals;
		mesh.uv = uv;
		
		//Assign our mesh to our filter/collider 
		MeshFilter mesh_filter = GetComponent<MeshFilter>();
		MeshCollider mesh_collider = GetComponent<MeshCollider>();
		//MeshRenderer mesh_renderer = GetComponent<MeshRenderer>();
		
		mesh_collider.sharedMesh = mesh;//(bez ovoga raycast test against this collider nece raditi, nije dovoljno imati mesh_collider, mora imati assigned mesh instancu tj. pointer na nj)
		mesh_filter.mesh = mesh;
		
		BuildTexture();
		
	}
	
	
	/*Color[][] LoadTileColors()
    {
        int numTilesPerRow = myTerrainTilesImage.width / tileResolution;
        int numRows = myTerrainTilesImage.height / tileResolution;
        Color[][] tiles = new Color[numTilesPerRow * numRows][];
        for (int y = 0; y < numRows; y++)//row by row
        {
            for (int x = 0; x < numTilesPerRow; x++)//iterate through row
            {
                tiles[y * numTilesPerRow + x] = myTerrainTilesImage.GetPixels(x * tileResolution, y * tileResolution, tileResolution, tileResolution);
            }
        }
        return tiles;
    }*/
}
