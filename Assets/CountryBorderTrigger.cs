﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class exposes listeners for OnTriggerXXX events, and routes them to Country component.
/// </summary>
/// 
/// note: Triggers are only called when dragging, so instead of behaving like you are clicking.
///  
/// commment3:
/// - cannot let user drag agent around like its nothing, you cant just cross a border, then dragg onto factory
/// You will first have to enter the country if you let your mouse button, then you will drag again after dialog onto factory
/// and then immediately you will have a structure dialog displayed

public class CountryBorderTrigger : GameBase {
    private CountryBorder border;
    private Country country;
    private MapSelector mapSelector;

    void Start() {
        border = gameObject.GetComponentInParent<CountryBorder>();
        country = border.country;
        mapSelector = MapSelector.Instance;
    }

    private Agent GetAgent(GameObject gameObject) {
        return gameObject.GetComponentInParent<Agent>();
    }

    void OnTriggerEnter(Collider collider) {
        Debug.Log("Country border trigger activated! OnTriggerEnter: " + collider.name);
        Agent agent = GetAgent(collider.gameObject);

        if (!CountryBorder.isInboundMovement(agent, country)) {
            return;//outbound movement means we're exiting the country
        }

        //country.OnCountryBorderEnter(null, agent);
    }

    void OnTriggerStay(Collider collider) {
        //Agent agent = GetAgent(collider.gameObject);

        //country.OnCountryBorderStay(agent);
    }

    void OnTriggerExit(Collider collider) {
        Debug.Log("Border trigger trigered! OnTriggerExit: " + collider.name);

        Agent agent = GetAgent(collider.gameObject);

        if (CountryBorder.isInboundMovement(agent, country)) {
            return;//inbound movement means we're entering the country
        }

        //country.OnCountryBorderExit(agent);
    }

}
